import {Entity, Column, PrimaryGeneratedColumn, JoinColumn, ManyToOne} from "typeorm";
import { Lobby } from "./Lobby";
import { User } from "./User";

@Entity({ name: "Lobby_History"
// @Entity({ name: "Lobby_History"
,orderBy: {
    Lobby_History_Id: "DESC"
}})
export class Lobby_History {

    @PrimaryGeneratedColumn()
    Lobby_History_Id : number

    //with boolean
    @Column({type:"enum", enum:["W1","W2","W3","W4","W5","Loss"], default:"Loss"})//change enum
    Winning_Status : string

    @Column({type:"float"})
    Winning_Price : number

    @Column({type:"int"})
    Powerups_used : number

    @Column({type:"float"})
    Total_Amount_Spend : number

    @ManyToOne(type => Lobby, lobby => lobby.Lobby_Id,{ onDelete: 'CASCADE', onUpdate: 'CASCADE' })
    @JoinColumn()
    Lobby : number

    @ManyToOne(type => User, user => user.User_Id,{ onDelete: 'CASCADE', onUpdate: 'CASCADE' })
    @JoinColumn()
    User : number
}