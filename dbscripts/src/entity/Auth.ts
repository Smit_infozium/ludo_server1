import {Entity, Column, OneToOne, JoinColumn, PrimaryGeneratedColumn} from "typeorm";
import { User } from "./User";

@Entity({ name: "Auth"})
// @Entity({ name: "Auth"})
export class Auth {

    @PrimaryGeneratedColumn()
    Auth_Id : number
    
    @Column({type: "varchar"})
    Token : string
    
    @Column()
    Currently_Playing : boolean
    
    @Column({nullable: true, type: "varchar" })
    Device_Id : string

    @OneToOne(type => User, user => user.User_Id,{ onDelete: 'CASCADE', onUpdate: 'CASCADE' })
    @JoinColumn()
    User : number
}