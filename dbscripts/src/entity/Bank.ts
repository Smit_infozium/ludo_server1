import {Entity, Column, ManyToOne, JoinColumn, PrimaryGeneratedColumn} from "typeorm";
import { User } from "./User";

@Entity({ name: "Bank"})
// @Entity({ name: "Bank"})
export class Bank {

    @PrimaryGeneratedColumn()
    Bank_Id : number
    
    @Column({type: "varchar"})
    Account_Holder_Name : string
    
    @Column({type: "varchar"})
    IFSC_Code : string

    @Column({type: "varchar"})
    Account_Number : string

    @ManyToOne(type => User, user => user.User_Id,{ onDelete: 'CASCADE', onUpdate: 'CASCADE' })
    @JoinColumn()
    User : number
}