import {Entity, Column, OneToOne, PrimaryGeneratedColumn, JoinColumn} from "typeorm";
import { User } from "./User";

@Entity({name:"GameData"})
// @Entity({name:"GameData"})
export class GameData {

    @PrimaryGeneratedColumn()
    GameData_Id : number
    
    @Column({type:"int"})
    Total_Wins : number
    
    @Column({type:"int"})
    Total_Loss : number
    
    @Column({type:"int"})
    Token_Captured : number
    
    @Column({type:"int"})
    Opp_Token_Captured : number

    @Column({type:"json", nullable: true})
    Achievements : string

    @OneToOne(type => User, user => user.User_Id,{ onDelete: 'CASCADE', onUpdate: 'CASCADE' })
    @JoinColumn()
    User : number
}