import {Entity, PrimaryGeneratedColumn, Column, ManyToOne, JoinColumn} from "typeorm";
import { Admin } from "./Admin";

@Entity({ name: "Lobby_Structure" })
// @Entity({ name: "Lobby_Structure"})
export class Lobby_Structure {

    @PrimaryGeneratedColumn()
    Lobby_Structure_Id : number

    @Column({type: "varchar"})
    Lobby_Structure_Name : string

    @Column({type: "float"})
    Lobby_Entry_fees : number

    @Column({type: "float", default:3})
    Lobby_Per_cut : number

    @Column({type: "int"})
    No_of_Players : number

    @Column()
    IsActive : boolean
    
    @ManyToOne(type => Admin, admin => admin.Admin_Id,{ onDelete: 'CASCADE', onUpdate: 'CASCADE' })
    @JoinColumn()
    Admin : number

}