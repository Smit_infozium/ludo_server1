import {Entity, PrimaryGeneratedColumn, Column} from "typeorm";

@Entity({name:"Admin"})
// @Entity({name:"Admin"})
export class Admin{
    
    @PrimaryGeneratedColumn()
    Admin_Id : number

    @Column({type:"varchar", unique:true})
    Admin_Name : string

    @Column({type:"enum", enum:["Admin","SubAdmin"], default:"SubAdmin"})
    Admin_Roles : string

    @Column()
    Admin_Password : string

    @Column({ default: true })
    IsActive : boolean
    
    @Column({type: "varchar", nullable: true})
    Token : string
}