import {Entity, PrimaryGeneratedColumn, Column, JoinColumn, ManyToOne} from "typeorm";
import { User } from "./User";

@Entity({name:"Buddies"})
// @Entity({name:"Buddies"})
export class Buddies{
    
    @PrimaryGeneratedColumn()
    Buddies_Id : number

    @ManyToOne(type => User, user => user.User_Id,{ onDelete: 'CASCADE', onUpdate: 'CASCADE' })
    @JoinColumn()
    Request_by : number

    @ManyToOne(type => User, user => user.User_Id,{ onDelete: 'CASCADE', onUpdate: 'CASCADE' })
    @JoinColumn()
    Request_to : number

    @Column({type:"enum", enum:["Pending","Approved","Rejected","Block"], default:"Pending"})
    Req_status : string
}