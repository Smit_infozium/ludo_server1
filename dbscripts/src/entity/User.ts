import {Entity, Column, OneToOne, JoinColumn, ManyToOne, PrimaryColumn} from "typeorm";
import { Auth } from "./Auth";
import { GameData } from "./GameData";
import { Wallet } from "./Wallet";

@Entity({name:"User"})
// @Entity({name:"User"})
export class User {
    
    @PrimaryColumn({type:"bigint"})
    User_Id : number

    @Column({type:"varchar", unique:true})
    User_Name : string

    @Column({type:"varchar"})
    User_DisplayName : string

    @Column({type:"varchar"})
    User_Password : string

    @Column({type:"varchar"})
    User_Country : string

    @Column({ type: "bigint",unique: true})
    User_Mobile_Number : number

    @Column("varchar")
    User_Image : string
    
    @Column()
    IsActive : boolean

    @Column()
    Priviledge : boolean

    @Column({type: "varchar", nullable: true})
    Fb_User_Id : string

    @Column({type:"varchar", unique:true})
    Reference_Id : string
    
    @ManyToOne(type => User, user => user.User_Id,{ onDelete: 'CASCADE', onUpdate: 'CASCADE' })
    @JoinColumn()
    Reference_By : number
    
    @OneToOne(type => Wallet, wallet => wallet.User)
    Wallet : Wallet

    @OneToOne(type => GameData, gamedata => gamedata.User)
    GameData : GameData

    @OneToOne(type => Auth, auth => auth.User)
    Auth : Auth
}
