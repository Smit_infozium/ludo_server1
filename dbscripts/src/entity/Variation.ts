import {Entity, PrimaryGeneratedColumn, Column} from "typeorm";

@Entity({ name: "Variation"})
// @Entity({ name: "Variation"})
export class Variation {

    @PrimaryGeneratedColumn()
    Variation_Id : number

    @Column({type: "varchar"})
    Variation_Name : string

    @Column({length:3})
    Variation_Initial : string

    @Column({type: "int"})
    Player_Count : number
}
