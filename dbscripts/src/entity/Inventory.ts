import {Entity, Column, OneToOne, PrimaryGeneratedColumn, JoinColumn} from "typeorm";
import { User } from "./User";

@Entity({name:"Inventory"})
//@Entity({name:"Inventory"})
export class Inventory {

    @PrimaryGeneratedColumn()
    Inventory_Id : number
    
    @Column({type:"json", nullable: true})
    Emoji : string

    @Column({type:"json", nullable: true})
    Dice : string

    @OneToOne(type => User, user => user.User_Id,{ onDelete: 'CASCADE', onUpdate: 'CASCADE' })
    @JoinColumn()
    User : number
}