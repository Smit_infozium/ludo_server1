import {Entity, PrimaryGeneratedColumn, Timestamp, Column, ManyToOne, JoinColumn} from "typeorm";
import { User } from "./User";
import { Variation } from "./Variation";

@Entity({ name: "Lobby" })
// @Entity({ name: "Lobby"})
export class Lobby {

    @PrimaryGeneratedColumn()
    Lobby_Id : number

    @Column({type: "varchar"})
    Lobby_Name : string

    @Column({type: "float"})
    Lobby_Total_Amount : number

    @Column({type: "float"})
    Lobby_Buy_In : number

    @Column({type: "float"})
    Lobby_Commision_Amount : number

    @Column({type: "varchar"})
    Room_Id : string

    @Column({type:"enum", enum:["Commit", "Rollback", "Null"], default:"Null"})
    Lobby_Status

    @Column({type:'json', nullable: true})
    Recorded_Move

    @Column({type:"timestamp",  default: () => 'CURRENT_TIMESTAMP'})
    Lobby_Start_Time_Date : Timestamp

    @Column({type:"timestamp", nullable: true, default:null })
    Lobby_End_Time_Date : Timestamp

    @ManyToOne(type => Variation, variation => variation.Variation_Id,{ onDelete: 'CASCADE', onUpdate: 'CASCADE' })
    @JoinColumn()
    Variation : number

    @ManyToOne(type => User, owner => owner.User_Id,{ onDelete: 'CASCADE', onUpdate: 'CASCADE' })
    @JoinColumn()
    Lobby_Owner : number
}