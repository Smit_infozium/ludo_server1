
import {Entity, PrimaryGeneratedColumn, Column, Timestamp, ManyToOne, JoinColumn} from "typeorm";
import { User } from "./User";

//to create tables
@Entity({ name: "Payment"
// @Entity({ name: "Payment"
    ,orderBy: {
        Payment_Time_Date: "DESC"
     }})
export class Payment {

    @PrimaryGeneratedColumn()
    Payment_Id : number

    @Column()
    Is_Credit : boolean

    @Column({type:"float"})
    Payment_Amount : number

    @Column({type:"timestamp",  default: () => 'CURRENT_TIMESTAMP'})
    Payment_Time_Date : Timestamp
    
    @Column({type:"bigint"})
    Transaction_Id : number
    
    @Column({type:"enum", enum:["Success","failed"] })
    Payment_status : string
    
    @ManyToOne(type => User, user => user.User_Id,{ onDelete: 'CASCADE', onUpdate: 'CASCADE' })
    @JoinColumn()
    User : User
}
