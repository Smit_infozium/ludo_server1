import {Entity, Column, ManyToOne, JoinColumn, PrimaryGeneratedColumn} from "typeorm";
import { User } from "./User";

@Entity({ name: "UPI"})
// @Entity({ name: "UPI"})
export class UPI {

    @PrimaryGeneratedColumn()
    U_Id : number
    
    @Column({type: "varchar"})
    UPI_Id : string

    @ManyToOne(type => User, user => user.User_Id,{ onDelete: 'CASCADE', onUpdate: 'CASCADE' })
    @JoinColumn()
    User : number
}