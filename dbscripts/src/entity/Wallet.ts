import {Entity, Column, JoinColumn, OneToOne, PrimaryGeneratedColumn} from "typeorm";
import { User } from "./User";

@Entity({ name: "Wallet"})
// @Entity({ name: "Wallet"})
export class Wallet {

    @PrimaryGeneratedColumn()
    Wallet_Id : number

    @Column({type: "float"})
    User_Amount : number
    
    @Column({type: "float"})
    User_Bonus_Cash : number

    @Column({type: "float"})
    User_Credit_Amount : number

    @Column({type: "float"})
    User_Debit_Amount : number

    @Column({type: "float"})
    User_Win_Amount : number

    @Column({type: "float"})
    User_Loss_Amount : number

    @OneToOne(type => User, user => user.User_Id,{ onDelete: 'CASCADE', onUpdate: 'CASCADE' })
    @JoinColumn()
    User : number

}