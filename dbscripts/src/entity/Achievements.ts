import {Entity, Column, PrimaryGeneratedColumn} from "typeorm";

@Entity({ name: "Achievements"})
// @Entity({ name: "Achievements"})
export class Achievements {

    @PrimaryGeneratedColumn()
    Achievements_Id : number
    
    @Column({type: "varchar"})
    Title : string

    @Column({type: "varchar"})
    Statement : string

    @Column({type: "int"})
    Total_Count : string

    @Column({type: "float"})
    Reward : string
}