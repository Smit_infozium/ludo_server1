import { DbUtils } from "../utils/DbUtils";
import { InternalServerError, DuplicateDataError, BannedError, WrongDataError, NotModifiedError, NotFoundError } from '../errors/InstancesCE';
import {CustomError} from '../errors/CustomError';
import { User } from "../entity/User";
import { Lobby } from "../entity/Lobby";
import { Admin } from "../entity/Admin";
import { Variation } from "../entity/Variation";
import { Lobby_History } from "../entity/Lobby_History";
import { Payment } from "../entity/Payment";
import { Not } from "typeorm";
import * as jwt from "jsonwebtoken";
import config from "../config/config";
import { Achievements } from "../entity/Achievements";
import { Lobby_Structure } from "../entity/Lobby_Structure";
var Passwordhash = require('password-hash');
const { check, oneOf, validationResult } = require('express-validator');


//=======================Admin============================

async function createAdmin(req, res, next){
    console.log('POST /api/admin/createadmin API call made');
    const { admin_name, admin_role, admin_password } = req.body;

    try {
        const val_err = validationResult(req);
        console.log(val_err);
        if (!val_err.isEmpty()) {
            let er = [];

            //return res.status(400).json({success: false, errors: val_err.array() });
            val_err.array().forEach(element => {
                er.push("data of "+element.param+" is "+element.msg+".")
                // return element
            });
            console.log("er: "+JSON.stringify(er));
            let err =  new WrongDataError(er);
            return next(err)
        }

        const connection = await DbUtils.getConnection();
        if(await connection.manager.findOne(Admin, { where: { Admin_Name : admin_name } })) {
            //return res.status(409).json({ message: 'The Admin Name has been taken, please try another'});
            let err =  new DuplicateDataError("The Admin Name has been taken, please try another");
            return next(err)

        }

        let admin = new Admin();
        admin.Admin_Name = admin_name;
        admin.Admin_Roles = admin_role;
        admin.Admin_Password = Passwordhash.generate(admin_password);
        admin.IsActive = true;
        await connection.manager.save(admin);

        return res.status(200).json({ message: 'Admin added successfully' });
    } catch (error) {
        console.log(error);
        throw new InternalServerError();
    }
}

async function adminLogin(req, res, next) {
    console.log('PUT /api/admin/adminlogin API call made');

    const { admin_name, admin_password} = req.body;
    try {
        const val_err = validationResult(req);
        console.log(val_err);
        if (!val_err.isEmpty()) {
            let er = [];

            //return res.status(400).json({success: false, errors: val_err.array() });
            val_err.array().forEach(element => {
                er.push("data of "+element.param+" is "+element.msg+".")
                // return element
            });
            console.log("er: "+JSON.stringify(er));
            let err =  new WrongDataError(er);
            return next(err)
        }

        const connection = await DbUtils.getConnection();
        const loginuser = await connection.manager.findOne(Admin, { where: { Admin_Name : admin_name } });

        if(loginuser && (Passwordhash.verify(admin_password, loginuser.Admin_Password))) {
            if(!loginuser.IsActive){
                //return res.status(403).json({ message: 'Your account is Banned' });
                let err = new BannedError("Your account is Banned")
                return next(err);
            }
            console.log("login");
            const token = jwt.sign(
                { userId: loginuser.id, username: admin_name},
                config.jwtSecret,
                { expiresIn: "1h" }
            );
            console.log(token+".."+loginuser.Admin_Id);
            const data1 = await connection
                .createQueryBuilder()
                .update(Admin)
                .set({
                    Token : token
                })
                .where("Admin_Id = :id", { id: loginuser.Admin_Id })
                .execute();
            let data = [await connection.manager.findOne(Admin, { where: { Admin_Id : loginuser.Admin_Id } })];
            console.log(data1);
            return res.status(200).json({data});
            // return res.setHeader('Authorization', `Bearer ${token}`).status(200).json({ message: 'User login successfully' });
        }
        //return res.status(400).json({ message: 'Incorrect Adminname or Password' });
        let err = new WrongDataError("Invalid Credentials")
        return next(err);
    } catch (error) {
        // if(error instanceof CustomError){
        //     let error_name = error.constructor.name;

        // }
        throw new InternalServerError();
    }
}

async function adminData(req, res, next) {
    console.log('GET /api/admin/admindata API call made');

    try {
        const connection = await DbUtils.getConnection();
        let data = await connection.createQueryBuilder()
            .select(['ad.Admin_Id"Admin_Id"', 'ad.Admin_Name"Admin_Name"',
                'ad.Admin_Roles"Admin_Roles"', 'ad.IsActive"IsActive"' ])
            .from(Admin, 'ad')
            .where("ad.IsActive = :ac", { ac: true})
            .getRawMany();

        //console.log("all users:", data[0].User_Image.toString());
        console.log("all users:", data);
        if (data) {
            return res.status(200).json(data);
        }
        //return res.status(404).json({ message: 'Data not available' });
        let err = new NotFoundError("Data not available")
        return next(err);
    } catch (error) {
        console.log(error);
        throw new InternalServerError();
    }
}

async function BannedadminData(req, res, next) {
    console.log('GET /api/admin/bannedadmindata API call made');

    try {
        const connection = await DbUtils.getConnection();
        let data = await connection.createQueryBuilder()
            .select(['ad.Admin_Id"Admin_Id"', 'ad.Admin_Name"Admin_Name"',
                'ad.Admin_Roles"Admin_Roles"', 'ad.IsActive"IsActive"' ])
            .from(Admin, 'ad')
            .where("ad.IsActive = :ac", { ac: false})
            .getRawMany();

        //console.log("all users:", data[0].User_Image.toString());
        console.log("all users:", data);
        if (data) {
            return res.status(200).json(data);
        }
        //return res.status(404).json({ message: 'Data not available' });
        let err = new NotFoundError("Data not available")
        return next(err);
    } catch (error) {
        console.log(error);
        throw new InternalServerError();
    }
}

async function searchAdminData(req, res, next) {
    console.log('POST /api/admin/searchadmindata API call made');
    const { admin_id } = req.body;

    try {
        const val_err = validationResult(req);
        console.log(val_err);
        if (!val_err.isEmpty()) {
            let er = [];

            //return res.status(400).json({success: false, errors: val_err.array() });
            val_err.array().forEach(element => {
                er.push("data of "+element.param+" is "+element.msg+".")
                // return element
            });
            console.log("er: "+JSON.stringify(er));
            let err =  new WrongDataError(er);
            return next(err)
        }

        const connection = await DbUtils.getConnection();
        let data = await connection.createQueryBuilder()
            .select(['ad.Admin_Id"Admin_Id"', 'ad.Admin_Name"Admin_Name"',
                'ad.Admin_Roles"Admin_Roles"', 'ad.IsActive"IsActive"'])
            .from(Admin, 'ad')
            .where("ad.Admin_Id = :id", { id: admin_id})
            .getRawMany();

        //console.log("all users:", data[0].User_Image.toString());
        console.log("all users:", data);
        if (data) {
            return res.status(200).json(data);
        }
        //return res.status(404).json({ message: 'Data not available' });
        let err = new NotFoundError("Data not available")
        return next(err);

    } catch (error) {
        console.log(error);
        throw new InternalServerError();
    }
}

async function updateAdminData(req, res, next) {
    console.log('PUT /api/admin/updateadmindata API call made');
    const { admin_id, admin_name, admin_role, admin_password, is_active } = req.body;
    console.log(req.body);
    try {
        const val_err = validationResult(req);
        console.log(val_err);
        if (!val_err.isEmpty()) {
            let er = [];

            //return res.status(400).json({success: false, errors: val_err.array() });
            val_err.array().forEach(element => {
                er.push("data of "+element.param+" is "+element.msg+".")
                // return element
            });
            console.log("er: "+JSON.stringify(er));
            let err =  new WrongDataError(er);
            return next(err)
        }

        const connection = await DbUtils.getConnection();
        const exists = await connection.manager.findOne(Admin, { where: { Admin_Id: admin_id } });
        if(!exists) {
            //return res.status(404).json({ message: 'Admin is not exist'});
            let err = new NotFoundError("Admin is not exist")
            return next(err);
        }
        if(await connection.manager.findOne(Admin, { where: { Admin_Name : admin_name } })) {
            //return res.status(409).json({ message: 'The Admin Name has been taken, please try another'});
            let err =  new DuplicateDataError("The Admin Name has been taken, please try another");
            return next(err)
        }
        const data = await connection
            .createQueryBuilder()
            .update(Admin)
            .set({
                Admin_Name : admin_name,
                Admin_Roles : admin_role,
                Admin_Password : Passwordhash.generate(admin_password),
                IsActive : Boolean(is_active)
            })
            .where("Admin_Id = :id", { id: admin_id })
            .execute();
        console.log(data);
        return res.status(200).json({ message: 'Admin updated successfully' });

    } catch (error) {
        console.log(error);
        throw new InternalServerError();

    }
}

async function updateBannedAdminData(req, res, next) {
    console.log('PUT /api/admin/updatebannedadmindata API call made');
    const { admin_id, is_active } = req.body;

    try {
        const val_err = validationResult(req);
        console.log(val_err);
        if (!val_err.isEmpty()) {
            let er = [];

            //return res.status(400).json({success: false, errors: val_err.array() });
            val_err.array().forEach(element => {
                er.push("data of "+element.param+" is "+element.msg+".")
                // return element
            });
            console.log("er: "+JSON.stringify(er));
            let err =  new WrongDataError(er);
            return next(err)
        }

        const connection = await DbUtils.getConnection();
        const exists = await connection.manager.findOne(Admin, { where: { Admin_Id: admin_id } });
        if(!exists) {
            //return res.status(404).json({ message: 'Admin is not exist'});
            let err = new NotFoundError("Admin is not exist")
            return next(err);
        }
        const data = await connection
            .createQueryBuilder()
            .update(Admin)
            .set({
                IsActive : Boolean(is_active)
            })
            .where("Admin_Id = :id", { id: admin_id })
            .execute();

        return res.status(200).json({ message: 'Admin Banned data updated successfully' });

    } catch (error) {
        console.log(error);
        throw new InternalServerError();

    }
}

async function deleteAdmin(req, res, next) {
    console.log('DELETE /api/admin/deleteadmin API call made');
    const { admin_id } = req.body;
    try {
        const val_err = validationResult(req);
        console.log(val_err);
        if (!val_err.isEmpty()) {
            let er = [];

            //return res.status(400).json({success: false, errors: val_err.array() });
            val_err.array().forEach(element => {
                er.push("data of "+element.param+" is "+element.msg+".")
                // return element
            });
            console.log("er: "+JSON.stringify(er));
            let err =  new WrongDataError(er);
            return next(err)
        }

        const connection = await DbUtils.getConnection();
        const exists = await connection.manager.findOne(Admin, { where: { Admin_Id: admin_id  } });
        if(!exists) {
            //return res.status(404).json({ message: 'Admin is not exist'});
            let err = new NotFoundError("Admin is not exist")
            return next(err);
        }

        //delete user data
        let data = await connection.getRepository(Admin).createQueryBuilder()
            .delete()
            .where("Admin_Id = :id", { id: admin_id  })
            .execute();

        // console.log("delete User: ", data);

        if(await connection.manager.findOne(Admin, { where: { Admin_Id: admin_id  } })) {
            //return res.status(304).json({ message: 'Admin not deleted' });
            let err = new NotModifiedError("Admin not deleted")
            return next(err);
        }
        return res.status(200).json({ message: 'Admin deleted successfully' });
    } catch (error) {
        console.log(error);
        throw new InternalServerError();
    }
}

//=======================User full details============================

async function userAllDetails(req, res, next) {
    console.log('GET /api/admin/usersalldetails API call made');

    try {
        const connection = await DbUtils.getConnection();
        let data =
            // await connection.manager.find(User, { select: ['User_Id', 'User_Name', 'User_DisplayName', 'User_Country', 'User_Image', 'User_Level'], relations: ['Wallet', 'GameData'] });
            await connection.createQueryBuilder()
                .select(['u.User_Id"User_Id"', 'u.User_Name"Username"',
                    'u.User_DisplayName"User_DisplayName"', 'u.User_Country"User_Country"',
                    'w.User_Amount"User_Amount"', 'u.Priviledge"Priviledge"',
                    'w.User_Credit_Amount"User_Credit_Amount"', 'w.User_Debit_Amount"User_Debit_Amount"',
                    'w.User_Win_Amount"User_Win_Amount"', 'w.User_Loss_Amount"User_Loss_Amount"',
                    'g.Total_Wins"Total_Wins"', 'g.Total_Loss"Total_Loss"',
                    'g.Token_Captured"Token_Captured"', 'g.Opp_Token_Captured"Opp_Token_Captured"' ])
                .from(User, 'u')
                .leftJoin('u.Wallet', 'w')
                .leftJoin('u.GameData', 'g')
                .getRawMany();

        //console.log("all users:", data[0].User_Image.toString());
        // console.log("all users:", data);
        if (data) {
            return res.status(200).json(data);
        }
        // return res.status(404).json({ message: 'Data not available' });
        let err = new NotFoundError("Data not available")
        return next(err);
    } catch (error) {
        throw new InternalServerError();
    }
}

async function userPreviledgeAllDetails(req, res, next) {
    console.log('GET /api/admin/userpreviledgealldetails API call made');

    try {
        const connection = await DbUtils.getConnection();
        let data =
            // await connection.manager.find(User, { select: ['User_Id', 'User_Name', 'User_DisplayName', 'User_Country', 'User_Image', 'User_Level'], relations: ['Wallet', 'GameData'] });
            await connection.createQueryBuilder()
                .select(['u.User_Id"User_Id"', 'u.User_Name"Username"',
                    'u.User_DisplayName"User_DisplayName"', 'u.User_Country"User_Country"',
                    'w.User_Amount"User_Amount"', 'u.Priviledge"Priviledge"',
                    'w.User_Credit_Amount"User_Credit_Amount"', 'w.User_Debit_Amount"User_Debit_Amount"',
                    'w.User_Win_Amount"User_Win_Amount"', 'w.User_Loss_Amount"User_Loss_Amount"',
                    'g.Total_Wins"Total_Wins"', 'g.Total_Loss"Total_Loss"',
                    'g.Token_Captured"Token_Captured"', 'g.Opp_Token_Captured"Opp_Token_Captured"' ])
                .from(User, 'u')
                .leftJoin('u.Wallet', 'w')
                .leftJoin('u.GameData', 'g')
                .where("u.Priviledge = :a", { a: true })
                .getRawMany();

        //console.log("all users:", data[0].User_Image.toString());
        // console.log("all users:", data);
        if (data) {
            return res.status(200).json(data);
        }
        // return res.status(404).json({ message: 'Data not available' });
        let err = new NotFoundError("Data not available")
        return next(err);
    } catch (error) {
        console.log(error)
        throw new InternalServerError();
    }
}

async function updatePriviledgeUser(req, res, next) {
    console.log('PUT /api/user/updatepriviledgeuser API call made');

    const {user_id, previledge} = req.body;
    try {
        console.log(req.body);
        const val_err = validationResult(req);
        console.log(val_err);
        if (!val_err.isEmpty()) {
            let er = [];

            //return res.status(400).json({success: false, errors: val_err.array() });
            val_err.array().forEach(element => {
                er.push("data of "+element.param+" is "+element.msg+".")
                // return element
            });
            console.log("er: "+JSON.stringify(er));
            let err =  new WrongDataError(er);
            return next(err)
        }

        const connection = await DbUtils.getConnection();
        const exists = await connection.manager.findOne(User, { where: { User_Id: user_id } });
        console.log(exists);
        if(!exists) {
            //return res.status(404).json({ message: 'User is not registered with us, please try another'});
            let err =  new NotFoundError("User is not registered with us, please try another");
            return next(err)
        }

        const data = await connection.getRepository(User)
            .createQueryBuilder()
            .update(User)
            .set({
                Priviledge : previledge
            })
            .where("User_Id = :id", { id: user_id })
            .execute();

        return res.status(200).json({ message: 'User Priviledge status Updated successfully' });
    } catch (error) {
        console.log(error);
        throw new InternalServerError();
    }
}

async function userDetails(req, res, next) {
    console.log('POST /api/admin/usersdetails API call made');
    const { user_id } = req.body;

    try {
        const val_err = validationResult(req);
        console.log(val_err);
        if (!val_err.isEmpty()) {
            //return res.status(400).json({success: false, errors: val_err.array() });
            let err =  new WrongDataError((val_err.errors[0].value+" is of "+val_err.errors[0].msg+" "+val_err.errors[0].param+".").toString());

            return next(err)
        }

        const connection = await DbUtils.getConnection();
        let data =
            // await connection.manager.find(User, { select: ['User_Id', 'User_Name', 'User_DisplayName', 'User_Country', 'User_Image', 'User_Level'], relations: ['Wallet', 'GameData'] });
            await connection.createQueryBuilder()
                .select(['u.User_Id"User_Id"', 'u.User_Name"Username"',
                    'u.User_DisplayName"User_DisplayName"', 'u.User_Country"User_Country"',
                    'w.User_Amount"User_Amount"', 'u.Priviledge"Priviledge',
                    'w.User_Credit_Amount"User_Credit_Amount"', 'w.User_Debit_Amount"User_Debit_Amount"',
                    'w.User_Win_Amount"User_Win_Amount"', 'w.User_Loss_Amount"User_Loss_Amount"',
                    'g.Total_Wins"Total_Wins"', 'g.Total_Loss"Total_Loss"',
                    'g.Token_Captured"Token_Captured"', 'g.Opp_Token_Captured"Opp_Token_Captured"' ])
                .from(User, 'u')
                .leftJoin('u.Wallet', 'w')
                .leftJoin('u.GameData', 'g')
                .where("u.User_Id = :id", { id: user_id })
                .getRawMany();

        //console.log("all users:", data[0].User_Image.toString());
        console.log("all users:", data);
        if (data) {
            return res.status(200).json(data);
        }
        // return res.status(404).json({ message: 'Data not available' });
        let err = new NotFoundError("Data not available")
        return next(err);
    } catch (error) {
        throw new InternalServerError();
    }
}

// ----------------------------------------------------

async function bannedUsers(req, res, next) {
    console.log('GET /api/admin/bannedusers API call made');

    try {
        const connection = await DbUtils.getConnection();
        let data = await connection.createQueryBuilder()
            .select(['u.User_Id"User_Id"', 'u.User_Name"Username"',
                'u.User_DisplayName"User_DisplayName"', 'u.User_Country"User_Country"' ])
            .from(User, 'u')
            .where("u.IsActive = :ac", { ac: false})
            .getRawMany();

        //console.log("all users:", data[0].User_Image.toString());
        console.log("all users:", data);
        if (data) {
            return res.status(200).json(data);
        }
        // return res.status(404).json({ message: 'Data not available' });
        let err = new NotFoundError("Data not available")
        return next(err);
    } catch (error) {
        console.log(error);
        throw new InternalServerError();
    }
}

async function updateActiveUser(req, res, next) {
    console.log('PUT /api/user/updateactiveuser API call made');

    const {user_id, is_active} = req.body;
    try {
        const val_err = validationResult(req);
        console.log(val_err);
        if (!val_err.isEmpty()) {
            let er = [];

            //return res.status(400).json({success: false, errors: val_err.array() });
            val_err.array().forEach(element => {
                er.push("data of "+element.param+" is "+element.msg+".")
                // return element
            });
            console.log("er: "+JSON.stringify(er));
            let err =  new WrongDataError(er);
            return next(err)
        }

        const connection = await DbUtils.getConnection();
        const exists = await connection.manager.findOne(User, { where: { User_Id: user_id } });
        if(!exists) {
            //return res.status(404).json({ message: 'User is not registered with us, please try another'});
            let err =  new NotFoundError("User is not registered with us, please try another");
            return next(err)
        }

        const data = await connection.getRepository(User)
            .createQueryBuilder()
            .update(User)
            .set({
                IsActive : is_active
            })
            .where("User_Id = :id", { id: user_id })
            .execute();

        return res.status(200).json({ message: 'User Active status Updated successfully' });
    } catch (error) {
        console.log(error);
        throw new InternalServerError();
    }
}

async function usersWallet(req, res, next) {
    console.log('GET /api/admin/userswallet API call made');

    try {
        const connection = await DbUtils.getConnection();
        let data =
            // await connection.manager.find(User, { select: ['User_Id', 'User_Name', 'User_DisplayName', 'User_Country', 'User_Image', 'User_Level'], relations: ['Wallet'] });
            await connection.createQueryBuilder()
                .select(['u.User_Id"User_Id"', 'u.User_Name"Username"',
                    'u.User_DisplayName"User_DisplayName"', 'u.User_Country"User_Country"',
                    'w.User_Amount"User_Amount"', 'w.User_Bonus_Cash"User_Bonus_Cash"',
                    'w.User_Credit_Amount"User_Credit_Amount"', 'w.User_Debit_Amount"User_Debit_Amount"',
                    'w.User_Win_Amount"User_Win_Amount"', 'w.User_Loss_Amount"User_Loss_Amount"'])
                .from(User, 'u')
                .leftJoin('u.Wallet', 'w')
                .where("u.IsActive = :id", { id: true })
                .getRawMany();

        //console.log("all users:", data[0].User_Image.toString());
        //console.log("all users:", data);
        if (data) {
            return res.status(200).json(data);
        }
        // return res.status(404).json({ message: 'Data not available' });
        let err = new NotFoundError("Data not available")
        return next(err);
    } catch (error) {
        throw new InternalServerError();
    }
}

async function userPayments(req, res, next) {
    console.log('POST /api/admin/userpayments API call made');
    const { user_id } = req.body;

    try {
        const val_err = validationResult(req);
        console.log(val_err);
        if (!val_err.isEmpty()) {
            let er = [];

            //return res.status(400).json({success: false, errors: val_err.array() });
            val_err.array().forEach(element => {
                er.push("data of "+element.param+" is "+element.msg+".")
                // return element
            });
            console.log("er: "+JSON.stringify(er));
            let err =  new WrongDataError(er);
            return next(err)
        }

        const connection = await DbUtils.getConnection();
        let data = await connection.createQueryBuilder()
            .select(['u.User_Id"User_Id"', 'u.User_Name"Username"',
                'p.Is_Credit"Is_Credit"', 'p.Payment_Amount"Payment_Amount"',
                'p.Payment_Time_Date"Payment_Time_Date"','p.Transaction_Id"Transaction_Id"',
                'p.Payment_status"Payment_status"'])
            .from(Payment, 'p')
            .leftJoin('p.User', 'u')
            .where("u.User_Id = :id", { id: user_id})
            .getRawMany();

        console.log("all users:", data);
        if (data) {
            return res.status(200).json(data);
        }
        // return res.status(404).json({ message: 'Data not available' });
        let err = new NotFoundError("Data not available")
        return next(err);
    }
    catch (error) {
        throw new InternalServerError();
    }
}

async function CreateVariatoin(req, res, next) {
    console.log('POST /api/admin/createvariation API call made');

    const { variation_name, variation_initial, player_count} = req.body;

    try {
        const val_err = validationResult(req);
        console.log(val_err);
        if (!val_err.isEmpty()) {
            let er = [];

            //return res.status(400).json({success: false, errors: val_err.array() });
            val_err.array().forEach(element => {
                er.push(element.value+" is  "+element.msg+" ats "+element.param+".")
                // return element
            });
            console.log("er: "+JSON.stringify(er));
            let err =  new WrongDataError(er);
            return next(err)
        }

        const connection = await DbUtils.getConnection();
        if(await connection.manager.findOne(Variation, { where: { Variation_Name : variation_name } })) {
            //return res.status(409).json({ message: 'The Variation Name has been taken, please try another'});
            let err =  new DuplicateDataError("The Variation Name has been taken, please try another");
            return next(err)
        }

        let variation = new Variation();
        variation.Variation_Name = variation_name;
        variation.Variation_Initial = variation_initial;
        variation.Player_Count = player_count;
        await connection.manager.save(variation);

        return res.status(200).json({ message: 'Variation Created successfully'});
    } catch (error) {
        throw new InternalServerError();
    }
}

async function Variations(req, res, next) {
    console.log('GET /api/admin/variations API call made');

    try {
        const connection = await DbUtils.getConnection();
        let data = await connection.manager.find(Variation);

        //console.log("all users:", data[0].User_Image.toString());
        console.log("all users:", data);
        if (data) {
            return res.status(200).json(data);
        }
        // return res.status(404).json({ message: 'Data not available' });
        let err = new NotFoundError("Data not available")
        return next(err);
    } catch (error) {
        throw new InternalServerError();
    }
}

async function SearchVariation(req, res, next) {
    console.log('POST /api/admin/searchvariations API call made');
    const { variation_id } = req.body;

    try {
        const val_err = validationResult(req);
        console.log(val_err);
        if (!val_err.isEmpty()) {
            let er = [];

            //return res.status(400).json({success: false, errors: val_err.array() });
            val_err.array().forEach(element => {
                er.push("data of "+element.param+" is "+element.msg+".")
                // return element
            });
            console.log("er: "+JSON.stringify(er));
            let err =  new WrongDataError(er);
            return next(err)
        }

        const connection = await DbUtils.getConnection();
        let data = await connection.manager.findOne(Variation, { where: { Variation_Id : variation_id } })

        //console.log("all users:", data[0].User_Image.toString());
        console.log("all users:", data);
        if (data) {
            return res.status(200).json(data);
        }
        // return res.status(404).json({ message: 'Data not available' });
        let err = new NotFoundError("Data not available")
        return next(err);
    } catch (error) {
        throw new InternalServerError();
    }
}

async function updateVariationsData(req, res, next) {
    console.log('PUT /api/admin/updatevariationsData API call made');
    const { variation_id, variation_name, variation_initial, player_count } = req.body;
    console.log(req.body);
    try {
        const val_err = validationResult(req);
        console.log(val_err);
        if (!val_err.isEmpty()) {
            let er = [];

            //return res.status(400).json({success: false, errors: val_err.array() });
            val_err.array().forEach(element => {
                er.push("data of "+element.param+" is "+element.msg+".")
                // return element
            });
            console.log("er: "+JSON.stringify(er));
            let err =  new WrongDataError(er);
            return next(err)
        }

        const connection = await DbUtils.getConnection();
        const exists = await connection.manager.findOne(Variation, { where: { Variation_Id : variation_id } })
        if(!exists) {
            // return res.status(404).json({ message: 'Variation is not exist'});
            let err = new NotFoundError("Variation is not exist")
            return next(err);
        }
        const data = await connection
            .createQueryBuilder()
            .update(Variation)
            .set({
                Variation_Name : variation_name,
                Variation_Initial : variation_initial,
                Player_Count : player_count
            })
            .where("Variation_Id = :id", { id: variation_id })
            .execute();
        console.log(data);

        return res.status(200).json({ message: 'Variation data updated successfully' });

    } catch (error) {
        console.log(error);
        throw new InternalServerError();

    }
}

async function deleteVariation(req, res, next) {
    console.log('DELETE /api/admin/deletevariation API call made');
    const { variation_id } = req.body;
    try {
        const val_err = validationResult(req);
        console.log(val_err);
        if (!val_err.isEmpty()) {
            let er = [];

            //return res.status(400).json({success: false, errors: val_err.array() });
            val_err.array().forEach(element => {
                er.push("data of "+element.param+" is "+element.msg+".")
                // return element
            });
            console.log("er: "+JSON.stringify(er));
            let err =  new WrongDataError(er);
            return next(err)
        }

        const connection = await DbUtils.getConnection();
        const exists = await connection.manager.findOne(Variation, { where: { Variation_Id: variation_id  } });
        if(!exists) {
            // return res.status(404).json({ message: 'Variation is not exist'});
            let err = new NotFoundError("Variation is not exist")
            return next(err);
        }

        //delete user data
        let data = await connection.getRepository(Variation).createQueryBuilder()
            .delete()
            .where("Variation_Id = :id", { id: variation_id  })
            .execute();

        // console.log("delete User: ", data);

        if(await connection.manager.findOne(Variation, { where: { Variation_Id: variation_id  } })) {
            //return res.status(304).json({ message: 'Variation not deleted' });
            let err = new NotModifiedError("Variation not deleted")
            return next(err);
        }
        return res.status(200).json({ message: 'Variation deleted successfully' });
    } catch (error) {
        console.log(error);
        throw new InternalServerError();
    }
}

async function userSearchLobbyHistoryData(req, res, next) {
    console.log('POST /api/admin/searchlobbyhistorydata API call made');
    const { lobby_id } = req.body;

    try {
        const val_err = validationResult(req);
        console.log(val_err);
        if (!val_err.isEmpty()) {
            //return res.status(400).json({success: false, errors: val_err.array() });
            let err =  new WrongDataError((val_err.errors[0].value+" is of "+val_err.errors[0].msg+" "+val_err.errors[0].param+".").toString());

            return next(err)
        }

        const connection = await DbUtils.getConnection();
        let data = await connection.createQueryBuilder()
            .select(['lb.Lobby_Id"Lobby_Id"', 'lb.Lobby_Name"Lobby_Name"',
                'lbh.Winning_Status"Winning_Status"', 'lbh.Winning_Price"Winning_Price"',
                'lbh.Powerups_used"Powerups_used"', 'lbh.Total_Amount_Spend"Total_Amount_Spend"',
                'u.User_Name"Lobby_Owner"'])
            .from(Lobby_History, 'lbh')
            .leftJoin('lbh.Lobby', 'lb')
            .leftJoin('lb.Lobby_Owner', 'u')
            .where("lb.Lobby_Id = :id", { id: lobby_id})
            .getRawMany();

        console.log("all users:", data);
        if (data) {
            return res.status(200).json(data);
        }
        // return res.status(404).json({ message: 'Data not available' });
        let err = new NotFoundError("Data not available")
        return next(err);
    } catch (error) {
        console.log(error);
        throw new InternalServerError();
    }
}

async function userSearchUserLobbyHistoryData(req, res, next) {
    console.log('POST /api/admin/searchuserlobbyhistorydata API call made');
    const { user_id } = req.body;

    try {
        const val_err = validationResult(req);
        console.log(val_err);
        if (!val_err.isEmpty()) {
            let er = [];

            //return res.status(400).json({success: false, errors: val_err.array() });
            val_err.array().forEach(element => {
                er.push("data of "+element.param+" is "+element.msg+".")
                // return element
            });
            console.log("er: "+JSON.stringify(er));
            let err =  new WrongDataError(er);
            return next(err)
        }

        const connection = await DbUtils.getConnection();
        let data = await connection.createQueryBuilder()
            .select(['lb.Lobby_Id"Lobby_Id"', 'lb.Lobby_Name"Lobby_Name"',
                'lbh.Winning_Status"Winning_Status"', 'lbh.Winning_Price"Winning_Price"',
                'lbh.Powerups_used"Powerups_used"', 'lbh.Total_Amount_Spend"Total_Amount_Spend"',
                'u.User_Name"User_Name"'])
            .from(Lobby_History, 'lbh')
            .leftJoin('lbh.Lobby', 'lb')
            .leftJoin('lbh.User', 'u')
            .where("u.User_Id = :id", { id: user_id})
            .getRawMany();

        console.log("all users:", data);
        if (data) {
            return res.status(200).json(data);
        }
        // return res.status(404).json({ message: 'Data not available' });
        let err = new NotFoundError("Data not available")
        return next(err);
    } catch (error) {
        console.log(error);
        throw new InternalServerError();
    }
}

//=======================Leisure============================

async function LeisureAllData(req, res, next) {
    console.log('GET /api/admin/leisurealldata API call made');

    try {
        const connection = await DbUtils.getConnection();
        let data = await connection.createQueryBuilder()
            .select(['lb.Lobby_Id"Lobby_Id"', 'lb.Lobby_Name"Lobby_Name"',
                'lbh.Winning_Status"Winning_Status"', 'lbh.Winning_Price"Winning_Price"',
                'lbh.Powerups_used"Powerups_used"', 'lbh.Total_Amount_Spend"Total_Amount_Spend"',
                'u.User_Name"User_Name"'])
            .from(Lobby_History, 'lbh')
            .leftJoin('lbh.Lobby', 'lb')
            .leftJoin('lbh.User', 'u')
            .where("lbh.Winning_Status != :st", {st: "Loss"})
            .getRawMany();

        console.log("all users:", data);
        if (data) {
            return res.status(200).json(data);
        }
        // return res.status(404).json({ message: 'Data not available' });
        let err = new NotFoundError("Data not available")
        return next(err);
    } catch (error) {
        console.log(error);
        throw new InternalServerError();
    }
}

async function LeisureData(req, res, next) {
    console.log('POST /api/admin/leisuredata API call made');
    const { lobby_id } = req.body;

    try {
        const val_err = validationResult(req);
        console.log(val_err);
        if (!val_err.isEmpty()) {
            let er = [];

            //return res.status(400).json({success: false, errors: val_err.array() });
            val_err.array().forEach(element => {
                er.push("data of "+element.param+" is "+element.msg+".")
                // return element
            });
            console.log("er: "+JSON.stringify(er));
            let err =  new WrongDataError(er);
            return next(err)
        }

        const connection = await DbUtils.getConnection();
        let data = await connection.createQueryBuilder()
            .select(['lb.Lobby_Id"Lobby_Id"', 'lb.Lobby_Name"Lobby_Name"',
                'lbh.Winning_Status"Winning_Status"', 'lbh.Winning_Price"Winning_Price"',
                'lbh.Powerups_used"Powerups_used"', 'lbh.Total_Amount_Spend"Total_Amount_Spend"',
                'u.User_Name"User_Name"'])
            .from(Lobby_History, 'lbh')
            .leftJoin('lbh.Lobby', 'lb')
            .leftJoin('lbh.User', 'u')
            .where("lb.Lobby_Id = :id", { id: lobby_id})
            .andWhere("lbh.Winning_Status != :st", {st: "Loss"})
            .getRawMany();

        console.log("all users:", data);
        if (data) {
            return res.status(200).json(data);
        }
        // return res.status(404).json({ message: 'Data not available' });
        let err = new NotFoundError("Data not available")
        return next(err);
    } catch (error) {
        console.log(error);
        throw new InternalServerError();
    }
}

async function userLeisureData(req, res, next) {
    console.log('POST /api/admin/userleisuredata API call made');
    const { user_id } = req.body;

    try {
        const val_err = validationResult(req);
        console.log(val_err);
        if (!val_err.isEmpty()) {
            let er = [];

            //return res.status(400).json({success: false, errors: val_err.array() });
            val_err.array().forEach(element => {
                er.push("data of "+element.param+" is "+element.msg+".")
                // return element
            });
            console.log("er: "+JSON.stringify(er));
            let err =  new WrongDataError(er);
            return next(err)
        }

        const connection = await DbUtils.getConnection();
        let data = await connection.createQueryBuilder()
            .select(['lb.Lobby_Id"Lobby_Id"', 'lb.Lobby_Name"Lobby_Name"',
                'lbh.Winning_Status"Winning_Status"', 'lbh.Winning_Price"Winning_Price"',
                'lbh.Powerups_used"Powerups_used"', 'lbh.Total_Amount_Spend"Total_Amount_Spend"',
                'u.User_Name"User_Name"'])
            .from(Lobby_History, 'lbh')
            .leftJoin('lbh.Lobby', 'lb')
            .leftJoin('lbh.User', 'u')
            .where("u.User_Id = :id", { id: user_id})
            .andWhere("lbh.Winning_Status != :st", {st: "Loss"})
            .getRawMany();

        console.log("all users:", data);
        if (data) {
            return res.status(200).json(data);
        }
        // return res.status(404).json({ message: 'Data not available' });
        let err = new NotFoundError("Data not available")
        return next(err);
    } catch (error) {
        console.log(error);
        throw new InternalServerError();
    }
}

//=======================Achievements============================

async function createAchievements(req, res, next){
    console.log('POST /api/admin/createachievements API call made');
    const { title, statement, total_count, reward } = req.body;

    try {
        const val_err = validationResult(req);
        console.log(val_err);
        if (!val_err.isEmpty()) {
            let er = [];

            //return res.status(400).json({success: false, errors: val_err.array() });
            val_err.array().forEach(element => {
                er.push("data of "+element.param+" is "+element.msg+".")
                // return element
            });
            console.log("er: "+JSON.stringify(er));
            let err =  new WrongDataError(er);
            return next(err)
        }

        const connection = await DbUtils.getConnection();
        if(await connection.manager.findOne(Achievements, { where: { Title : title } })) {
            //return res.status(409).json({ message: 'The Title has been taken, please try another'});
            let err =  new DuplicateDataError("The Title Name has been taken, please try another");
            return next(err)
        }
        else if(await connection.manager.findOne(Achievements, { where: { Statement : statement } })) {
            //return res.status(409).json({ message: 'The Statement has been taken, please try another'});
            let err =  new DuplicateDataError("The Statement Name has been taken, please try another");
            return next(err)
        }

        let achievements = new Achievements();
        achievements.Title = title;
        achievements.Statement = statement;
        achievements.Total_Count = total_count;
        achievements.Reward = reward;
        await connection.manager.save(achievements);

        return res.status(200).json({ message: 'Achievements added successfully' });
    } catch (error) {
        console.log(error);
        throw new InternalServerError();
    }
}

async function AllAchievements(req, res, next) {
    console.log('GET /api/admin/allachievements API call made');

    try {
        const connection = await DbUtils.getConnection();
        let data = await connection.manager.find(Achievements);

        //console.log("all users:", data[0].User_Image.toString());
        console.log("all users:", data);
        if (data) {
            return res.status(200).json(data);
        }
        // return res.status(404).json({ message: 'Data not available' });
        let err = new NotFoundError("Data not available")
        return next(err);
    } catch (error) {
        throw new InternalServerError();
    }
}

async function updateAchievementsData(req, res, next) {
    console.log('PUT /api/admin/updateachievementsData API call made');
    const { achievements_id, title, statement, total_count, reward } = req.body;

    try {
        const val_err = validationResult(req);
        console.log(val_err);
        if (!val_err.isEmpty()) {
            let er = [];

            //return res.status(400).json({success: false, errors: val_err.array() });
            val_err.array().forEach(element => {
                er.push("data of "+element.param+" is "+element.msg+".")
                // return element
            });
            console.log("er: "+JSON.stringify(er));
            let err =  new WrongDataError(er);
            return next(err)
        }

        const connection = await DbUtils.getConnection();
        const exists = await connection.manager.findOne(Achievements, { where: { Achievements_Id : achievements_id } })
        if(!exists) {
            // return res.status(404).json({ message: 'Achievements is not exist' });
            let err = new NotFoundError("Achievements is not exist")
            return next(err);
        }
        const data = await connection
            .createQueryBuilder()
            .update(Achievements)
            .set({
                Title : title,
                Statement : statement,
                Total_Count : total_count,
                Reward : reward
            })
            .where("Achievements_Id = :id", { id: achievements_id })
            .execute();

        return res.status(200).json({ message: 'Achievements data updated successfully' });

    } catch (error) {
        console.log(error);
        throw new InternalServerError();

    }
}

async function deleteAchievements(req, res, next) {
    console.log('DELETE /api/admin/deleteachievements API call made');
    const { achievements_id } = req.body;
    try {
        const val_err = validationResult(req);
        console.log(val_err);
        if (!val_err.isEmpty()) {
            let er = [];

            //return res.status(400).json({success: false, errors: val_err.array() });
            val_err.array().forEach(element => {
                er.push("data of "+element.param+" is "+element.msg+".")
                // return element
            });
            console.log("er: "+JSON.stringify(er));
            let err =  new WrongDataError(er);
            return next(err)
        }

        const connection = await DbUtils.getConnection();
        const exists = await connection.manager.findOne(Achievements, { where: { Achievements_Id: achievements_id  } });
        if(!exists) {
            // return res.status(404).json({ message: 'Achievements is not exist' });
            let err = new NotFoundError("Achievements is not exist")
            return next(err);
        }

        //delete user data
        let data = await connection.getRepository(Achievements).createQueryBuilder()
            .delete()
            .where("Achievements_Id = :id", { id: achievements_id  })
            .execute();

        // console.log("delete User: ", data);

        if(await connection.manager.findOne(Achievements, { where: { Achievements_Id: achievements_id  } })) {
            //return res.status(304).json({ message: 'Achievements not deleted' });
            let err = new NotModifiedError("Achievements not deleted")
            return next(err);
        }
        return res.status(200).json({ message: 'Achievements deleted successfully' });
    } catch (error) {
        console.log(error);
        throw new InternalServerError();
    }
}

async function AllPayments(req, res, next) {
    console.log('GET /api/admin/allpayments API call made');

    try {
        const connection = await DbUtils.getConnection();
        let data = await connection.createQueryBuilder()
            .select(['u.User_Id"User_Id"', 'u.User_Name"Username"',
                'p.Is_Credit"Is_Credit"', 'p.Payment_Amount"Payment_Amount"',
                'p.Payment_Time_Date"Payment_Time_Date"','p.Transaction_Id"Transaction_Id"',
                'p.Payment_status"Payment_status"'])
            .from(Payment, 'p')
            .leftJoin('p.User', 'u')
            .getRawMany();

        //console.log("all users:", data[0].User_Image.toString());
        console.log("all users:", data);
        if (data) {
            return res.status(200).json(data);
        }
        // return res.status(404).json({ message: 'Data not available' });
        let err = new NotFoundError("Data not available")
        return next(err);
    } catch (error) {
        throw new InternalServerError();
    }
}

async function AllLobbies(req, res, next) {
    console.log('GET /api/admin/alllobbies API call made');

    try {
        const connection = await DbUtils.getConnection();
        let data =
            //await connection.manager.find(Lobby);
            await connection.createQueryBuilder()
                .select(['lb.Lobby_Id"Lobby_Id"', 'lb.Lobby_Name"Lobby_Name"',
                    'lb.Lobby_Total_Amount"Lobby_Total_Amount"','lb.Lobby_Buy_In"Lobby_Buy_In"',
                    'lb.Lobby_Commision_Amount"Lobby_Commision_Amount"',
                    'lb.Room_Id"Room_Id"','lb.Lobby_Status"Lobby_Status"',
                    'lb.Lobby_Start_Time_Date"Lobby_Start_Time_Date"', 'lb.Lobby_End_Time_Date"Lobby_End_Time_Date"',
                    'u.User_Name"Lobby_Owner"', 'v.Variation_Name"Variation_Name"' ])
                .from(Lobby, 'lb')
                .leftJoin('lb.Lobby_Owner', 'u')
                .leftJoin('lb.Variation', 'v')
                .getRawMany();

        //console.log("all users:", data[0].User_Image.toString());
        console.log("all users:", data);
        if (data) {
            return res.status(200).json(data);
        }
        // return res.status(404).json({ message: 'Data not available' });
        let err = new NotFoundError("Data not available")
        return next(err);
    } catch (error) {
        throw new InternalServerError();
    }
}

async function AllLobbiesCommision(req, res, next) {
    console.log('GET /api/admin/alllobbiescommision API call made');

    try {
        const connection = await DbUtils.getConnection();
        let data1 = await connection.createQueryBuilder()
            .select(['lb.Lobby_Id"Lobby_Id"', 'lb.Lobby_Name"Lobby_Name"',
                'lb.Lobby_Total_Amount"Lobby_Total_Amount"','lb.Lobby_Buy_In"Lobby_Buy_In"',
                'lb.Lobby_Commision_Amount"Lobby_Commision_Amount"',
                'u.User_Name"Lobby_Owner"', 'v.Variation_Name"Variation_Name"' ])
            .from(Lobby, 'lb')
            .leftJoin('lb.Lobby_Owner', 'u')
            .leftJoin('lb.Variation', 'v')
            .getRawMany();

        let total = await connection.createQueryBuilder()
            .select([
                'SUM(lb.Lobby_Commision_Amount)"Admin_Commision"'])
            .from(Lobby, 'lb')
            .getRawMany();

        let data = {data1, total};
        //console.log("all users:", data[0].User_Image.toString());
        console.log("all users:", data);
        if (data) {
            return res.status(200).json(data);
        }
        // return res.status(404).json({ message: 'Data not available' });
        let err = new NotFoundError("Data not available")
        return next(err);
    } catch (error) {
        console.log(error);
        throw new InternalServerError();
    }
}

//=======================Lobby Structure============================

async function createLobbyStructure(req, res, next){
    console.log('POST /api/admin/createlobbystructure API call made');
    console.log(req.body);
    const { ls_name, ls_entry_fees, ls_per_cut, ls_players, admin_id } = req.body;

    try {
        const val_err = validationResult(req);
        console.log(val_err);
        if (!val_err.isEmpty()) {
            let er = [];

            //return res.status(400).json({success: false, errors: val_err.array() });
            val_err.array().forEach(element => {
                er.push("data of "+element.param+" is "+element.msg+".")
                // return element
            });
            console.log("er: "+JSON.stringify(er));
            let err =  new WrongDataError(er);
            return next(err)
        }

        const connection = await DbUtils.getConnection();
        if(await connection.manager.findOne(Lobby_Structure, { where: { Lobby_Structure_Name : ls_name } })) {
            //return res.status(409).json({ message: 'The Lobby Structure Name has been taken, please try another'});
            let err =  new DuplicateDataError("The Lobby Structure Name has been taken, please try another");
            return next(err)
        }
        else if(await connection.manager.findOne(Lobby_Structure, { where: { Lobby_Entry_fees : ls_entry_fees, Lobby_Per_cut : ls_per_cut, No_of_Players : ls_players } })) {
            //return res.status(409).json({ message: 'The Details has been taken, please try another'});
            let err =  new DuplicateDataError("The Lobby Details has been taken, please try another");
            return next(err)
        }

        let ls = new Lobby_Structure();
        ls.Lobby_Structure_Name = ls_name;
        ls.Lobby_Entry_fees = ls_entry_fees;
        ls.Lobby_Per_cut = ls_per_cut;
        ls.No_of_Players = ls_players;
        ls.IsActive = true;
        let ad = await connection.manager.findOne(Admin, { where: { Admin_Id : admin_id } });
        ls.Admin = ad.Admin_Id;
        await connection.manager.save(ls);

        return res.status(200).json({ message: 'Lobby Structure added successfully' });
    } catch (error) {
        console.log(error);
        throw new InternalServerError();
    }
}

async function AllLobbyStructure(req, res, next) {
    console.log('POST /api/admin/alllobbystructure API call made');
    const { admin_id } = req.body;

    try {
        const val_err = validationResult(req);
        console.log(val_err);
        if (!val_err.isEmpty()) {
            let er = [];

            //return res.status(400).json({success: false, errors: val_err.array() });
            val_err.array().forEach(element => {
                er.push("data of "+element.param+" is "+element.msg+".")
                // return element
            });
            console.log("er: "+JSON.stringify(er));
            let err =  new WrongDataError(er);
            return next(err)
        }

        const connection = await DbUtils.getConnection();
        let data1 = await connection.createQueryBuilder()
            .select(['lbs.Lobby_Structure_Id"Lobby_Structure_Id"', 'lbs.Lobby_Structure_Name"Lobby_Structure_Name"',
                'lbs.Lobby_Entry_fees"Lobby_Entry_fees"','lbs.Lobby_Per_cut"Lobby_Per_cut"',
                'lbs.No_of_Players"No_of_Players"','lbs.IsActive"IsActive"'])
            .from(Lobby_Structure, 'lbs')
            .leftJoin('lbs.Admin', 'ad')
            .where("ad.Admin_Id = :id", { id: admin_id})
            .andWhere("lbs.IsActive = :ida", { ida: true })
            .getRawMany();
        let data2 = await connection.createQueryBuilder()
            .select(['lbs.Lobby_Structure_Id"Lobby_Structure_Id"', 'lbs.Lobby_Structure_Name"Lobby_Structure_Name"',
                'lbs.Lobby_Entry_fees"Lobby_Entry_fees"','lbs.Lobby_Per_cut"Lobby_Per_cut"',
                'lbs.No_of_Players"No_of_Players"','lbs.IsActive"IsActive"', 'ad.Admin_Name"Admin_Name"'])
            .from(Lobby_Structure, 'lbs')
            .leftJoin('lbs.Admin', 'ad')
            .where("lbs.IsActive = :id", { id: true })
            .getRawMany();

        let data = {"Admin":data1,"All":data2};
        //console.log("all users:", data[0].User_Image.toString());
        console.log("all users:", data);
        if (data) {
            return res.status(200).json(data);
        }
        // return res.status(404).json({ message: 'Data not available' });
        let err = new NotFoundError("Data not available")
        return next(err);
    } catch (error) {
        console.log(error);
        throw new InternalServerError();
    }
}

async function BannedAllLobbyStructure(req, res, next) {
    console.log('POST /api/admin/bannedalllobbystructure API call made');
    const { admin_id } = req.body;

    try {
        const val_err = validationResult(req);
        console.log(val_err);
        if (!val_err.isEmpty()) {
            let er = [];

            //return res.status(400).json({success: false, errors: val_err.array() });
            val_err.array().forEach(element => {
                er.push("data of "+element.param+" is "+element.msg+".")
                // return element
            });
            console.log("er: "+JSON.stringify(er));
            let err =  new WrongDataError(er);
            return next(err)
        }

        const connection = await DbUtils.getConnection();
        let data1 = await connection.createQueryBuilder()
            .select(['lbs.Lobby_Structure_Id"Lobby_Structure_Id"', 'lbs.Lobby_Structure_Name"Lobby_Structure_Name"',
                'lbs.Lobby_Entry_fees"Lobby_Entry_fees"','lbs.Lobby_Per_cut"Lobby_Per_cut"',
                'lbs.No_of_Players"No_of_Players"','lbs.IsActive"IsActive"'])
            .from(Lobby_Structure, 'lbs')
            .leftJoin('lbs.Admin', 'ad')
            .where("ad.Admin_Id = :id", { id: admin_id})
            .andWhere("lbs.IsActive = :ida", { ida: false })
            .getRawMany();
        let data2 = await connection.createQueryBuilder()
            .select(['lbs.Lobby_Structure_Id"Lobby_Structure_Id"', 'lbs.Lobby_Structure_Name"Lobby_Structure_Name"',
                'lbs.Lobby_Entry_fees"Lobby_Entry_fees"','lbs.Lobby_Per_cut"Lobby_Per_cut"',
                'lbs.No_of_Players"No_of_Players"','lbs.IsActive"IsActive"', 'ad.Admin_Name"Admin_Name"'])
            .from(Lobby_Structure, 'lbs')
            .leftJoin('lbs.Admin', 'ad')
            .where("lbs.IsActive = :id", { id: false })
            .getRawMany();

        let data = {"Admin":data1,"All":data2};
        //console.log("all users:", data[0].User_Image.toString());
        console.log("all users:", data);
        if (data) {
            return res.status(200).json(data);
        }
        // return res.status(404).json({ message: 'Data not available' });
        let err = new NotFoundError("Data not available")
        return next(err);
    } catch (error) {
        console.log(error);
        throw new InternalServerError();
    }
}

async function updateLobbyStructure(req, res, next) {
    console.log('PUT /api/admin/updatelobbystructure API call made');
    const { ls_id, ls_name, ls_entry_fees, ls_per_cut, ls_players } = req.body;

    try {
        const val_err = validationResult(req);
        console.log(val_err);
        if (!val_err.isEmpty()) {
            let er = [];

            //return res.status(400).json({success: false, errors: val_err.array() });
            val_err.array().forEach(element => {
                er.push("data of "+element.param+" is "+element.msg+".")
                // return element
            });
            console.log("er: "+JSON.stringify(er));
            let err =  new WrongDataError(er);
            return next(err)
        }

        const connection = await DbUtils.getConnection();
        const exists = await connection.manager.findOne(Lobby_Structure, { where: { Lobby_Structure_Id : ls_id } })
        if(!exists) {
            //return res.status(404).json({ message: 'Lobby Structure is not exist'});
            let err = new NotFoundError("Lobby Structure is not exist")
            return next(err);
        }
        const data = await connection
            .createQueryBuilder()
            .update(Lobby_Structure)
            .set({
                Lobby_Structure_Name : ls_name,
                Lobby_Entry_fees : ls_entry_fees,
                Lobby_Per_cut : ls_per_cut,
                No_of_Players : ls_players
            })
            .where("Lobby_Structure_Id = :id", { id: ls_id })
            .execute();

        return res.status(200).json({ message: 'Lobby Structure data updated successfully' });

    } catch (error) {
        console.log(error);
        throw new InternalServerError();

    }
}

async function updateActiveLobbyStructure(req, res, next) {
    console.log('PUT /api/admin/updateactivelobbystructure API call made');
    const { ls_id, is_active } = req.body;
    console.log(req.body);
    try {
        const val_err = validationResult(req);
        console.log(val_err);
        if (!val_err.isEmpty()) {
            let er = [];

            //return res.status(400).json({success: false, errors: val_err.array() });
            val_err.array().forEach(element => {
                er.push("data of "+element.param+" is "+element.msg+".")
                // return element
            });
            console.log("er: "+JSON.stringify(er));
            let err =  new WrongDataError(er);
            return next(err)
        }

        const connection = await DbUtils.getConnection();
        const exists = await connection.manager.findOne(Lobby_Structure, { where: { Lobby_Structure_Id : ls_id } })
        if(!exists) {
            //return res.status(404).json({ message: 'Lobby Structure is not exist'});
            let err = new NotFoundError("Lobby Structure is not exist")
            return next(err);
        }
        const data = await connection
            .createQueryBuilder()
            .update(Lobby_Structure)
            .set({
                IsActive : Boolean(is_active)
            })
            .where("Lobby_Structure_Id = :id", { id: ls_id })
            .execute();
        console.log(data);
        return res.status(200).json({ message: 'Lobby Structure Active data updated successfully' });

    } catch (error) {
        console.log(error);
        throw new InternalServerError();

    }
}

async function deleteLobbyStructure(req, res, next) {
    console.log('DELETE /api/admin/deletelobbystructure API call made');
    const { ls_id } = req.body;
    try {
        const val_err = validationResult(req);
        console.log(val_err);
        if (!val_err.isEmpty()) {
            let er = [];

            //return res.status(400).json({success: false, errors: val_err.array() });
            val_err.array().forEach(element => {
                er.push("data of "+element.param+" is "+element.msg+".")
                // return element
            });
            console.log("er: "+JSON.stringify(er));
            let err =  new WrongDataError(er);
            return next(err)
        }

        const connection = await DbUtils.getConnection();
        const exists = await connection.manager.findOne(Lobby_Structure, { where: { Lobby_Structure_Id: ls_id  } });
        if(!exists) {
            //return res.status(404).json({ message: 'Lobby Structure is not exist'});
            let err = new NotFoundError("Lobby Structure is not exist")
            return next(err);
        }

        //delete user data
        let data = await connection.getRepository(Lobby_Structure).createQueryBuilder()
            .delete()
            .where("Lobby_Structure_Id = :id", { id: ls_id  })
            .execute();

        // console.log("delete User: ", data);

        if(await connection.manager.findOne(Lobby_Structure, { where: { Lobby_Structure_Id: ls_id  } })) {
            //return res.status(304).json({ message: 'Lobby Structure not deleted' });
            let err = new NotModifiedError("Lobby Structure not deleted")
            return next(err);
        }
        return res.status(200).json({ message: 'Lobby Structure deleted successfully' });
    } catch (error) {
        console.log(error);
        throw new InternalServerError();
    }
}

//=======================Dashboard============================

async function adminDashboard(req, res, next) {
    console.log('GET /api/admin/dashboard API call made');
    try {
        const connection = await DbUtils.getConnection();

        let Usercount = await connection.manager.count(User);

        let varcount = await connection.manager.count(Variation);

        let Paymentcount = await connection.manager.count(Payment);

        let Admincount = await connection.manager.count(Admin);

        let lobby = await connection.manager.count(Lobby);

        let achievements = await connection.manager.count(Achievements);

        let lscount = await connection.manager.count(Lobby_Structure);

        let leisure = await connection.manager.count(Lobby_History, { where: { Winning_Status : Not("Loss") } });


        let data = [{"User":Usercount , "Variation":varcount, "Withdrawls":Paymentcount, "Commission":lobby, "Admin":Admincount, "Lobbies":lobby, "leisure":leisure, "Achievements":achievements, "Lobby_Structure":lscount }];

        console.log("length: ", data);
        if (data) {
            return res.status(200).json({data});
        }
        //return res.status(200).json({ data: [] });
        // return res.status(404).json({ message: 'Data not available' });
        let err = new NotFoundError("Data not available")
        return next(err);
    } catch (error) {
        console.log(error);
        throw new InternalServerError();
    }
}

async function adminRegenrateToken(req, res, next) {
    console.log("PUT /admin/regeneratetoken API call made");
    const { Admin_Name, Admin_Id, Admin_Roles} = req.body;
    try {
        const val_err = validationResult(req);
        console.log(val_err);
        if (!val_err.isEmpty()) {
            let er = [];

            //return res.status(400).json({success: false, errors: val_err.array() });
            val_err.array().forEach(element => {
                er.push("data of "+element.param+" is "+element.msg+".")
                // return element
            });
            console.log("er: "+JSON.stringify(er));
            let err =  new WrongDataError(er);
            return next(err)
        }

        const connection = await DbUtils.getConnection();
        const exists = await connection.manager.findOne(Admin, { where: { Admin_Id : Admin_Id } })
        if(!exists) {
            //return res.status(404).json({ message: 'Lobby Structure is not exist'});
            let err = new NotFoundError("Lobby Structure is not exist")
            return next(err);
        }
        const token = jwt.sign(
            { userId: Admin_Id, username: Admin_Name, role: Admin_Roles},
            config.jwtSecret,
            { expiresIn: "1h" }
        );
        const data = await connection
            .createQueryBuilder()
            .update(Admin)
            .set({
                Token : token
            })
            .where("Admin_Id = :id", { id: Admin_Id })
            .execute();
        console.log("token:", token);
        return res.status(200).json( [{
            "Admin_Id": Admin_Id,
            "Admin_Name": Admin_Name,
            "Admin_Roles": Admin_Roles,
            "Token": token
        }])
    } catch (error) {
        console.log(error);
        throw new InternalServerError();
    }
}

export const adminController = {userDetails, bannedUsers, usersWallet, userPayments, CreateVariatoin,
    Variations, userSearchLobbyHistoryData, userSearchUserLobbyHistoryData, createAchievements, createAdmin,
    adminData, searchAdminData, updateAdminData, deleteAdmin, adminLogin, AllAchievements, AllPayments,
    AllLobbyStructure, updateAchievementsData, adminDashboard, userAllDetails,
    BannedadminData, updateVariationsData, deleteVariation, SearchVariation, deleteAchievements,
    createLobbyStructure, updateLobbyStructure, deleteLobbyStructure, updateActiveLobbyStructure,updateBannedAdminData,
    AllLobbies, AllLobbiesCommision, updateActiveUser, LeisureData, LeisureAllData, userLeisureData,
    adminRegenrateToken, BannedAllLobbyStructure, userPreviledgeAllDetails, updatePriviledgeUser }

