import { DbUtils } from "../utils/DbUtils";
import { User } from "../entity/User";
import { Lobby } from "../entity/Lobby";
import { InternalServerError, DuplicateDataError, BannedError, NotModifiedError, NotFoundError, WrongDataError } from '../errors/InstancesCE'
import { Lobby_History } from "../entity/Lobby_History";
import { Auth } from "../entity/Auth";
import { Payment } from "../entity/Payment";
import { Variation } from "../entity/Variation";
import { Wallet } from "../entity/Wallet";
import { Buddies } from "../entity/Buddies";
const TokenGenerator = require('uuid-token-generator');
let tokenGenerator = new TokenGenerator();
var Passwordhash = require('password-hash');
import { GameData } from "../entity/GameData";
import { Bank } from "../entity/Bank";
import { UPI } from "../entity/UPI";
import { Achievements } from "../entity/Achievements";
import { Inventory } from "../entity/Inventory";
import { Lobby_Structure } from "../entity/Lobby_Structure";
import path from 'path';
import { json } from "express";
const generate = require('nanoid/generate');
import fs from 'fs';
const imagepath = '../media/';

//=======================Registration & Login============================

async function userRegister(req, res, next){
    console.log('POST /api/user/register API call made');

    const { user_name, user_displayname, user_password, user_country, user_mob_no, user_image, reference_by} = req.body;

    console.log(req.body);
    try {
        const connection = await DbUtils.getConnection();
        if(await connection.manager.findOne(User, { where: { User_Name : user_name } })) {
            // return res.status(409).json({ message: 'The Username has been taken, please try another'});
            let err =  new DuplicateDataError("The UserName has been taken, please try another");
            return next(err)
        }
        else if(await connection.manager.findOne(User, { where: { User_Mobile_Number :  user_mob_no } })) {
            // return res.status(409).json({ message: 'The Mobile number has been taken, please try another'});
            let err =  new DuplicateDataError("The Mobile number has been taken, please try another");
            return next(err)
        }

        let user = new User();
        let auth = new Auth();
        let gamedata = new GameData();
        let wallet = new Wallet();
        let inv = new Inventory();

        let user_id = parseInt(Math.random().toFixed(7).replace("0.",""));
        while(await connection.manager.findOne(User, { where: { User_Id : user_id } })){
            console.log("user_id exist.");
            user_id = parseInt(Math.random().toFixed(7).replace("0.",""));
        }

        user.User_Id = user_id;
        user.User_Name = user_name;
        user.User_DisplayName = user_displayname;
        user.User_Password = Passwordhash.generate(user_password); //to convert password in hash
        user.User_Country = user_country;
        user.User_Mobile_Number = user_mob_no;

        //image decoding & uploading code
        const file_ = `${user_id}_Profile.png`
        console.log(file_)
        let path_ = path.join(__dirname,imagepath+file_)
        console.log(path_)

        fs.writeFile(path_, user_image, 'base64', (err)=>{
            if(err){
                throw new Error(err.message)
            }
        })
        user.User_Image = file_;//add image url
        user.IsActive = true;
        user.Priviledge = false;
        let referralcode = user_name.substring(0,3).toUpperCase()+generate('1234567890abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ', 7);
        while(await connection.manager.findOne(User, { where: { Reference_Id : referralcode } })){
            referralcode = user_name.substring(0,3).toUpperCase()+generate('1234567890abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ', 7);
        }
        user.Reference_Id = referralcode;
        let ref = await connection.manager.findOne(User, { where: { Reference_Id : reference_by } });
        if(ref){
            user.Reference_By = ref.User_Id;
        }
        user.Fb_User_Id = null;
        await connection.manager.save(user);

        wallet.User_Amount = 10000;
        wallet.User_Bonus_Cash = 0;
        wallet.User_Credit_Amount = 0;
        wallet.User_Debit_Amount = 0;
        wallet.User_Win_Amount = 0;
        wallet.User_Loss_Amount = 0;
        wallet.User = user.User_Id;
        await connection.manager.save(wallet);

        auth.Token = tokenGenerator.generate();
        auth.Currently_Playing = false;
        auth.User = user.User_Id;
        await connection.manager.save(auth);

        gamedata.Total_Wins = 0;
        gamedata.Total_Loss = 0;
        gamedata.Token_Captured = 0;
        gamedata.Opp_Token_Captured = 0;
        gamedata.Achievements = null;
        gamedata.User = user.User_Id;
        await connection.manager.save(gamedata);

        // var emojiObj = {Purchased:{id:[1,2,3,4,5,6,7,8,9,10,11,12]}};
        // var diceObj = {Selected:{id:1}, Purchased:{id:[1]}};
        // inv.Emoji = JSON.stringify(emojiObj);
        // inv.Dice = JSON.stringify(diceObj);
        // fs.writeFileSync('./s.txt', inv.Emoji)
        // console.log(inv.Emoji);
        // console.log(inv.Dice);
        // inv.User = user.User_Id;
        // await connection.manager.save(inv);

        inv.Emoji = null;
        inv.Dice = null;
        inv.User = user.User_Id;
        await connection.manager.save(inv);

        const data = await connection
            .createQueryBuilder()
            .update(Inventory)
            .set({
                Emoji : {"Purchased":{"id":[1,2,3,4,5,6,7,8,9,10,11,12]}},
                Dice : {"Selected":{"id":1}, "Purchased":{"id":[1]}}
            })
            .where("User = :id", { id: user.User_Id })
            .execute();

        return res.status(200).json({ Issuccessful: true ,user_id:user.User_Id });
    } catch (error) {
        console.log(error);
        throw new InternalServerError();
    }
}

async function verifyMobile(req, res, next) {
    console.log('POST /api/user/verifymobile API call made');
    const { user_mob_no } = req.body;

    try {
        const connection = await DbUtils.getConnection();
        const verifyuser = await connection.manager.findOne(User, { where: { User_Mobile_Number : user_mob_no } });
        if(verifyuser) {
            return res.status(200).json({ Issuccessful: true , user_id : verifyuser.User_Id });
        }
        // return res.status(400).json({ Issuccessful: false });
        let err = new WrongDataError("Issuccessful: false")
        return next(err);
    } catch (error) {
        console.log(error);
        throw new InternalServerError();
    }
}

async function checkReferall(req, res, next) {
    console.log('POST /api/user/checkreferral API call made');
    const { reference_id } = req.body;

    try {
        const connection = await DbUtils.getConnection();
        if(await connection.manager.findOne(User, { where: { Reference_Id : reference_id } })) {
            return res.status(200).json({ Issuccessful: true });
        }

        // return res.status(400).json({ Issuccessful: false });
        let err = new WrongDataError("Issuccessful: false")
        return next(err);
    } catch (error) {
        throw new InternalServerError();
    }
}

async function userLogin(req, res, next) {
    console.log('POST /api/user/login API call made');

    const { user_name, user_password, device_id} = req.body;
    try {
        const connection = await DbUtils.getConnection();
        const loginuser = await connection.manager.findOne(User, { where: [{ User_Name : user_name }, { User_Mobile_Number : user_name}] });
        //console.log(Passwordhash.generate(loginuser.User_Password));
        if(loginuser && (Passwordhash.verify(user_password, loginuser.User_Password))) {
            if(!loginuser.IsActive)
            {
                //return res.status(403).json({ message: 'Your account is Banned' });
                let err = new BannedError("Your account is Banned")
                return next(err);
            }
            const exists = await connection.manager.findOne(Auth, { where: { User: loginuser.User_Id } });
            console.log(loginuser);
            if(!exists) {
                // return res.status(404).json({ message: 'User Token is not available'});
                let err = new NotFoundError("User Token is not available")
                return next(err);
            }
            if(exists.Currently_Playing == 1 && exists.Device_Id != null )
            {
                //return res.status(409).json({ message: 'This user is already login in other device'});
                let err =  new DuplicateDataError("This user is already login in other device");
                return next(err)
            }
            let tokengen =tokenGenerator.generate();
            const data = await connection.getRepository(Auth)
                .createQueryBuilder()
                .update(Auth)
                .set({
                    Token : tokengen,
                    Currently_Playing : false,
                    Device_Id : device_id
                })
                .where("User = :id", { id: loginuser.User_Id })
                .execute();
            const temp = await connection.manager.findOne(Auth, { where: { User: loginuser.User_Id } });
            //image fetching from user image url
            return res.status(200).json({ Issuccessful: true , user_id : loginuser.User_Id, token : temp.Token });
        }

        // return res.status(400).json({ message: 'Incorrect User ID or Password', Issuccessful: false });
        let err = new WrongDataError("message: 'Incorrect User ID or Password', Issuccessful: false")
        return next(err);
    } catch (error) {
        console.log(error);
        throw new InternalServerError();
    }
}

async function enterExitLobby(req, res, next) {
    console.log('PUT /api/user/enterexitLobby API call made');
    const { user_id, playing } = req.body;

    try {
        const connection = await DbUtils.getConnection();
        const exists = await connection.manager.findOne(Auth, { where: { User: user_id } });
        if(!exists) {
            // return res.status(404).json({ message: 'User Token is not available'});
            let err = new NotFoundError("User Token is not available")
            return next(err);
        }

        const data = await connection.getRepository(Auth)
            .createQueryBuilder()
            .update(Auth)
            .set({
                Currently_Playing : playing
            })
            .where("User = :id", { id: user_id })
            .execute();

        return res.status(200).json({ Issuccessful: true , user_id : user_id, token : data.Token });
    } catch (error) {
        console.log(error);
        throw new InternalServerError();
    }
}

async function userLogout(req, res, next) {
    console.log('PUT /api/user/logout API call made');
    const { user_id } = req.body;

    try {
        const connection = await DbUtils.getConnection();
        const exists = await connection.manager.findOne(Auth, { where: { User: user_id } });
        if(!exists) {
            // return res.status(404).json({ message: 'User Token is not available'});
            let err = new NotFoundError("User Token is not available")
            return next(err);
        }
        const data = await connection.getRepository(Auth)
            .createQueryBuilder()
            .update(Auth)
            .set({
                Currently_Playing : false,
                Device_Id : null
            })
            .where("User = :id", { id: user_id })
            .execute();

        return res.status(200).json({ Issuccessful: true , user_id : user_id, token : data.Token });

    } catch (error) {
        throw new InternalServerError();
    }
}

//=======================User Profile============================

async function updateProfile(req, res, next) {
    console.log('PUT /api/user/updateprofile API call made');
    const {user_id, user_displayname, user_country, user_image} = req.body;

    try {
        const connection = await DbUtils.getConnection();
        const exists = await connection.manager.findOne(User, { where: { User_Id: user_id } });
        if(!exists) {
            //return res.status(404).json({ message: 'User is not registered with us, please try another'});
            let err =  new NotFoundError("User is not registered with us, please try another");
            return next(err);
        }
        //image decoding & uploading code
        const file_ = `${user_id}_Profile.png`
        console.log(file_)
        let path_ = path.join(__dirname,imagepath+file_)
        console.log(path_)

        fs.writeFile(path_, user_image, 'base64', (err)=>{
            if(err){
                throw new Error(err.message)
            }
        })

        const data = await connection.getRepository(User)
            .createQueryBuilder()
            .update(User)
            .set({
                User_DisplayName : user_displayname,
                User_Country : user_country,
                User_Image : path_
            })
            .where("User_Id = :id", { id: user_id })
            .execute();

        return res.status(200).json({ message: 'User updated successfully' });
    } catch (error) {
        throw new InternalServerError();

    }
}

async function changePassword(req, res, next) {
    console.log('PUT /api/user/changepassword API call made');

    const {user_id, user_password} = req.body;
    try {
        const connection = await DbUtils.getConnection();
        const exists = await connection.manager.findOne(User, { where: { User_Id: user_id } });
        if(!exists) {
            // return res.status(404).json({ message: 'User is not registered with us, please try another'});
            let err =  new NotFoundError("User is not registered with us, please try another");
            return next(err)
        }

        const data = await connection.getRepository(User)
            .createQueryBuilder()
            .update(User)
            .set({
                User_Password : Passwordhash.generate(user_password)//to convert password in hash
            })
            .where("User_Id = :id", { id: user_id })
            .execute();

        return res.status(200).json({ message: 'User Password updated successfully' });
    } catch (error) {
        console.log(error);
        throw new InternalServerError();
    }
}

async function activeUser(req, res, next) {
    console.log('PUT /api/user/activeuser API call made');

    const {user_id, active} = req.body;
    try {
        const connection = await DbUtils.getConnection();
        const exists = await connection.manager.findOne(User, { where: { User_Id: user_id } });
        if(!exists) {
            // return res.status(404).json({ message: 'User is not registered with us, please try another'});
            let err =  new NotFoundError("User is not registered with us, please try another");
            return next(err)
        }

        const data = await connection.getRepository(User)
            .createQueryBuilder()
            .update(User)
            .set({
                IsActive : active
            })
            .where("User_Id = :id", { id: user_id })
            .execute();

        return res.status(200).json({ message: 'User Active status Updated successfully' });
    } catch (error) {
        console.log(error);
        throw new InternalServerError();
    }
}

async function updateReferral(req, res, next) {
    console.log('POST /api/user/updatereferral API call made');
    const { user_id, reference_id} = req.body;

    try {
        const connection = await DbUtils.getConnection();

        const exists = await connection.manager.findOne(User, { where: { User_Id: user_id } });
        if(!exists) {
            // return res.status(404).json({ message: 'User is not registered with us, please try another'});
            let err =  new NotFoundError("User is not registered with us, please try another");
            return next(err)
        }

        const ref = await connection.manager.findOne(User, { where: { Reference_Id: reference_id } });
        const data = await connection.getRepository(User)
            .createQueryBuilder()
            .update(User)
            .set({
                Reference_By : ref.User_Id
            })
            .where("User_Id = :id", { id: user_id })
            .execute();

        return res.status(200).json({ message: 'User Reference updated successfully' });
    } catch (error) {
        console.log(error);
        throw new InternalServerError();

    }
}

async function searchUserImage(req, res, next) {
    console.log('POST /api/user/searchuserimage API call made');
    const { filename } = req.body;
    try {
        const connection = await DbUtils.getConnection();
        let data = fs.readFileSync(path.join(__dirname+'/'+imagepath)+filename);

        // res.setHeader('Content-Type','image/png');
        // console.log(path.join(__dirname+'/'+imagepath))
        // return res.sendFile(filename, {root:path.join(__dirname+'/'+imagepath)})

        console.log("all users:", data);
        if (data) {
            //resolve(data);
            return res.status(200).json(data);
        }
        console.log("Data not available");
        //reject ("Data not available");
        // return res.status(404).json({ message: 'Data not available' });
        let err = new NotFoundError("Data not available")
        return next(err);
    }
    catch (error)
    {
        console.log(error);
        //reject(error);
        throw new InternalServerError();
    }
}

async function searchUserData(req, res, next) {
    console.log('POST /api/user/searchuserdata API call made');
    const { user_id } = req.body;

    try {
        const connection = await DbUtils.getConnection();
        let data = await connection.createQueryBuilder()
            .select(['u.User_Id"User_Id"', 'u.User_Name"Username"',
                'u.User_DisplayName"User_DisplayName"', 'u.User_Country"User_Country"',
                'u.User_Image"User_Image"'])
            .from(User, 'u')
            .where("u.User_Id = :id", { id: user_id})
            .getRawMany();

        //console.log("all users:", data[0].User_Image.toString());
        console.log("all users:", data);
        if (data) {
            return res.status(200).json(data);
        }
        // return res.status(404).json({ message: 'Data not available' });
        let err = new NotFoundError("Data not available")
        return next(err);
    }catch (error) {
        console.log(error);
        throw new InternalServerError();
    }
}

async function searchUserDetail(req, res, next) {
    console.log('POST /api/user/searchuserdetail API call made');
    const { user_id } = req.body;

    try {
        const connection = await DbUtils.getConnection();
        let data = await connection.createQueryBuilder()
            .select(['u.User_Id"User_Id"', 'u.User_Name"Username"',
                'u.User_DisplayName"User_DisplayName"', 'u.User_Country"User_Country"',
                'u.User_Image"User_Image"', 'w.User_Amount"User_Amount"',
                'w.User_Bonus_Cash"User_Bonus_Cash"', 'w.User_Credit_Amount"User_Credit_Amount"',
                'w.User_Debit_Amount"User_Debit_Amount"','w.User_Win_Amount"User_Win_Amount"',
                'w.User_Loss_Amount"User_Loss_Amount"',  'g.Total_Wins"Total_Wins"', 'g.Total_Loss"Total_Loss"',
                'g.Token_Captured"Token_Captured"',
                'g.Opp_Token_Captured"Opp_Token_Captured"'])
            .from(User, 'u')
            .leftJoin('u.Wallet', 'w')
            .leftJoin('u.GameData', 'g')
            .where("u.User_Id = :id", { id: user_id})
            .getRawMany();

        //console.log("all users:", data[0].User_Image.toString());
        console.log("all users:", data);
        if (data) {
            return res.status(200).json(data);
        }
        // return res.status(404).json({ message: 'Data not available' });
        let err = new NotFoundError("Data not available")
        return next(err);
    }catch (error) {
        console.log(error);
        throw new InternalServerError();
    }
}


//=======================Wallet============================

async function userWallet(req, res, next) {
    console.log('POST /api/user/userwallet API call made');
    const { user_id } = req.body;

    try {
        const connection = await DbUtils.getConnection();
        let data = await connection.createQueryBuilder()
            .select(['u.User_Id"User_Id"', 'w.User_Amount"User_Amount"',
                'w.User_Bonus_Cash"User_Bonus_Cash"', 'w.User_Credit_Amount"User_Credit_Amount"',
                'w.User_Debit_Amount"User_Debit_Amount"','w.User_Win_Amount"User_Win_Amount"',
                'w.User_Loss_Amount"User_Loss_Amount"'])
            .from(Wallet, 'w')
            .leftJoin('w.User', 'u')
            .where("u.User_Id = :id", { id: user_id})
            .getRawMany();

        console.log("all users:", data);
        if (data) {
            return res.status(200).json(data);
        }
        // return res.status(404).json({ message: 'Data not available' });
        let err = new NotFoundError("Data not available")
        return next(err);
    }catch (error) {
        throw new InternalServerError();
    }
}

async function updateWalletData(req, res, next) {
    console.log('PUT /api/user/updatewalletdata API call made');
    const { user_id, user_amount, user_bonus, user_credit_amount, user_debit_amount, user_win_amount, user_loss_amount } = req.body;

    try {
        const connection = await DbUtils.getConnection();

        const exists = await connection.manager.findOne(User, { where: { User_Id: user_id } });
        if(!exists) {
            // return res.status(404).json({ message: 'User is not registered with us, please try another'});
            let err =  new NotFoundError("User is not registered with us, please try another");
            return next(err)
        }
        const chips = await connection.manager.findOne(Wallet, { where: { User: user_id } });
        const data = await connection.getRepository(Wallet)
            .createQueryBuilder()
            .update(Wallet)
            .set({
                User_Amount : (chips.User_Amount + user_amount),
                User_Bonus_Cash : ( chips.User_Bonus_Cash + user_bonus),
                User_Credit_Amount : ( chips.User_Credit_Amount + user_credit_amount),
                User_Debit_Amount : ( chips.User_Debit_Amount  + user_debit_amount),
                User_Win_Amount : (chips.User_Win_Amount + user_win_amount),
                User_Loss_Amount : (chips.User_Loss_Amount + user_loss_amount)
            })
            .where("User = :id", { id: user_id })
            .execute();

        return res.status(200).json({ message: 'Wallet updated successfully' });
    } catch (error) {
        throw new InternalServerError();

    }
}

//=======================Auth============================

async function userAuth(req, res, next) {
    console.log('POST /api/user/userauth API call made');
    const { user_id } = req.body;

    try {
        const connection = await DbUtils.getConnection();
        let data = await connection.createQueryBuilder()
            .select(['u.User_Id"User_Id"', 'a.Token"Token"',
                'a.Currently_Playing"Currently_Playing"', 'a.Device_Id"Device_Id"'])
            .from(Auth, 'a')
            .leftJoin('a.User', 'u')
            .where("u.User_Id = :id", { id: user_id})
            .getRawMany();

        console.log("all users:", data);
        if (data) {
            return res.status(200).json(data);
        }
        // return res.status(404).json({ message: 'Data not available' });
        let err = new NotFoundError("Data not available")
        return next(err);
    }catch (error) {
        throw new InternalServerError();
    }
}

async function updateAuthData(req, res, next) {
    console.log('PUT /api/user/updateauthdata API call made');
    const { user_id, user_token, playing, device_id } = req.body;

    try {
        const connection = await DbUtils.getConnection();
        const exists = await connection.manager.findOne(User, { where: { User_Id: user_id } });
        if(!exists) {
            // return res.status(404).json({ message: 'User is not registered with us, please try another'});
            let err =  new NotFoundError("User is not registered with us, please try another");
            return next(err)
        }
        const data = await connection.getRepository(Auth)
            .createQueryBuilder()
            .update(Auth)
            .set({
                Token : user_token,
                Currently_Playing : playing,
                Device_Id : device_id
            })
            .where("User = :id", { id: user_id })
            .execute();

        return res.status(200).json({ message: 'Auth updated successfully' });
    }catch (error) {
        throw new InternalServerError();

    }
}

//=======================Game Data============================

async function userGameData(req, res, next) {
    console.log('POST /api/user/usergamedata API call made');
    const { user_id } = req.body;

    try {
        const connection = await DbUtils.getConnection();

        let data = await connection.createQueryBuilder()
            .select(['u.User_Id"User_Id"',
                'g.Total_Wins"Total_Wins"', 'g.Total_Loss"Total_Loss"',
                'g.Token_Captured"Token_Captured"',
                'g.Opp_Token_Captured"Opp_Token_Captured"', 'g.Achievements"Achievements"'])
            .from(GameData, 'g')
            .leftJoin('g.User', 'u')
            .where("u.User_Id = :id", { id: user_id})
            .getRawMany();

        console.log("all users:", data);
        if (data) {
            return res.status(200).json(data);
        }
        // return res.status(404).json({ message: 'Data not available' });
        let err = new NotFoundError("Data not available")
        return next(err);
    }catch (error) {
        throw new InternalServerError();
    }
}

async function updateGameData(req, res, next) {
    console.log('PUT /api/user/updategamedata API call made');
    const {user_id, total_wins, total_loss, token_captured, opp_token_captured, achievements} = req.body;

    try {
        const connection = await DbUtils.getConnection();

        const exists = await connection.manager.findOne(User, { where: { User_Id: user_id } });
        if(!exists) {
            // return res.status(404).json({ message: 'User is not registered with us, please try another'});
            let err =  new NotFoundError("User is not registered with us, please try another");
            return next(err)
        }
        const gem = await connection.manager.findOne(GameData, { where: { User: user_id } });
        // console.log("ach: ",gem.Achievements.id);
        const data = await connection.getRepository(GameData)
            .createQueryBuilder()
            .update(GameData)
            .set({
                Total_Wins : ( gem.Total_Wins + total_wins),
                Total_Loss : ( gem.Total_Loss + total_loss),
                Token_Captured : ( gem.Token_Captured + token_captured),
                Opp_Token_Captured : ( gem.Opp_Token_Captured + opp_token_captured),
                Achievements : achievements
                //SON.parse(achievements)
            })
            .where("User = :id", { id: user_id })
            .execute();
        return res.status(200).json({ message: 'GameData updated successfully' });
    } catch (error) {
        throw new InternalServerError();

    }
}

async function createPayment(req, res, next) {
    console.log('POST /api/user/createpayment API call made');

    const { user_id, iscredit, payment_amount, transaction_id } = req.body;
    try {
        console.log(req.body);
        const connection = await DbUtils.getConnection();
        let user = await connection.manager.findOne(User, { where: { User_Id : user_id } });
        console.log(user);
        let wallet = await connection.manager.findOne(Wallet, { where: { User : user.User_Id } });
        console.log(wallet);
        if(!user)
        {
            //return res.status(404).json({ message: 'User not exist' });
            let err =  new NotFoundError("User not exist");
            return next(err);
        }
        if((parseFloat(wallet.User_Amount) >= parseFloat(payment_amount)) && (iscredit == "false"))
        {
            // return res.status(400).json({ message: 'Unsufficient balance' });
            let err = new WrongDataError("message: 'Unsufficient balance")
            return next(err);
        }
        let payment = new Payment();
        payment.User = user;
        if(iscredit == "true"){
            payment.Is_Credit = true;
            wallet.User_Amount = parseFloat(wallet.User_Amount) + parseFloat(payment_amount);
        }
        else{
            payment.Is_Credit = false;
            wallet.User_Amount = parseFloat(wallet.User_Amount) - parseFloat(payment_amount);
        }
        payment.Payment_Amount = payment_amount;
        payment.Transaction_Id = transaction_id;
        await connection.manager.save(payment);
        await connection.manager.save(wallet);
        return res.status(200).json({ message: 'Payment Created successfully' });


    } catch (error) {
        console.log(error);
        throw new InternalServerError();

    }
}

async function userPayment(req, res, next) {
    console.log('POST /api/user/userpayments API call made');
    const { user_id } = req.body;

    try {
        const connection = await DbUtils.getConnection();
        let data =
            await connection.createQueryBuilder()
                .select(['u.User_Id"User_Id"', 'u.User_Name"Username"',
                    'p.Is_Credit"Is_Credit"', 'p.Payment_Amount"Payment_Amount"',
                    'p.Payment_Time_Date"Payment_Time_Date"','p.Transaction_Id"Transaction_Id"'])
                .from(Payment, 'p')
                .leftJoin('p.User', 'u')
                .where("u.User_Id = :id", { id: user_id })
                .getRawMany();

        //console.log("all users:", data[0].User_Image.toString());
        console.log("all users:", data);
        if (data) {
            return res.status(200).json(data);
        }
        // return res.status(404).json({ message: 'Data not available' });
        let err = new NotFoundError("Data not available")
        return next(err);
    } catch (error) {
        throw new InternalServerError();
    }
}

//=======================Lobby============================

async function userCreateLobby(req, res, next) {
    console.log('POST /api/user/createlobby API call made');

    const { lobby_name, variation_name, player_count, lobby_total_amount, lobby_buy_in, lobby_commision_amount, room_id, lobby_status, lobby_owner } = req.body;

    try {
        const connection = await DbUtils.getConnection();
        if(await connection.manager.findOne(Lobby, { where: { Lobby_Name : lobby_name } })) {
            // return res.status(409).json({ message: 'The Lobby Name has been taken, please try another'});
            let err =  new DuplicateDataError("The Lobby Name has been taken, please try another");
            return next(err)
        }
        let vart = await connection.manager.findOne(Variation, { where: { Variation_Name : variation_name, Player_Count : player_count } });
        let lobby = new Lobby();
        lobby.Lobby_Name = lobby_name;
        lobby.Lobby_Total_Amount = lobby_total_amount;
        lobby.Lobby_Buy_In = lobby_buy_in;
        lobby.Lobby_Commision_Amount = lobby_commision_amount;
        lobby.Room_Id = room_id;
        lobby.Lobby_Status = lobby_status;
        lobby.Recorded_Move = null;
        lobby.Variation = vart.Variation_Id;
        let u = await connection.manager.findOne(User, { where: { User_Id : lobby_owner } });
        lobby.Lobby_Owner = u.User_Id;
        await connection.manager.save(lobby);

        return res.status(200).json({ message: 'Lobby Created successfully'});
    } catch (error) {
        console.log(error);
        throw new InternalServerError();
    }
}

async function userSearchLobbyData(req, res, next) {
    console.log('POST /api/user/searchlobbydata API call made');
    const { lobby_id } = req.body;

    try {
        const connection = await DbUtils.getConnection();
        let data = await connection.createQueryBuilder()
            .select(['lb.Lobby_Id"Lobby_Id"', 'lb.Lobby_Name"Lobby_Name"',
                'lb.Lobby_Total_Amount"Lobby_Total_Amount"','lb.Lobby_Buy_In"Lobby_Buy_In"',
                'lb.Lobby_Commision_Amount"Lobby_Commision_Amount"', 'lb.Recorded_Move"Recorded_Move"',
                'lb.Room_Id"Room_Id"','lb.Lobby_Status"Lobby_Status"',
                'lb.Lobby_Start_Time_Date"Lobby_Start_Time_Date"', 'lb.Lobby_End_Time_Date"Lobby_End_Time_Date"',
                'u.User_Name"Lobby_Owner"', 'v.Variation_Name"Variation_Name"' ])
            .from(Lobby, 'lb')
            .leftJoin('lb.Lobby_Owner', 'u')
            .leftJoin('lb.Variation', 'v')
            .where("lb.Lobby_Id = :id", { id: lobby_id})
            .getRawOne();

        //console.log("all users:", data[0].User_Image.toString());
        console.log("all users:", data);
        if (data) {
            return res.status(200).json(data);
        }
        // return res.status(404).json({ message: 'Data not available' });
        let err = new NotFoundError("Data not available")
        return next(err);
    } catch (error) {
        console.log(error);
        throw new InternalServerError();
    }
}

async function userSearchUserLobbyData(req, res, next) {
    console.log('POST /api/user/searchuserlobbydata API call made');
    const { user_id } = req.body;

    try {
        const connection = await DbUtils.getConnection();
        let data = await connection.createQueryBuilder()
            .select(['lb.Lobby_Id"Lobby_Id"', 'lb.Lobby_Name"Lobby_Name"',
                'lb.Lobby_Total_Amount"Lobby_Total_Amount"','lb.Lobby_Buy_In"Lobby_Buy_In"',
                'lb.Lobby_Commision_Amount"Lobby_Commision_Amount"', 'lb.Recorded_Move"Recorded_Move"',
                'lb.Room_Id"Room_Id"','lb.Lobby_Status"Lobby_Status"',
                'lb.Lobby_Start_Time_Date"Lobby_Start_Time_Date"', 'lb.Lobby_End_Time_Date"Lobby_End_Time_Date"',
                'u.User_Name"Lobby_Owner"', 'v.Variation_Name"Variation_Name"' ])
            .from(Lobby, 'lb')
            .leftJoin('lb.Lobby_Owner', 'u')
            .leftJoin('lb.Variation', 'v')
            .where("u.User_Id = :id", { id: user_id})
            .getRawMany();

        //console.log("all users:", data[0].User_Image.toString());
        console.log("all users:", data);
        if (data) {
            return res.status(200).json(data);
        }
        // return res.status(404).json({ message: 'Data not available' });
        let err = new NotFoundError("Data not available")
        return next(err);
    } catch (error) {
        throw new InternalServerError();
    }
}

async function userUpdateLobbyData(req, res, next) {
    console.log('PUT /api/user/updatelobbydata API call made');
    const { lobby_id, lobby_total_amount, lobby_buy_in, lobby_status } = req.body;

    try {
        const connection = await DbUtils.getConnection();
        const exists = await connection.manager.findOne(Lobby, { where: { Lobby_Id: lobby_id } });
        if(!exists) {
            // return res.status(404).json({ message: 'Lobby is not exist'});
            let err = new NotFoundError("Lobby is not exist")
            return next(err);
        }
        const data = await connection
            .createQueryBuilder()
            .update(Lobby)
            .set({
                Lobby_Total_Amount : lobby_total_amount,
                Lobby_Buy_In : lobby_buy_in,
                Lobby_Status : lobby_status,
                Recorded_Move : {name: "dfdf"},
                Lobby_End_Time_Date : () => 'CURRENT_TIMESTAMP'
            })
            .where("Lobby_Id = :id", { id: lobby_id })
            .execute();

        return res.status(200).json({ message: 'Lobby updated successfully' });

    } catch (error) {
        throw new InternalServerError();

    }
}

async function userDeleteLobby(req, res, next) {
    console.log('DELETE /api/user/deletelobby API call made');
    const { lobby_id } = req.body;
    try {
        const connection = await DbUtils.getConnection();
        const exists = await connection.manager.findOne(Lobby, { where: { Lobby_Id: lobby_id  } });
        if(!exists) {
            // return res.status(404).json({ message: 'Lobby is not exist'});
            let err = new NotFoundError("Lobby is not exist")
            return next(err);
        }

        //delete user data
        let data = await connection.getRepository(Lobby).createQueryBuilder()
            .delete()
            .where("Lobby_Id = :id", { id: lobby_id  })
            .execute();

        // console.log("delete User: ", data);

        if(await connection.manager.findOne(Lobby, { where: { Lobby_Id: lobby_id  } })) {
            //return res.status(304).json({ message: 'Lobby not deleted' });
            let err = new NotModifiedError("Lobby not deleted")
            return next(err);
        }
        return res.status(200).json({ message: 'Lobby deleted successfully' });
    } catch (error) {
        throw new InternalServerError();
    }
}

//=======================Lobby History============================

async function userCreateLobbyHistory(req, res, next) {
    console.log('POST /api/user/createlobbyhistory API call made');

    const { lobby_id, winning_status, winning_price, user_id, powerups_used, total_amount_spend } = req.body;

    try {
        const connection = await DbUtils.getConnection();
        if(!await connection.manager.findOne(Lobby, { where: { Lobby_Id : lobby_id } })) {
            // return res.status(404).json({ message: 'Lobby is not exist'});
            let err = new NotFoundError("Lobby is not exist")
            return next(err);
        }
        let lobby_history = new Lobby_History();
        lobby_history.Winning_Status = winning_status;
        lobby_history.Winning_Price = winning_price;
        let lb = await connection.manager.findOne(Lobby, { where: { Lobby_Id : lobby_id } });
        lobby_history.Lobby = lb.Lobby_Id;
        let u = await connection.manager.findOne(User, { where: { User_Id : user_id } });
        lobby_history.User = u.User_Id;
        lobby_history.Powerups_used = powerups_used;
        lobby_history.Total_Amount_Spend = total_amount_spend;
        await connection.manager.save(lobby_history);

        return res.status(200).json({ message: 'Lobby history Created successfully'});
    } catch (error) {
        console.log(error);
        throw new InternalServerError();
    }
}

async function userSearchLobbyHistoryData(req, res, next) {
    console.log('POST /api/user/searchlobbyhistorydata API call made');
    const { lobby_id } = req.body;

    try {
        const connection = await DbUtils.getConnection();
        let data = await connection.createQueryBuilder()
            .select(['lb.Lobby_Id"Lobby_Id"', 'lb.Lobby_Name"Lobby_Name"',
                'lbh.Winning_Status"Winning_Status"', 'lbh.Winning_Price"Winning_Price"',
                'lbh.Powerups_used"Powerups_used"', 'lbh.Total_Amount_Spend"Total_Amount_Spend"',
                'u.User_Name"User_Name"'])
            .from(Lobby_History, 'lbh')
            .leftJoin('lbh.Lobby', 'lb')
            .leftJoin('lb.Lobby_Owner', 'u')
            .where("lb.Lobby_Id = :id", { id: lobby_id})
            .getRawMany();

        console.log("all users:", data);
        if (data) {
            return res.status(200).json(data);
        }
        // return res.status(404).json({ message: 'Data not available' });
        let err = new NotFoundError("Data not available")
        return next(err);
    } catch (error) {
        console.log(error);
        throw new InternalServerError();
    }
}

async function userSearchUserLobbyHistoryData(req, res, next) {
    console.log('POST /api/user/searchuserlobbyhistorydata API call made');
    const { user_id } = req.body;

    try {
        const connection = await DbUtils.getConnection();
        let data = await connection.createQueryBuilder()
            .select(['lb.Lobby_Id"Lobby_Id"', 'lb.Lobby_Name"Lobby_Name"',
                'lbh.Winning_Status"Winning_Status"', 'lbh.Winning_Price"Winning_Price"',
                'lbh.Powerups_used"Powerups_used"', 'lbh.Total_Amount_Spend"Total_Amount_Spend"',
                'u.User_Name"User_Name"'])
            .from(Lobby_History, 'lbh')
            .leftJoin('lbh.Lobby', 'lb')
            .leftJoin('lbh.User', 'u')
            .where("u.User_Id = :id", { id: user_id})
            .getRawMany();

        console.log("all users:", data);
        if (data) {
            return res.status(200).json(data);
        }
        // return res.status(404).json({ message: 'Data not available' });
        let err = new NotFoundError("Data not available")
        return next(err);
    } catch (error) {
        console.log(error);
        throw new InternalServerError();
    }
}

//=======================Buddies============================
async function userCreateBuddies(req, res, next) {
    console.log('POST /api/user/createbuddies API call made');
    const { request_by, request_to } = req.body;

    try {
        const connection = await DbUtils.getConnection();
        if((!await connection.manager.findOne(User, { where: { User_Id : request_by } }))
            || (!await connection.manager.findOne(User, { where: { User_Id : request_to } }))){
            //return res.status(404).json({ message: 'User not exist' });
            let err =  new NotFoundError("User not exist");
            return next(err);
        }
        if(await connection.manager.findOne(Buddies, { where: { Request_by : request_by, Request_to: request_to } })){
            // return res.status(409).json({ message: 'Request already sended' });
            let err =  new DuplicateDataError("Request already sended");
            return next(err)
        }
        if(await connection.manager.findOne(Buddies, { where: { Request_by : request_to, Request_to: request_by } })){
            // return res.status(409).json({ message: 'Request already pending' });
            let err =  new DuplicateDataError("Request already pending");
            return next(err)
        }
        //blocked user condition

        let buddies = new Buddies();
        let u = await connection.manager.findOne(User, { where: { User_Id : request_by } });
        buddies.Request_by = u.User_Id;
        u = await connection.manager.findOne(User, { where: { User_Id : request_to } });
        buddies.Request_to = u.User_Id;
        await connection.manager.save(buddies);

        return res.status(200).json({ message: 'Buddies request added successfully' });
    } catch (error) {
        console.log(error);
        throw new InternalServerError()
    }
}

async function userBuddiesData(req, res, next) {
    console.log('POST /api/user/buddiesdata API call made');
    const { user_id } = req.body;

    try {
        const connection = await DbUtils.getConnection();
        let data = await connection.createQueryBuilder()
            .select(['bd.Buddies_Id"Buddies_Id"', 'rb.User_Name"User_Name"', 'w.User_Amount"User_Amount"'])
            .from(Buddies, 'bd')
            .leftJoin('bd.Request_by', 'rb')
            .leftJoin('bd.Request_to', 'rt')
            .leftJoin('rb.Wallet', 'w')
            .where("rt.User_Id = :id", { id: user_id})
            .andWhere("bd.Req_status = :a", { a: "Approved"})
            .getRawMany();

        //console.log("all users:", data[0].User_Image.toString());
        console.log("all users:", data);
        if (data) {
            return res.status(200).json(data);
        }
        // return res.status(404).json({ message: 'Data not available' });
        let err = new NotFoundError("Data not available")
        return next(err);

    } catch (error) {
        console.log(error);
        throw new InternalServerError()
    }
}

async function searchBuddiesData(req, res, next) {
    console.log('POST /api/user/searchbuddiesdata API call made');
    const { user_id, frnds_id } = req.body;

    try {
        const connection = await DbUtils.getConnection();
        let data = await connection.createQueryBuilder()
            .select(['bd.Buddies_Id"Buddies_Id"', 'rb.User_Name"User_Name"'])
            .from(Buddies, 'bd')
            .leftJoin('bd.Request_by', 'rb')
            .leftJoin('bd.Request_to', 'rt')
            .where("rt.User_Id = :id", { id: user_id})
            .where("rb.User_Id = :ida", { ida: frnds_id})
            .andWhere("bd.Req_status = :a", { a: "Approved"})
            .getRawMany();

        //console.log("all users:", data[0].User_Image.toString());
        console.log("all users:", data);
        if (data) {
            return res.status(200).json(data);
        }
        // return res.status(404).json({ message: 'Data not available' });
        let err = new NotFoundError("Data not available")
        return next(err);

    } catch (error) {
        console.log(error);
        throw new InternalServerError()
    }
}

async function requestBuddiesData(req, res, next) {
    console.log('POST /api/user/reqbuddiesdata API call made');
    const { user_id } = req.body;

    try {
        const connection = await DbUtils.getConnection();
        let data = await connection.createQueryBuilder()
            .select(['bd.Buddies_Id"Buddies_Id"', 'rb.User_Name"User_Name"'])
            .from(Buddies, 'bd')
            .leftJoin('bd.Request_by', 'rb')
            .leftJoin('bd.Request_to', 'rt')
            .where("rt.User_Id = :id", { id: user_id })
            .andWhere("bd.Req_status = :a", { a: "Pending"})
            .getRawMany();

        //console.log("all users:", data[0].User_Image.toString());
        console.log("all users:", data);
        if (data) {
            return res.status(200).json(data);
        }
        // return res.status(404).json({ message: 'Data not available' });
        let err = new NotFoundError("Data not available")
        return next(err);

    } catch (error) {
        console.log(error);
        throw new InternalServerError()
    }
}

async function updateBuddiesData(req, res, next) {
    console.log('PUT /api/user/updatebuddiesdata API call made');
    const { request_by, request_to, req_status } = req.body;

    try {
        const connection = await DbUtils.getConnection();
        const exists = await connection.manager.findOne(Buddies, { where: { Request_by: request_by, Request_to: request_to } });
        if(!exists) {
            // return res.status(404).json({ message: 'Buddies is not exist'});
            let err = new NotFoundError("Buddies is not exist")
            return next(err);
        }
        const data = await connection
            .createQueryBuilder()
            .update(Buddies)
            .set({
                Req_status : req_status
            })
            .where("Request_by = :id", { id: request_by })
            .andWhere("Request_to = :ids", { ids: request_to })
            .execute();

        return res.status(200).json({ message: 'Buddies updated successfully' });

    } catch (error) {
        console.log(error);
        throw new InternalServerError()

    }
}

async function deleteBuddies(req, res, next) {
    console.log('DELETE /api/user/deletebuddies API call made');
    const { request_by, request_to } = req.body;
    try {
        const connection = await DbUtils.getConnection();
        const exists = await connection.manager.findOne(Buddies, { where: { Request_by: request_by, Request_to: request_to } });
        if(!exists) {
            // return res.status(404).json({ message: 'Buddies is not exist'});
            let err = new NotFoundError("Buddies is not exist")
            return next(err);
        }

        //delete user data
        let data = await connection.getRepository(Buddies).createQueryBuilder()
            .delete()
            .where("Request_by = :id", { id: request_by })
            .andWhere("Request_to = :ids", { ids: request_to })
            .execute();

        // console.log("delete User: ", data);

        if(await connection.manager.findOne(Buddies, { where: { Request_by: request_by, Request_to: request_to } })) {
            //return res.status(304).json({ message: 'Buddies not deleted' });
            let err = new NotModifiedError("Buddies not deleted")
            return next(err);
        }
        return res.status(200).json({ message: 'Buddies deleted successfully' });
    } catch (error) {
        console.log(error);
        throw new InternalServerError()
    }
}

async function reGenerateToken(req, res, next) {
    console.log('POST /api/user/regeneratetoken API call made');

    const { user_id, user_token, device_id} = req.body;

    try {
        const connection = await DbUtils.getConnection();

        const loginuser = await connection.manager.findOne(Auth, { where: { User : user_id } });
        if(loginuser.Token == user_token && loginuser.Device_Id == device_id) {
            let tokengen =tokenGenerator.generate();
            console.log(tokengen);
            const data = await connection.getRepository(Auth)
                .createQueryBuilder()
                .update(Auth)
                .set({
                    Token : tokengen,
                    Currently_Playing : true,
                    Device_Id : device_id
                })
                .where("User = :id", { id: user_id})
                .execute();

            console.log(data);
            return res.status(200).json({ Issuccessful: true , user_id : user_id, token : tokengen });
        }
        //return res.status(404).json({ message: 'Device id not matched logout the person',Issuccessful: false });
        let err = new WrongDataError('Device id not matched logout the person,Issuccessful: false');
        return next(err);
    }

    catch (error) {
        console.log(error);
        throw new InternalServerError()
    }
}

//=======================Bank============================

async function userCreateBank(req, res, next){
    console.log('POST /api/user/createbank API call made');
    const { account_holder_name, ifsc_code, account_number, user_id } = req.body;

    try {
        const connection = await DbUtils.getConnection();
        if(!await connection.manager.findOne(User, { where: { User_Id : user_id } })) {
            // return res.status(404).json({ message: 'User is not available'});
            let err = new NotFoundError("User is not exist")
            return next(err);
        }
        else if(await connection.manager.findOne(Bank, { where: { Account_Number : account_number } })) {
            // return res.status(409).json({ message: 'The Account number has been taken, please try another'});
            let err =  new DuplicateDataError("The Account number has been taken, please try another");
            return next(err)
        }

        let bank = new Bank();
        console.log("fgf");
        bank.Account_Holder_Name = account_holder_name;
        bank.IFSC_Code = ifsc_code;
        bank.Account_Number = account_number;
        bank.User = await connection.manager.findOne(User, { where: { User_Id : user_id } });
        await connection.manager.save(bank);

        return res.status(200).json({ message: 'Bank added successfully' });
    } catch (error) {
        console.log(error);
        throw new InternalServerError();
    }
}

async function usersBankData(req, res, next) {
    console.log('POST /api/user/usersbankdata API call made');
    const { user_id } = req.body;

    try {
        const connection = await DbUtils.getConnection();
        let data = await connection.createQueryBuilder()
            .select(['b.Bank_Id"Bank_Id"', 'b.Account_Holder_Name"Account_Holder_Name"',
                'b.IFSC_Code"IFSC_Code"', 'b.Account_Number"Account_Number"',
                'u.User_Name"User_Name"'])
            .from(Bank, 'b')
            .leftJoin('b.User', 'u')
            .where("u.User_Id = :id", { id: user_id})
            .getRawMany();

        console.log("all users:", data);
        if (data) {
            return res.status(200).json(data);
        }
        // return res.status(404).json({ message: 'Data not available' });
        let err = new NotFoundError("Data not available")
        return next(err);
    } catch (error) {
        console.log(error);
        throw new InternalServerError();
    }
}

//=======================UPI============================

async function userCreateUpi(req, res, next){
    console.log('POST /api/user/createupi API call made');
    const { upi_id, user_id } = req.body;

    try {
        const connection = await DbUtils.getConnection();
        if(!await connection.manager.findOne(User, { where: { User_Id : user_id } })) {
            // return res.status(404).json({ message: 'User is not available'});
            let err = new NotFoundError("User is not exist")
            return next(err);
        }
        else if(await connection.manager.findOne(UPI, { where: { UPI_Id : upi_id } })) {
            // return res.status(409).json({ message: 'The UPI Id has been taken, please try another'});
            let err =  new DuplicateDataError("The UPI Id has been taken, please try another");
            return next(err)
        }

        let upi = new UPI();
        upi.UPI_Id = upi_id;
        upi.User = await connection.manager.findOne(User, { where: { User_Id : user_id } });
        await connection.manager.save(upi);

        return res.status(200).json({ message: 'UPI added successfully' });
    } catch (error) {
        console.log(error);
        throw new InternalServerError();
    }
}

async function usersUpiData(req, res, next) {
    console.log('POST /api/user/usersupidata API call made');
    const { user_id } = req.body;

    try {
        const connection = await DbUtils.getConnection();
        let data = await connection.createQueryBuilder()
            .select(['up.U_Id"U_Id"', 'up.UPI_Id"UPI_Id"',
                'u.User_Name"User_Name"'])
            .from(UPI, 'up')
            .leftJoin('up.User', 'u')
            .where("u.User_Id = :id", { id: user_id})
            .getRawMany();

        console.log("all users:", data);
        if (data) {
            return res.status(200).json(data);
        }
        // return res.status(404).json({ message: 'Data not available' });
        let err = new NotFoundError("Data not available")
        return next(err);
    } catch (error) {
        console.log(error);
        throw new InternalServerError();
    }
}

//=======================Lobby Structure============================

async function userSearchUserLobbyStructureData(req, res, next) {
    console.log('POST /api/user/searchuserlobbystructuredata API call made');
    const { entry_fees, player_count } = req.body;

    try {
        const connection = await DbUtils.getConnection();
        let data = await connection.createQueryBuilder()
            .select(['lbs.Lobby_Structure_Id"Lobby_Structure_Id"', 'lbs.Lobby_Structure_Name"Lobby_Structure_Name"',
                'lbs.Lobby_Entry_fees"Lobby_Entry_fees"','lbs.Lobby_Per_cut"Lobby_Per_cut"',
                'lbs.No_of_Players"No_of_Players"', 'ad.Admin_Name"Admin_Name"' ])
            .from(Lobby_Structure, 'lbs')
            .leftJoin('lbs.Admin', 'ad')
            .where("lbs.Lobby_Entry_fees = :id", { id: entry_fees})
            .andWhere("lbs.No_of_Players = :idb", { idb: player_count})
            .andWhere("lbs.IsActive = :ida", { ida: true})
            .getRawMany();

        //console.log("all users:", data[0].User_Image.toString());
        console.log("all users:", data);
        if (data) {
            return res.status(200).json(data);
        }
        // return res.status(404).json({ message: 'Data not available' });
        let err = new NotFoundError("Data not available")
        return next(err);
    } catch (error) {
        console.log(error);
        throw new InternalServerError();
    }
}

//=======================Inventory============================

async function userSearchUserInventoryData(req, res, next) {
    console.log('POST /api/user/searchuserinventorydata API call made');
    const { user_id } = req.body;

    try {
        const connection = await DbUtils.getConnection();
        let data1 = await connection.createQueryBuilder()
            .select(['in.Inventory_Id"Inventory_Id"', 'in.Emoji"Emoji"',
                'in.Dice"Dice"'])
            .from(Inventory, 'in')
            .where("in.User = :id", { id: user_id})
            .getRawMany();

        let data = {"Inventory_Id": data1[0].Inventory_Id,"Emoji":JSON.parse(data1[0].Emoji),"Dice":JSON.parse(data1[0].Dice)};

        //console.log("all users:", data[0].User_Image.toString());
        console.log("all users:", data);
        if (data) {
            return res.status(200).json(data);
        }
        // return res.status(404).json({ message: 'Data not available' });
        let err = new NotFoundError("Data not available")
        return next(err);
    } catch (error) {
        console.log(error);
        throw new InternalServerError();
    }
}

async function updateByEmoji(req, res, next) {
    console.log('PUT /api/user/updatebuyemoji API call made');
    const { user_id, emoji } = req.body;

    try {
        const connection = await DbUtils.getConnection();
        const exists = await connection.manager.findOne(Inventory, { where: { User: user_id } });
        if(!exists) {
            //return res.status(404).json({ message: 'Inventory is not exist'});
            let err = new NotFoundError("Inventory is not exist")
            return next(err);
        }
        const data = await connection
            .createQueryBuilder()
            .update(Inventory)
            .set({
                Emoji : emoji
            })
            .where("User = :id", { id: user_id })
            .execute();

        return res.status(200).json({ message: 'User Emoji data updated successfully' });

    } catch (error) {
        console.log(error);
        return res.status(500).json({ message: 'Internal Server Error' });

    }
}

async function updateByDice(req, res, next) {
    console.log('PUT /api/user/updatebuydice API call made');
    const { user_id, dice } = req.body;

    try {
        const connection = await DbUtils.getConnection();
        const exists = await connection.manager.findOne(Inventory, { where: { User: user_id } });
        if(!exists) {
            // return res.status(404).json({ message: 'Inventory is not exist'});
            let err = new NotFoundError("Inventory is not exist")
            return next(err);
        }
        const data = await connection
            .createQueryBuilder()
            .update(Inventory)
            .set({
                Dice : dice
            })
            .where("User = :id", { id: user_id })
            .execute();

        return res.status(200).json({ message: 'User Dice data updated successfully' });

    } catch (error) {
        console.log(error);
        return res.status(500).json({ message: 'Internal Server Error' });

    }
}

//=======================Achievements============================

async function AllAchievementss(req, res, next) {
    console.log('GET /api/user/allachievementss API call made');

    try {
        const connection = await DbUtils.getConnection();
        let data = await connection.manager.find(Achievements);

        //console.log("all users:", data[0].User_Image.toString());
        console.log("all users:", data);
        if (data) {
            return res.status(200).json(data);
        }
        // return res.status(404).json({ message: 'Data not available' });
        let err = new NotFoundError("Data not available")
        return next(err);
    } catch (error) {
        throw new InternalServerError();
    }
}

async function AllNormalLobbyStructure(req, res, next) {
    console.log('GET /api/admin/allnormallobbystructure API call made');

    try {
        const connection = await DbUtils.getConnection();

        let data = await connection.createQueryBuilder()
            .select(['lbs.Lobby_Structure_Id"Lobby_Structure_Id"',
                'lbs.Lobby_Entry_fees"Lobby_Entry_fees"','lbs.Lobby_Per_cut"Lobby_Per_cut"',
                'lbs.No_of_Players"No_of_Players"'])
            .from(Lobby_Structure, 'lbs')
            .where("lbs.IsActive = :id", { id: true })
            .andWhere("lbs.Lobby_Entry_fees < 1000")
            .getRawMany();

        //console.log("all users:", data[0].User_Image.toString());
        console.log("all users:", data);
        if (data) {
            return res.status(200).json(data);
        }
        // return res.status(404).json({ message: 'Data not available' });
        let err = new NotFoundError("Data not available")
        return next(err);
    } catch (error) {
        console.log(error);
        throw new InternalServerError();
    }
}

async function AllVipLobbyStructure(req, res, next) {
    console.log('GET /api/admin/allviplobbystructure API call made');

    try {
        const connection = await DbUtils.getConnection();

        let data = await connection.createQueryBuilder()
            .select(['lbs.Lobby_Structure_Id"Lobby_Structure_Id"',
                'lbs.Lobby_Entry_fees"Lobby_Entry_fees"','lbs.Lobby_Per_cut"Lobby_Per_cut"',
                'lbs.No_of_Players"No_of_Players"'])
            .from(Lobby_Structure, 'lbs')
            .where("lbs.IsActive = :id", { id: true })
            .andWhere("lbs.Lobby_Entry_fees > 1000")
            .getRawMany();

        //console.log("all users:", data[0].User_Image.toString());
        console.log("all users:", data);
        if (data) {
            return res.status(200).json(data);
        }
        // return res.status(404).json({ message: 'Data not available' });
        let err = new NotFoundError("Data not available")
        return next(err);
    } catch (error) {
        console.log(error);
        throw new InternalServerError();
    }
}

export const userController = {userRegister, verifyMobile, checkReferall, enterExitLobby, userLogin,userLogout, updateProfile, changePassword, activeUser, updateReferral, searchUserData, userWallet,
    updateWalletData, userAuth, updateAuthData, userGameData, updateGameData, createPayment, userPayment, userCreateLobby, userSearchLobbyData, userSearchUserLobbyData, userUpdateLobbyData, userDeleteLobby,
    userCreateBank, userCreateUpi, userCreateLobbyHistory, userSearchLobbyHistoryData, userSearchUserLobbyHistoryData,userCreateBuddies,userBuddiesData,searchBuddiesData,requestBuddiesData,updateBuddiesData,
    usersBankData, usersUpiData, deleteBuddies,reGenerateToken, searchUserDetail, userSearchUserLobbyStructureData, userSearchUserInventoryData, updateByEmoji, updateByDice, AllAchievementss,
    searchUserImage, AllNormalLobbyStructure, AllVipLobbyStructure }
