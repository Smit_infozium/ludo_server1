import { DbUtils } from "../utils/DbUtils";
import { User } from "../entity/User";
import { Lobby } from "../entity/Lobby";
import { Lobby_History } from "../entity/Lobby_History";
import { Auth } from "../entity/Auth";
import { Payment } from "../entity/Payment";
import { Variation } from "../entity/Variation";
import { Wallet } from "../entity/Wallet";
import { Buddies } from "../entity/Buddies";
import { GameData } from "../entity/GameData";
import { Bank } from "../entity/Bank";
import { UPI } from "../entity/UPI";
import { Achievements } from "../entity/Achievements";
import { Inventory } from "../entity/Inventory";
import { Lobby_Structure } from "../entity/Lobby_Structure";
import path from 'path';
import {In} from "typeorm";
import {InternalServerError, NotFoundError} from "../errors/InstancesCE";
const fs = require('fs');
const imagepath = '../media/';

//=======================Registration & Login============================

async function enterExitLobby(user_id:number, playing:boolean) 
{
    console.log('PUT /api/user/enterexitLobby API call made');
    //const { user_id, playing } = req.body;
    let promise = new Promise(async(resolve, reject) =>
    {
        try 
        {
            const connection = await DbUtils.getConnection();
            const exists = await connection.manager.findOne(Auth, { where: { User: user_id } });
            if(!exists) {
                console.log("User Token is not available");
                reject ("User Token is not available");
                //return res.status(404).json({ message: 'User Token is not available'});
            }

            const data = await connection.getRepository(Auth)
            .createQueryBuilder()
            .update(Auth)
            .set({ 
                Currently_Playing : playing
            })
            .where("User = :id", { id: user_id })
            .execute();
            resolve({ Issuccessful : true , user_id : user_id, token : data.Token });
            console.log({ Issuccessful : true , user_id : user_id, token : data.Token });
            //return res.status(200).json({ Issuccessful: true , user_id : user_id, token : data.Token });
        } 
        catch (error) 
        {
        console.log(error);
        reject(error);
        // throw new InternalServerError();
        } 
    });
    return promise;
     
}

async function userLogout(user_id:number) {
    console.log('PUT /api/user/logout API call made');
    //const { user_id } = req.body;
    let promise = new Promise(async(resolve, reject) =>
    {
        try {
            const connection = await DbUtils.getConnection();
            const exists = await connection.manager.findOne(Auth, { where: { User: user_id } });
            if(!exists) {
                console.log("User Token is not available");
                reject ("User Token is not available");
                //return res.status(404).json({ message: 'User Token is not available'});
            }
            const data = await connection.getRepository(Auth)
            .createQueryBuilder()
            .update(Auth)
            .set({ 
                Currently_Playing : false,
                Device_Id : null
            })
            .where("User = :id", { id: user_id })
            .execute();
            
            resolve({ Issuccessful: true , user_id : user_id, token : data.Token });
            console.log({ Issuccessful: true , user_id : user_id, token : data.Token });
            //return res.status(200).json({ Issuccessful: true , user_id : user_id, token : data.Token });    
        
    } catch (error) {
        console.log(error);
        reject(error);
        //throw new InternalServerError();
    } 
    });
    return promise;
}

//=======================User Profile============================

async function updateProfile(user_id:number, user_displayname:string, user_country:string, user_image:string) {
    console.log('PUT /api/user/updateprofile API call made');
    //const {user_id, user_displayname, user_country, user_image} = req.body;
    let promise = new Promise(async(resolve, reject) =>
    {
        try {
            const connection = await DbUtils.getConnection();
            const exists = await connection.manager.findOne(User, { where: { User_Id: user_id } });
            if(!exists) {
                console.log( "User is not registered with us, please try another");
                reject ("User is not registered with us, please try another");
                //return res.status(409).json({ message: 'User is not registered with us, please try another'});
            }
            //image decoding & uploading code
            const file_ = `${user_id}_Profile.png`
            console.log(file_)
            let path_ = path.join(__dirname,imagepath+file_)
            console.log(path_)
    
            fs.writeFile(path_, user_image, 'base64', (err)=>{
                if(err){
                    throw new Error(err.message)
                }
            })
            const data = await connection.getRepository(User)
            .createQueryBuilder()
            .update(User)
            .set({ 
                User_DisplayName : user_displayname,
                User_Country : user_country,
                User_Image : file_
            })
            .where("User_Id = :id", { id: user_id })
            .execute();
            resolve("User updated successfully");
            console.log("User updated successfully");
            //return res.status(200).json({ message: 'User updated successfully' });   
        } catch (error) {
            console.log(error);
            reject(error);
            // throw new InternalServerError();
        }
    });
    return promise;
}

async function updateFbUserId(user_id:number, fb_user_id:string) {
    console.log('PUT /api/user/updatefbid API call made');
    //const {user_id, user_displayname, user_country, user_image} = req.body;
    let promise = new Promise(async(resolve, reject) =>
    {
        try {
            const connection = await DbUtils.getConnection();
            const exists = await connection.manager.findOne(User, { where: { User_Id: user_id } });
            if(!exists) {
                console.log( "User is not registered with us, please try another");
                reject ("User is not registered with us, please try another");
                //return res.status(409).json({ message: 'User is not registered with us, please try another'});
            }
            //image decoding & uploading code
            
            const data = await connection.getRepository(User)
            .createQueryBuilder()
            .update(User)
            .set({ 
                Fb_User_Id : fb_user_id,
            })
            .where("User_Id = :id", { id: user_id })
            .execute();
            resolve("Fb ID updated successfully");
            console.log("Fb ID updated successfully");
            //return res.status(200).json({ message: 'User updated successfully' });   
        } catch (error) {
            console.log(error);
            reject(error);
            // throw new InternalServerError();
        }
    });
    return promise;
}

async function updateReferral(user_id:number, reference_id:string) {
    console.log('POST /api/user/updatereferral API call made');
    //const { user_id, reference_id} = req.body;
    let promise = new Promise(async(resolve, reject) =>
    {
        try {
            const connection = await DbUtils.getConnection();
      
                const exists = await connection.manager.findOne(User, { where: { User_Id: user_id } });
                if(!exists) {
                    console.log("User is not registered with us, please try another");
                    reject ("User is not registered with us, please try another");
                    //return res.status(409).json({ message: 'User is not registered with us, please try another'});
                }

                const ref = await connection.manager.findOne(User, { where: { Reference_Id: reference_id } });
                const data = await connection.getRepository(User)
                .createQueryBuilder()
                .update(User)
                .set({ 
                    Reference_By : ref.User_Id
                })
                .where("User_Id = :id", { id: user_id })
                .execute();
                resolve("User Reference updated successfully");
                console.log("User Reference updated successfully");
                //return res.status(200).json({ message: 'User Reference updated successfully' });   
            } 
            catch (error) 
            {
            console.log(error);
            reject(error);
            // throw new InternalServerError();
            } 
        });
        return promise;
}

async function searchUserImage(filename:string) {
    console.log('POST /api/user/searchuserimage API call made');
    //const { user_id } = req.body;
    let promise = new Promise(async(resolve, reject) =>
    {
        try {
            console.log("filename: " + filename);
            const connection = await DbUtils.getConnection();
                let data = fs.readFileSync(path.join(__dirname+'/'+imagepath)+filename);

                //console.log("all users:", data[0].User_Image.toString());
                console.log("all users:", data);
                if (data) {
                    resolve(data);
                    //return res.status(200).json(data);
                }
                console.log("Data not available");
                reject ("Data not available");
                //return res.status(404).json({ message: 'Data not available' });   
            } 
            catch (error) 
            {
            console.log(error);
            reject(error);
            // throw new InternalServerError();
            } 
        });
        return promise;
         
}

async function searchUserData(user_id:number) {
    console.log('POST /api/user/searchuserdata API call made');
    //const { user_id } = req.body;
    let promise = new Promise(async(resolve, reject) =>
    {
        try {
            const connection = await DbUtils.getConnection();
                let data = await connection.createQueryBuilder()
                .select(['u.User_Id"User_Id"', 'u.User_Name"Username"',
                'u.User_DisplayName"User_DisplayName"', 'u.User_Country"User_Country"',
                'u.User_Image"User_Image"','u.Fb_User_id"Fb_User_Id"', 'w.User_Amount"User_Amount"'])
                .from(User, 'u')
                .leftJoin('u.Wallet', 'w')
                .where("u.User_Id = :id", { id: user_id})
                .getRawOne();

                //console.log("all users:", data[0].User_Image.toString());
                console.log("all users:", data);
                if (data) {
                    resolve(data);
                    //return res.status(200).json(data);
                }else{
                    
                    console.log("Data not available");
                    reject("Data not available");
                }
                //return res.status(404).json({ message: 'Data not available' });   
            } 
            catch (error) 
            {
            console.log(error);
            reject(error);
            // throw new InternalServerError();
            } 
        });
        return promise;
         
}

async function searchUserDetail(user_id:number) {
    console.log('POST /api/user/searchuserdetail API call made');
    //const { user_id } = req.body;
    let promise = new Promise(async(resolve, reject) =>
    {
        try 
        {
            const connection = await DbUtils.getConnection();
            let data = await connection.createQueryBuilder()
            .select(['u.User_Id"User_Id"', 'u.User_Name"Username"',
            'u.User_DisplayName"User_DisplayName"', 'u.User_Country"User_Country"',
            'u.User_Image"User_Image"', 'w.User_Amount"User_Amount"',
            'w.User_Bonus_Cash"User_Bonus_Cash"', 'w.User_Credit_Amount"User_Credit_Amount"',
            'w.User_Debit_Amount"User_Debit_Amount"','w.User_Win_Amount"User_Win_Amount"',
            'w.User_Loss_Amount"User_Loss_Amount"',  'g.Total_Wins"Total_Wins"', 'g.Total_Loss"Total_Loss"',
            'g.Token_Captured"Token_Captured"',
            'g.Opp_Token_Captured"Opp_Token_Captured"'])
            .from(User, 'u')
            .leftJoin('u.Wallet', 'w')
            .leftJoin('u.GameData', 'g')
            .where("u.User_Id = :id", { id: user_id})
            .getRawOne();

            //console.log("all users:", data[0].User_Image.toString());
            console.log("all users:", data);
            if (data) {
                resolve(data);
                    //return res.status(200).json(data);
                }
            else {
                console.log("Data not available");
                reject("Data not available");
            }
                //return res.status(404).json({ message: 'Data not available' });  
        } 
        catch (error) 
        {
        console.log(error);
        reject(error);
        // throw new InternalServerError();
        } 
    });
    return promise;
         
}


//=======================Wallet============================

async function userWallet(user_id:number) {
    console.log('POST /api/user/userwallet API call made');
    //const { user_id } = req.body;
    let promise = new Promise(async(resolve, reject) =>
    {
        try {
            const connection = await DbUtils.getConnection();
                    let data = await connection.createQueryBuilder()
                    .select(['u.User_Id"User_Id"', 'w.User_Amount"User_Amount"',
                    'w.User_Bonus_Cash"User_Bonus_Cash"', 'w.User_Credit_Amount"User_Credit_Amount"',
                    'w.User_Debit_Amount"User_Debit_Amount"','w.User_Win_Amount"User_Win_Amount"',
                    'w.User_Loss_Amount"User_Loss_Amount"'])
                    .from(Wallet, 'w')
                    .leftJoin('w.User', 'u')
                    .where("u.User_Id = :id", { id: user_id})
                    .getRawOne();
    
                    console.log("all users:", data);
                    if (data) {
                        resolve(data);
                        //return res.status(200).json(data);
                    }
                    console.log("Data not available");
                    reject ("Data not available");
                    //return res.status(404).json({ message: 'Data not available' });   
                } 
                catch (error) 
                {
                console.log(error);
                reject(error);
                // throw new InternalServerError();
                } 
            });
            return promise;
             
}

async function updateWalletData(user_id:number, user_amount:number, user_bonus:number, user_credit_amount:number,
    user_debit_amount:number, user_win_amount:number, user_loss_amount:number ) {
    console.log('PUT /api/user/updatewalletdata API call made');
    //const { user_id, user_amount, user_bonus, user_credit_amount, user_debit_amount, user_win_amount, user_loss_amount } = req.body;
    let promise = new Promise(async(resolve, reject) =>
    {
        try 
        {
            const connection = await DbUtils.getConnection();
        
                const exists = await connection.manager.findOne(User, { where: { User_Id: user_id } });
                if(!exists) {
                    console.log("User is not registered with us, please try another");
                    reject ("User is not registered with us, please try another");
                    //return res.status(409).json({ message: 'User is not registered with us, please try another'});
                }
                const chips = await connection.manager.findOne(Wallet, { where: { User: user_id } });
                const data = await connection.getRepository(Wallet)
                .createQueryBuilder()
                .update(Wallet)
                .set({ 
                    User_Amount : (chips.User_Amount + user_amount),
                    User_Bonus_Cash : ( chips.User_Bonus_Cash + user_bonus),
                    User_Credit_Amount : ( chips.User_Credit_Amount + user_credit_amount),
                    User_Debit_Amount : ( chips.User_Debit_Amount  + user_debit_amount),
                    User_Win_Amount : (chips.User_Win_Amount + user_win_amount),
                    User_Loss_Amount : (chips.User_Loss_Amount + user_loss_amount)
                })
                .where("User = :id", { id: user_id })
                .execute();
                resolve("Wallet updated successfully");
                console.log("Wallet updated successfully");
                //res.status(200).json({ message: 'Wallet updated successfully' });   
        } catch (error) {
            console.log(error);
            reject(error);
            //throw new InternalServerError();
        } 
    });
    return promise;
}

//=======================Game Data============================

async function userGameData(user_id:number) {
    console.log('POST /api/user/usergamedata API call made');
    //const { user_id } = req.body;
    let promise = new Promise(async(resolve, reject) =>
    {   
        try 
        {
            const connection = await DbUtils.getConnection();
    
            let data = await connection.createQueryBuilder()
            .select(['u.User_Id"User_Id"',
            'g.Total_Wins"Total_Wins"', 'g.Total_Loss"Total_Loss"',
            'g.Token_Captured"Token_Captured"',
            'g.Opp_Token_Captured"Opp_Token_Captured"', 'g.Achievements"Achievements"'])
            .from(GameData, 'g')
            .leftJoin('g.User', 'u')
            .where("u.User_Id = :id", { id: user_id})
            .getRawOne();

            console.log("all users:", data);
            if (data) {
                resolve(data);
                //return res.status(200).json(data);
            }
            else{
                console.log("Data not available");
                reject ("Data not available");
            }
            //return res.status(400).json({ message: 'Data not available' });   
        } 
        catch (error) 
        {
        console.log(error);
        reject(error);
        // throw new InternalServerError();
        } 
    });
    return promise;
            
}   

async function updateGameData(user_id:number, total_wins:number, total_loss:number, token_captured:number, 
    opp_token_captured:number, achievements:number ) {
    console.log('PUT /api/user/updategamedata API call made');
    //const {user_id, total_wins, total_loss, token_captured, opp_token_captured, achievements} = req.body;
    let promise = new Promise(async(resolve, reject) =>
    {
        try 
        {
            const connection = await DbUtils.getConnection();
        
            const exists = await connection.manager.findOne(User, { where: { User_Id: user_id } });
            if(!exists) {
                console.log("User is not registered with us, please try another");
                reject ("User is not registered with us, please try another");
                //return res.status(409).json({ message: 'User is not registered with us, please try another'});
            }
            const gem = await connection.manager.findOne(GameData, { where: { User: user_id } });
            // console.log("ach: ",gem.Achievements.id);
            const data = await connection.getRepository(GameData)
            .createQueryBuilder()
            .update(GameData)
            .set({ 
                Total_Wins : ( gem.Total_Wins + total_wins),
                Total_Loss : ( gem.Total_Loss + total_loss),
                Token_Captured : ( gem.Token_Captured + token_captured),
                Opp_Token_Captured : ( gem.Opp_Token_Captured + opp_token_captured),
                Achievements : achievements
                //SON.parse(achievements)
            })
            .where("User = :id", { id: user_id })
            .execute();
            resolve("GameData updated successfully");
            console.log("GameData updated successfully");
            //return res.status(200).json({ message: 'GameData updated successfully' });    
        } 
        catch (error) 
        {
        console.log(error);
        reject(error);
        // throw new InternalServerError();
        } 
    });
    return promise;
}

async function createPayment( user_id:number, iscredit:boolean, payment_amount:number, transaction_id:number ) {
    console.log('POST /api/user/createpayment API call made');
    let promise = new Promise(async(resolve, reject) =>
    {
    //const { user_id, iscredit, payment_amount, transaction_id } = req.body;
        try 
        {
            const connection = await DbUtils.getConnection();
            let user = await connection.manager.findOne(User, { where: { User_Id : user_id } });
            console.log(user);
            let wallet = await connection.manager.findOne(Wallet, { where: { User : user.User_Id } });
            console.log(wallet);
            if(!user)
            {
                console.log("User not exist");
                reject ("User not exist");
                //return res.status(200).json({ message: 'User not exist' });
            }
            if((parseFloat(wallet.User_Amount) >= (payment_amount)) && (iscredit == false))
            {
                console.log("Unsufficient balance");
                reject ("Unsufficient balance");
                //return res.status(200).json({ message: 'Unsufficient balance' }); 
            }
            let payment = new Payment();
            payment.User = user;
            if(iscredit == true){
                payment.Is_Credit = true;
                wallet.User_Amount = parseFloat(wallet.User_Amount) + (payment_amount);
            }
            else{
                payment.Is_Credit = false;
                wallet.User_Amount = parseFloat(wallet.User_Amount) - (payment_amount);
            }
            payment.Payment_Amount = payment_amount;
            payment.Transaction_Id = transaction_id;
            await connection.manager.save(payment);
            await connection.manager.save(wallet);

            resolve("Payment Created successfully");
            console.log("Payment Created successfully");
            //return res.status(200).json({ message: 'Payment Created successfully' }); 
        
        } 
        catch (error) 
        {
        console.log(error);
        reject(error);
        // throw new InternalServerError();
        } 
    });
    return promise;
}

async function userPayment(user_id:number) {
    console.log('POST /api/user/userpayments API call made');
    //const { user_id } = req.body;
    let promise = new Promise(async(resolve, reject) =>
    {   
        try {
            const connection = await DbUtils.getConnection();
            let data = 
            await connection.createQueryBuilder()
            .select(['u.User_Id"User_Id"', 'u.User_Name"Username"',
            'p.Is_Credit"Is_Credit"', 'p.Payment_Amount"Payment_Amount"',
            'p.Payment_Time_Date"Payment_Time_Date"','p.Transaction_Id"Transaction_Id"'])
            .from(Payment, 'p')
            .leftJoin('p.User', 'u')
            .where("u.User_Id = :id", { id: user_id })
            .getRawMany();

            //console.log("all users:", data[0].User_Image.toString());
            console.log("all users:", data);
            if (data) {
                resolve(data);
                //return res.status(200).json(data);
            }
            console.log("Data not available");
            reject ("Data not available");
            //return res.status(404).json({ message: 'Data not available' });   
        } 
        catch (error) 
        {
        console.log(error);
        reject(error);
        // throw new InternalServerError();
        } 
    });
    return promise;
     
}

//=======================Lobby============================

async function userCreateLobby( lobby_name:string, variation_name:string, player_count:number, lobby_total_amount:number, 
    lobby_buy_in:number, lobby_commision_amount:number, room_id:string, lobby_status:string, lobby_owner:number ) {
    console.log('POST /api/user/createlobby API call made');
    //const { lobby_name, variation_name, player_count, lobby_total_amount, lobby_buy_in, lobby_commision_amount, room_id, lobby_status, lobby_owner } = req.body;
    let promise = new Promise(async(resolve, reject) =>
    {
        try {
            const connection = await DbUtils.getConnection();
            if(await connection.manager.findOne(Lobby, { where: { Lobby_Name : lobby_name } })) {
                console.log("The Lobby Name has been taken, please try another");
                reject ("The Lobby Name has been taken, please try another");
                //return res.status(409).json({ message: 'The Lobby Name has been taken, please try another'});
            }
            let vart = await connection.manager.findOne(Variation, { where: { Variation_Name : variation_name, Player_Count : player_count } });
            let lobby = new Lobby();
            lobby.Lobby_Name = lobby_name;
            lobby.Lobby_Total_Amount = lobby_total_amount;
            lobby.Lobby_Buy_In = lobby_buy_in;
            lobby.Lobby_Commision_Amount = lobby_commision_amount;
            lobby.Room_Id = room_id;
            lobby.Lobby_Status = lobby_status;
            lobby.Recorded_Move = null;
            lobby.Variation = vart.Variation_Id;
            let u = await connection.manager.findOne(User, { where: { User_Id : lobby_owner } });
            lobby.Lobby_Owner = u.User_Id;
            await connection.manager.save(lobby);

            resolve("Lobby Created successfully");
            console.log("Lobby Created successfully");
            //res.status(200).json({ message: 'Lobby Created successfully'});  
        } 
        catch (error) 
        {
        console.log(error);
        reject(error);
        // throw new InternalServerError();
        } 
    });
    return promise;
}

async function userSearchLobbyData( lobby_id:number ) {
    console.log('POST /api/user/searchlobbydata API call made');
    //const { lobby_id } = req.body;
    let promise = new Promise(async(resolve, reject) =>
    {    
        try {
            const connection = await DbUtils.getConnection();
            let data = await connection.createQueryBuilder()
            .select(['lb.Lobby_Id"Lobby_Id"', 'lb.Lobby_Name"Lobby_Name"',
            'lb.Lobby_Total_Amount"Lobby_Total_Amount"','lb.Lobby_Buy_In"Lobby_Buy_In"',
            'lb.Lobby_Commision_Amount"Lobby_Commision_Amount"', 'lb.Recorded_Move"Recorded_Move"',
            'lb.Room_Id"Room_Id"','lb.Lobby_Status"Lobby_Status"', 
            'lb.Lobby_Start_Time_Date"Lobby_Start_Time_Date"', 'lb.Lobby_End_Time_Date"Lobby_End_Time_Date"',
            'u.User_Name"Lobby_Owner"', 'v.Variation_Name"Variation_Name"' ])
            .from(Lobby, 'lb')
            .leftJoin('lb.Lobby_Owner', 'u')
            .leftJoin('lb.Variation', 'v')
            .where("lb.Lobby_Id = :id", { id: lobby_id})
            .getRawOne();

            //console.log("all users:", data[0].User_Image.toString());
            console.log("all users:", data);
            if (data) {
                resolve(data);
                //return res.status(200).json(data);
            }
            console.log("Data not available");
            reject ("Data not available");
            //return res.status(404).json({ message: 'Data not available' });   
        } 
        catch (error) 
        {
        console.log(error);
        reject(error);
        // throw new InternalServerError();
        } 
    });
    return promise;
     
}

async function userSearchUserLobbyData(user_id:number) {
    console.log('POST /api/user/searchuserlobbydata API call made');
    //const { user_id } = req.body;
    let promise = new Promise(async(resolve, reject) =>
    {   
        try {
            const connection = await DbUtils.getConnection();
            let data = await connection.createQueryBuilder()
            .select(['lb.Lobby_Id"Lobby_Id"', 'lb.Lobby_Name"Lobby_Name"',
            'lb.Lobby_Total_Amount"Lobby_Total_Amount"','lb.Lobby_Buy_In"Lobby_Buy_In"',
            'lb.Lobby_Commision_Amount"Lobby_Commision_Amount"', 'lb.Recorded_Move"Recorded_Move"',
            'lb.Room_Id"Room_Id"','lb.Lobby_Status"Lobby_Status"', 
            'lb.Lobby_Start_Time_Date"Lobby_Start_Time_Date"', 'lb.Lobby_End_Time_Date"Lobby_End_Time_Date"',
            'u.User_Name"Lobby_Owner"', 'v.Variation_Name"Variation_Name"' ])
            .from(Lobby, 'lb')
            .leftJoin('lb.Lobby_Owner', 'u')
            .leftJoin('lb.Variation', 'v')
            .where("u.User_Id = :id", { id: user_id})
            .getRawMany();

            //console.log("all users:", data[0].User_Image.toString());
            console.log("all users:", data);
            if (data) {
                resolve(data);
                //return res.status(200).json(data);
            }
            console.log("Data not available");
            reject ("Data not available");
            //return res.status(404).json({ message: 'Data not available' });   
        } 
        catch (error) 
        {
        console.log(error);
        reject(error);
        // throw new InternalServerError();
        } 
    });
    return promise;
     
}

async function userUpdateLobbyData(lobby_id:number, lobby_total_amount:number, lobby_buy_in:number, lobby_status:string) {
    console.log('PUT /api/user/updatelobbydata API call made');
    //const { lobby_id, lobby_total_amount, lobby_buy_in, lobby_status } = req.body;
    let promise = new Promise(async(resolve, reject) =>
    {
        try {
            const connection = await DbUtils.getConnection();
            const exists = await connection.manager.findOne(Lobby, { where: { Lobby_Id: lobby_id } });
            if(!exists) {
                console.log("Lobby is not exist");
                reject ("Lobby is not exist");
                //return res.status(404).json({ message: 'Lobby is not exist'});
            }
            const data = await connection
            .createQueryBuilder()
            .update(Lobby)
            .set({ 
                Lobby_Total_Amount : lobby_total_amount,
                Lobby_Buy_In : lobby_buy_in,
                Lobby_Status : lobby_status,
                Recorded_Move : {name: "dfdf"},
                Lobby_End_Time_Date : () => 'CURRENT_TIMESTAMP'
            })
            .where("Lobby_Id = :id", { id: lobby_id })
            .execute();
            resolve("Lobby updated successfully");
            console.log("Lobby updated successfully");
            //res.status(200).json({ message: 'Lobby updated successfully' });   
            
        } 
        catch (error) 
        {
        console.log(error);
        reject(error);
        // throw new InternalServerError();
        } 
    });
    return promise;
}

async function userDeleteLobby(lobby_id:number) {
    console.log('DELETE /api/user/deletelobby API call made');
    //const { lobby_id } = req.body;
    let promise = new Promise(async(resolve, reject) =>
    {
        try {
            const connection = await DbUtils.getConnection();
            const exists = await connection.manager.findOne(Lobby, { where: { Lobby_Id: lobby_id  } });
            if(!exists) {
                console.log("Lobby is not exist");
                reject ("Lobby is not exist");
                //return res.status(404).json({ message: 'Lobby is not exist'});
            }

            //delete user data
            let data = await connection.getRepository(Lobby).createQueryBuilder()
            .delete()
            .where("Lobby_Id = :id", { id: lobby_id  })
            .execute();
            
            // console.log("delete User: ", data);
            
            if(await connection.manager.findOne(Lobby, { where: { Lobby_Id: lobby_id  } })) {
                console.log("Lobby not deleted");
                reject ("Lobby not deleted");
                //return res.status(304).json({ message: 'Lobby not deleted' });
            }
            resolve("Lobby deleted successfully");
            console.log("Lobby deleted successfully");
            //return res.status(200).json({ message: 'Lobby deleted successfully' });  
        } 
        catch (error) 
        {
        console.log(error);
        reject(error);
        // throw new InternalServerError();
        } 
    });
    return promise;
}

//=======================Lobby History============================

async function userCreateLobbyHistory(lobby_id:number, winning_status:string, winning_price:number, user_id:number, 
    powerups_used:number, total_amount_spend:number ) {
    console.log('POST /api/user/createlobbyhistory API call made');
    //const { lobby_id, winning_status, winning_price, user_id, powerups_used, total_amount_spend } = req.body;
    let promise = new Promise(async(resolve, reject) =>
    {
        try {
            const connection = await DbUtils.getConnection();
            if(!await connection.manager.findOne(Lobby, { where: { Lobby_Id : lobby_id } })) {
                console.log("Lobby not exist");
                reject ("Lobby not exist");
                //return res.status(409).json({ message: 'Lobby not exist'});
            }
            let lobby_history = new Lobby_History();
            lobby_history.Winning_Status = winning_status;
            lobby_history.Winning_Price = winning_price;
            let lb = await connection.manager.findOne(Lobby, { where: { Lobby_Id : lobby_id } });
            lobby_history.Lobby = lb.Lobby_Id;
            let u = await connection.manager.findOne(User, { where: { User_Id : user_id } });
            lobby_history.User = u.User_Id;
            lobby_history.Powerups_used = powerups_used;
            lobby_history.Total_Amount_Spend = total_amount_spend;
            await connection.manager.save(lobby_history);

            resolve("Lobby history Created successfully");
            console.log("Lobby history Created successfully");
            //res.status(200).json({ message: 'Lobby history Created successfully'});   
        } 
        catch (error) 
        {
        console.log(error);
        reject(error);
        // throw new InternalServerError();
        } 
    });
    return promise;
}

async function userSearchLobbyHistoryData(lobby_id:number) {
    console.log('POST /api/user/searchlobbyhistorydata API call made');
    //const { lobby_id } = req.body;
    let promise = new Promise(async(resolve, reject) =>
    {   
        try {
            const connection = await DbUtils.getConnection();
            let data = await connection.createQueryBuilder()
            .select(['lb.Lobby_Id"Lobby_Id"', 'lb.Lobby_Name"Lobby_Name"',
            'lbh.Winning_Status"Winning_Status"', 'lbh.Winning_Price"Winning_Price"',
            'lbh.Powerups_used"Powerups_used"', 'lbh.Total_Amount_Spend"Total_Amount_Spend"',                                                                                                                                                                                                                                                                                                                                             
            'u.User_Name"User_Name"'])
            .from(Lobby_History, 'lbh')
            .leftJoin('lbh.Lobby', 'lb')
            .leftJoin('lb.Lobby_Owner', 'u')
            .where("lb.Lobby_Id = :id", { id: lobby_id})
            .getRawMany();

            console.log("all users:", data);
            if (data) {
                resolve(data);
                //return res.status(200).json(data);
            }
            console.log("Data not available");
            reject ("Data not available");
            //return res.status(404).json({ message: 'Data not available' });   
        } 
        catch (error) 
        {
        console.log(error);
        reject(error);
        // throw new InternalServerError();
        } 
    });
    return promise;
     
}

async function userSearchUserLobbyHistoryData(user_id:number) {
    console.log('POST /api/user/searchuserlobbyhistorydata API call made');
    //const { user_id } = req.body;
    let promise = new Promise(async(resolve, reject) =>
    {    
        try {
            const connection = await DbUtils.getConnection();
            let data = await connection.createQueryBuilder()
            .select(['lb.Lobby_Id"Lobby_Id"', 'lb.Lobby_Name"Lobby_Name"',
            'lbh.Winning_Status"Winning_Status"', 'lbh.Winning_Price"Winning_Price"',
            'lbh.Powerups_used"Powerups_used"', 'lbh.Total_Amount_Spend"Total_Amount_Spend"',                                                                                                                                                                                                                                                                                                                                             
            'u.User_Name"User_Name"'])
            .from(Lobby_History, 'lbh')
            .leftJoin('lbh.Lobby', 'lb')
            .leftJoin('lbh.User', 'u')
            .where("u.User_Id = :id", { id: user_id})
            .getRawMany();

            console.log("all users:", data);
            if (data) {
                resolve(data);
                //return res.status(200).json(data);
            }
            console.log("Data not available");
            reject ("Data not available");
            //return res.status(404).json({ message: 'Data not available' });   
        } 
        catch (error) 
        {
        console.log(error);
        reject(error);
        // throw new InternalServerError();
        } 
    });
    return promise;
     
}

//=======================Buddies============================
async function userCreateBuddies(request_by:number, request_to:number) {
    console.log('POST /api/user/createbuddies API call made');
    //const { request_by, request_to } = req.body;
    let promise = new Promise(async(resolve, reject) =>
    {
    try {
        const connection = await DbUtils.getConnection();
        if((!await connection.manager.findOne(User, { where: { User_Id : request_by } })) 
            || (!await connection.manager.findOne(User, { where: { User_Id : request_to } }))){
                console.log("User not exist");
                reject ("User not exist");
                //return res.status(404).json({ message: 'User not exist' });
        }
        if(await connection.manager.findOne(Buddies, { where: { Request_by : request_by, Request_to: request_to } })){
            console.log("Request already sended");
            reject ("Request already sended");
            //return res.status(409).json({ message: 'Request already sended' });
        }
        if(await connection.manager.findOne(Buddies, { where: { Request_by : request_to, Request_to: request_by } })){
            console.log("Request already pending");
            reject ("Request already pending");
            //return res.status(409).json({ message: 'Request already pending' });
        }
        //blocked user condition

        let buddies = new Buddies();
        let u = await connection.manager.findOne(User, { where: { User_Id : request_by } });
        buddies.Request_by = u.User_Id;
        u = await connection.manager.findOne(User, { where: { User_Id : request_to } });
        buddies.Request_to = u.User_Id;
        await connection.manager.save(buddies);

        resolve("Buddies request added successfully");
        console.log("Buddies request added successfully");
        //return res.status(200).json({ message: 'Buddies request added successfully' });  
    } 
    catch (error) 
    {
    console.log(error);
    reject(error);
    // throw new InternalServerError();
    } 
});
return promise;
}

async function userBuddiesData( user_id:number ) {
    console.log('POST /api/user/buddiesdata API call made');
    //const { user_id } = req.body;
    let promise = new Promise(async(resolve, reject) =>
    {    
    try {
        const connection = await DbUtils.getConnection();
        let data = await connection.createQueryBuilder()
        .select(['bd.Buddies_Id"Buddies_Id"', 'rb.User_Name"User_Name"', 'rb.User_Id"Req_By_Id"', 'rb.User_Image"User_Image"', 'w.User_Amount"User_Amount"'])
        .from(Buddies, 'bd')
        .leftJoin('bd.Request_by', 'rb')
        .leftJoin('bd.Request_to', 'rt')
        .leftJoin('rb.Wallet', 'w')
        .where("rt.User_Id = :id", { id: user_id})
        .andWhere("bd.Req_status = :a", { a: "Approved"})
        .getRawMany();

        //console.log("all users:", data[0].User_Image.toString());
        console.log("all users:", data);
        if (data) {
            resolve(data);
            //return res.status(200).json(data);
        }
        console.log("Data not available");
        reject ("Data not available");
        //return res.status(404).json({ message: 'Data not available' });   
    } 
    catch (error) 
    {
    console.log(error);
    reject(error);
    // throw new InternalServerError();
    } 
});
return promise;
 
}

async function searchBuddiesData(user_id:number, frnds_id:number) {
    console.log('POST /api/user/searchbuddiesdata API call made');
    //const { user_id, frnds_id } = req.body;
    let promise = new Promise(async(resolve, reject) =>
    {   
        try {
            const connection = await DbUtils.getConnection();
            let data = await connection.createQueryBuilder()
            .select(['bd.Buddies_Id"Buddies_Id"', 'rb.User_Name"User_Name"'])
            .from(Buddies, 'bd')
            .leftJoin('bd.Request_by', 'rb')
            .leftJoin('bd.Request_to', 'rt')
            .where("rt.User_Id = :id", { id: user_id})
            .where("rb.User_Id = :ida", { ida: frnds_id})
            .andWhere("bd.Req_status = :a", { a: "Approved"})
            .getRawMany();

            //console.log("all users:", data[0].User_Image.toString());
            console.log("all users:", data);
            if (data) {
                resolve(data);
                //return res.status(200).json(data);
            }
            console.log("Data not available");
            reject ("Data not available");
            //return res.status(404).json({ message: 'Data not available' });   
        } 
        catch (error) 
        {
        console.log(error);
        reject(error);
        // throw new InternalServerError();
        } 
    });
    return promise;
 
}

async function requestBuddiesData( user_id:number ) {
    console.log('POST /api/user/reqbuddiesdata API call made');
    //const { user_id } = req.body;
    let promise = new Promise(async(resolve, reject) =>
    {    
        try {
            const connection = await DbUtils.getConnection();
            let data = await connection.createQueryBuilder()
            .select(['bd.Buddies_Id"Buddies_Id"', 'rb.User_Name"User_Name"', 'rb.User_Id"Req_By_Id"', 'rb.User_Image"User_Image"','w.User_Amount"User_Amount"'])
            .from(Buddies, 'bd')
            .leftJoin('bd.Request_by', 'rb')
            .leftJoin('bd.Request_to', 'rt')
            .leftJoin('rb.Wallet', 'w')
            .where("rt.User_Id = :id", { id: user_id })
            .andWhere("bd.Req_status = :a", { a: "Pending"})
            .getRawMany();

            //console.log("all users:", data[0].User_Image.toString());
            console.log("all users:", data);
            if (data) {
                resolve(data);
                //return res.status(200).json(data);
            }
            console.log("Data not available");
            reject ("Data not available");
            //return res.status(404).json({ message: 'Data not available' });   
        } 
        catch (error) 
        {
        console.log(error);
        reject(error);
        // throw new InternalServerError();
        } 
    });
    return promise;
 
}

async function updateBuddiesData(request_by:number, request_to:number, req_status:string) {
    console.log('PUT /api/user/updatebuddiesdata API call made');
    //const { request_by, request_to, req_status } = req.body;
    let promise = new Promise(async(resolve, reject) =>
    {
        try {
            const connection = await DbUtils.getConnection();
            const exists = await connection.manager.findOne(Buddies, { where: { Request_by: request_by, Request_to: request_to } });
            if(!exists) {
                console.log("Buddies is not exist");
                reject ("Buddies is not exist");
                //return res.status(404).json({ message: 'Buddies is not exist'});
            }
            const data = await connection
            .createQueryBuilder()
            .update(Buddies)
            .set({ 
                Req_status : req_status
            })
            .where("Request_by = :id", { id: request_by })
            .andWhere("Request_to = :ids", { ids: request_to })
            .execute();

            resolve("Buddies updated successfully");
            console.log("Buddies updated successfully");
            //res.status(200).json({ message: 'Buddies updated successfully' });   
            
        } 
        catch (error) 
        {
        console.log(error);
        reject(error);
        // throw new InternalServerError();
        } 
    });
    return promise;
}

async function deleteBuddies(request_by:number, request_to:number) {
    console.log('DELETE /api/user/deletebuddies API call made');
    //const { request_by, request_to } = req.body;
    let promise = new Promise(async(resolve, reject) =>
    {
        try {
            const connection = await DbUtils.getConnection();
            const exists = await connection.manager.findOne(Buddies, { where: { Request_by: request_by, Request_to: request_to } });
            if(!exists) {
                console.log("Buddies is not exist");
                reject ("Buddies is not exist");
                //return res.status(404).json({ message: 'Buddies is not exist'});
            }

            //delete user data
            let data = await connection.getRepository(Buddies).createQueryBuilder()
            .delete()
            .where("Request_by = :id", { id: request_by })
            .andWhere("Request_to = :ids", { ids: request_to })
            .execute();
            
            // console.log("delete User: ", data);
            
            if(await connection.manager.findOne(Buddies, { where: { Request_by: request_by, Request_to: request_to } })) {
                console.log("Buddies not deleted");
                reject ("Buddies not deleted");
                //return res.status(304).json({ message: 'Buddies not deleted' });
            }
            resolve("Buddies deleted successfully");
            console.log("Buddies deleted successfully");
            //return res.status(200).json({ message: 'Buddies deleted successfully' });  
        } 
        catch (error) 
        {
        console.log(error);
        reject(error);
        // throw new InternalServerError();
        } 
    });
    return promise;
}

//=======================Bank============================  

async function userCreateBank(account_holder_name, ifsc_code, account_number, user_id){
    console.log('POST /api/user/createbank API call made');
    //const { account_holder_name, ifsc_code, account_number, user_id } = req.body;
    let promise = new Promise(async(resolve, reject) =>
    {
        try {
            const connection = await DbUtils.getConnection();
            if(!await connection.manager.findOne(User, { where: { User_Id : user_id } })) {
                console.log("User is not available");
                reject ("User is not available");
                return;
                //return res.status(409).json({ message: 'User is not available'});
            }
            else if(await connection.manager.findOne(Bank, { where: { Account_Number : account_number } })) {
                console.log("The Account number has been taken, please try another");
                reject ("The Account number has been taken, please try another");
                return;
                //return res.status(409).json({ message: 'The Account number has been taken, please try another'});
            }

            let bank = new Bank();
            console.log("fgf");
            bank.Account_Holder_Name = account_holder_name;
            bank.IFSC_Code = ifsc_code;
            bank.Account_Number = account_number;
            bank.User = await connection.manager.findOne(User, { where: { User_Id : user_id } });
            await connection.manager.save(bank);

            resolve("Bank added successfully");
            console.log("Bank added successfully");
            //return res.status(200).json({ message: 'Bank added successfully' });  
        } 
        catch (error) 
        {
        console.log(error);
        reject(error);
        // throw new InternalServerError();
        } 
    });
    return promise;
}

async function usersBankData(user_id:number) {
    console.log('POST /api/user/usersbankdata API call made');
    //const { user_id } = req.body;
    let promise = new Promise(async(resolve, reject) =>
    {    
        try {
            const connection = await DbUtils.getConnection();
            let data = await connection.createQueryBuilder()
            .select(['b.Bank_Id"Bank_Id"', 'b.Account_Holder_Name"Account_Holder_Name"',
            'b.IFSC_Code"IFSC_Code"', 'b.Account_Number"Account_Number"',                                                                                                                                                                                                                                                                                                                                      
            'u.User_Name"User_Name"'])
            .from(Bank, 'b')
            .leftJoin('b.User', 'u')
            .where("u.User_Id = :id", { id: user_id})
            .getRawMany();

            console.log("all users:", data);
            if (data) {
                resolve(data);
                //return res.status(200).json(data);
            }
            console.log("Data not available");
            reject ("Data not available");
            //return res.status(404).json({ message: 'Data not available' });   
        } 
        catch (error) 
        {
        console.log(error);
        reject(error);
        // throw new InternalServerError();
        } 
    });
    return promise;
     
}

//=======================UPI============================  

async function userCreateUpi(upi_id:string, user_id:number){
    console.log('POST /api/user/createupi API call made');
    //const { upi_id, user_id } = req.body;
    let promise = new Promise(async(resolve, reject) =>
    {
        try {
            const connection = await DbUtils.getConnection();
            if(!await connection.manager.findOne(User, { where: { User_Id : user_id } })) {
                console.log("User is not available");
                reject ("User is not available");
                return;
                //return res.status(409).json({ message: 'User is not available'});
            }
            else if(await connection.manager.findOne(UPI, { where: { UPI_Id : upi_id } })) {
                console.log("The UPI Id has been taken, please try another");
                reject ("The UPI Id has been taken, please try another");
                return;
                //return res.status(409).json({ message: 'The UPI Id has been taken, please try another'});
            }

            let upi = new UPI();
            upi.UPI_Id = upi_id;
            upi.User = await connection.manager.findOne(User, { where: { User_Id : user_id } });
            await connection.manager.save(upi);

            resolve("UPI added successfully");
            console.log("UPI added successfully");
            //return res.status(200).json({ message: 'UPI added successfully' });  
        } 
        catch (error) 
        {
        console.log(error);
        reject(error);
        // throw new InternalServerError();
        } 
    });
    return promise;
}

async function usersUpiData(user_id:number) {
    console.log('POST /api/user/usersupidata API call made');
    //const { user_id } = req.body;
    let promise = new Promise(async(resolve, reject) =>
    {    
        try {
            const connection = await DbUtils.getConnection();
            let data = await connection.createQueryBuilder()
            .select(['up.U_Id"U_Id"', 'up.UPI_Id"UPI_Id"',                                                                                                                                                                                                                                                                                                                                           
            'u.User_Name"User_Name"'])
            .from(UPI, 'up')
            .leftJoin('up.User', 'u')
            .where("u.User_Id = :id", { id: user_id})
            .getRawMany();

            console.log("all users:", data);
            if (data) {
                resolve(data);
                //return res.status(200).json(data);
            }
            console.log("Data not available");
            reject ("Data not available");
            //return res.status(404).json({ message: 'Data not available' });   
        } 
        catch (error) 
        {
        console.log(error);
        reject(error);
        // throw new InternalServerError();
        } 
    });
    return promise;
     
}

//=======================Lobby Structure============================  

async function userSearchUserLobbyStructureData(entry_fees:number, player_count:number) {
    console.log('POST /api/user/searchuserlobbystructuredata API call made');
    //const { entry_fees, player_count } = req.body;
    let promise = new Promise(async(resolve, reject) =>
    {    
        try {
            const connection = await DbUtils.getConnection();
            let data = await connection.createQueryBuilder()
            .select(['lbs.Lobby_Structure_Id"Lobby_Structure_Id"', 'lbs.Lobby_Structure_Name"Lobby_Structure_Name"',
            'lbs.Lobby_Entry_fees"Lobby_Entry_fees"','lbs.Lobby_Per_cut"Lobby_Per_cut"',
            'lbs.No_of_Players"No_of_Players"', 'ad.Admin_Name"Admin_Name"' ])
            .from(Lobby_Structure, 'lbs')
            .leftJoin('lbs.Admin', 'ad')
            .where("lbs.Lobby_Entry_fees = :id", { id: entry_fees})
            .andWhere("lbs.No_of_Players = :idb", { idb: player_count})
            .andWhere("lbs.IsActive = :ida", { ida: true})
            .getRawMany();

            //console.log("all users:", data[0].User_Image.toString());
            console.log("all users:", data);
            if (data) {
                resolve(data);
                //return res.status(200).json(data);
            }
            console.log("Data not available");
            reject ("Data not available");
            //return res.status(404).json({ message: 'Data not available' });   
        } 
        catch (error) 
        {
        console.log(error);
        reject(error);
        // throw new InternalServerError();
        } 
    });
    return promise;
     
}

//=======================Inventory============================  

async function userSearchUserInventoryData(user_id:number) {
    console.log('POST /api/user/searchuserinventorydata API call made');
    //const { user_id } = req.body;
    let promise = new Promise(async(resolve, reject) =>
    {    
        try {
            const connection = await DbUtils.getConnection();
            let data1 = await connection.createQueryBuilder()
            .select(['in.Inventory_Id"Inventory_Id"', 'in.Emoji"Emoji"',
            'in.Dice"Dice"'])
            .from(Inventory, 'in')
            .where("in.User = :id", { id: user_id})
            .getRawMany();

            let data = {"Inventory_Id": data1[0].Inventory_Id,"Emoji":JSON.parse(data1[0].Emoji),"Dice":JSON.parse(data1[0].Dice)};

            //console.log("all users:", data[0].User_Image.toString());
            console.log("all users:", data);
            if (data) {
                resolve(data);
                //return res.status(200).json(data);
            }
            console.log("Data not available");
            reject ("Data not available");
            //return res.status(404).json({ message: 'Data not available' });   
        } 
        catch (error) 
        {
        console.log(error);
        reject(error);
        // throw new InternalServerError();
        } 
    });
    return promise;
     
}

async function updateByEmoji(user_id:number, emoji:object) {
    console.log('PUT /api/user/updatebuyemoji API call made');
    //const { user_id, emoji } = req.body;
    let promise = new Promise(async(resolve, reject) =>
    {
        try {
            const connection = await DbUtils.getConnection();
            const exists = await connection.manager.findOne(Inventory, { where: { User: user_id } });
            if(!exists) {
                console.log("Inventory is not exist");
                reject ("Inventory is not exist");
                //return res.status(404).json({ message: 'Inventory is not exist'});
            }
            const data = await connection
            .createQueryBuilder()
            .update(Inventory)
            .set({ 
                Emoji : emoji
            })
            .where("User = :id", { id: user_id })
            .execute();

            resolve("User Emoji data updated successfully");
            console.log("User Emoji data updated successfully");
            //res.status(200).json({ message: 'User Emoji data updated successfully' });   
            
        } 
        catch (error) 
        {
        console.log(error);
        reject(error);
        // throw new InternalServerError();
        } 
    });
    return promise;
}

async function updateByDice(user_id:number, dice:object) {
    console.log('PUT /api/user/updatebuydice API call made');
    //const { user_id, dice } = req.body;
    let promise = new Promise(async(resolve, reject) =>
    {
        try {
            const connection = await DbUtils.getConnection();
            const exists = await connection.manager.findOne(Inventory, { where: { User: user_id } });
            if(!exists) {
                console.log("Inventory is not exist");
                reject("Inventory is not exist");
                //return res.status(404).json({ message: 'Inventory is not exist'});
            }
            const data = await connection
            .createQueryBuilder()
            .update(Inventory)
            .set({ 
                Dice : dice
            })
            .where("User = :id", { id: user_id })
            .execute();

            resolve("User Dice data updated successfully");
            console.log("User Dice data updated successfully");
            //res.status(200).json({ message: 'User Dice data updated successfully' });   
            
        } 
        catch (error) 
        {
        console.log(error);
        reject(error);
        // throw new InternalServerError();
        } 
    });
    return promise;
}

//=======================Achievements============================

async function AllAchievementss() {
    console.log('GET /api/user/allachievementss API call made');
    let promise = new Promise(async(resolve, reject) =>
    {    
        try {
            const connection = await DbUtils.getConnection();
            let data = await connection.manager.find(Achievements);

            //console.log("all users:", data[0].User_Image.toString());
            console.log("all users:", data);
            if (data) {
                resolve(data);
                //return res.status(200).json(data);
            }
            console.log("Data not available");
            reject ("Data not available");
            //return res.status(404).json({ message: 'Data not available' });   
        } 
        catch (error) 
        {
        console.log(error);
        reject(error);
        // throw new InternalServerError();
        } 
    });
    return promise;
     
}


async function AllNormalLobbyStructure() {
    console.log('GET /api/admin/allnormallobbystructure API call made');
    let promise = new Promise(async(resolve, reject) =>
    { 
        try {
            const connection = await DbUtils.getConnection();
            
            let data = await connection.createQueryBuilder()
            .select(['lbs.Lobby_Structure_Id"Lobby_Structure_Id"', 'lbs.Lobby_Structure_Name"Lobby_Structure_Name"',
            'lbs.Lobby_Entry_fees"Lobby_Entry_fees"','lbs.Lobby_Per_cut"Lobby_Per_cut"',
            'lbs.No_of_Players"No_of_Players"','lbs.IsActive"IsActive"'])
            .from(Lobby_Structure, 'lbs')
            .where("lbs.IsActive = :id", { id: true })
            .andWhere("lbs.Lobby_Entry_fees < 1000")
            .getRawMany();

            //console.log("all users:", data[0].User_Image.toString());
            console.log("all users:", data);
            if (data) {
                resolve(data);
                //return res.status(200).json(data);
            }
            console.log("Data not available");
            reject ("Data not available");
            //return res.status(404).json({ message: 'Data not available' });   
        } 
        catch (error) 
        {
        console.log(error);
        reject(error);
        // throw new InternalServerError();
        } 
    });
    return promise;
     
}
async function AllVipLobbyStructure() {
    console.log('GET /api/admin/allviplobbystructure API call made');
    let promise = new Promise(async (resolve, reject) => {
        try {
            const connection = await DbUtils.getConnection();

            let data = await connection.createQueryBuilder()
                .select(['lbs.Lobby_Structure_Id"Lobby_Structure_Id"', 'lbs.Lobby_Structure_Name"Lobby_Structure_Name"',
                    'lbs.Lobby_Entry_fees"Lobby_Entry_fees"', 'lbs.Lobby_Per_cut"Lobby_Per_cut"',
                    'lbs.No_of_Players"No_of_Players"', 'lbs.IsActive"IsActive"'])
                .from(Lobby_Structure, 'lbs')
                .where("lbs.IsActive = :id", {id: true})
                .andWhere("lbs.Lobby_Entry_fees > 1000")
                .getRawMany();

            //console.log("all users:", data[0].User_Image.toString());
            console.log("all users:", data);
            if (data) {
                resolve(data);
                //return res.status(200).json(data);
            }
            console.log("Data not available");
            reject("Data not available");
            //return res.status(404).json({ message: 'Data not available' });   
        } catch (error) {
            console.log(error);
            reject(error);
            // throw new InternalServerError();
        }
    });
    return promise;
}
async function Cut_update_money(user_id:Array<number>, Money:number)
{
    console.log('PUT /api/user/updatewalletdatas API call made');
    // const { user_id, user_amount } = req.body;
    let promise = new Promise(async(resolve, reject) => {
        try {
            console.log(user_id);
            const connection = await DbUtils.getConnection();
            const exists1 = await connection.manager.find(User, {where: {User_Id: In(user_id)}});
            const exists = await connection.manager.find(Wallet, {where: {User: In(user_id)}});
            if (exists.length < user_id.length) {
                console.log("User is not exist");
                reject("User is not exist");
                // return res.status(404).json({ message: 'User is not exist'});
            }

            console.log(exists);
            const data1 = await connection
                .createQueryBuilder()
                .update(Wallet)
                .set({
                    User_Amount: () => `user_amount + ${Money}`
                })
                .where({User: In(user_id)})
                .andWhere(`user_amount >= ${Money}`)
                .execute();
            console.log(data1);

            let data = await connection.createQueryBuilder()
                .select(['u.User_Id"User_Id"', 'w.User_Amount"User_Amount"', 'w.Wallet_Id"Wallet_Id"'])
                .from(Wallet, 'w')
                .leftJoin('w.User', 'u')
                .where({User: In(user_id)})
                .getRawMany();
            console.log("updated: ", data);

            resolve(data);
            console.log("User Wallet data updated successfully");
            // res.status(200).json({ message: 'User Wallet data updated successfully' });

        } catch (error) {
            console.log(error);
            reject(error);
            //throw new InternalServerError();
        }
    });
    return promise;
}

async function updateAuthData(UserUI:Array<number>,  playing:boolean ) {
    console.log('PUT /api/user/updateauthdata API call made');
    //const { user_id, user_token, playing, device_id } = req.body;
    let promise = new Promise(async(resolve, reject) =>
    {
        try {
            const connection = await DbUtils.getConnection();
            const exists1 = await connection.manager.find(User, { where: { User_Id: In(UserUI) } });
            const exists = await connection.manager.find(Auth, { where: { User: In(UserUI) }});
            if(exists.length < UserUI.length) {
                console.log("User is not exist");
                reject("User is not exist");
                // return res.status(404).json({ message: 'User is not exist'});
            }
            const data1 = await connection
                .createQueryBuilder()
                .update(Auth)
                .set({
                    Currently_Playing : playing
                })
                .where( { User: In(UserUI)})
                .execute();
            console.log(data1);

            resolve("Auth updated successfully");
            console.log("Auth updated successfully");
            // return res.status(200).json({ message: 'Auth updated successfully' });
        } catch (error) {
            console.log(error);
            reject(error);
            //throw new InternalServerError();
        }
    });
    return promise;
}

export const userControllerSocket = { Cut_update_money, enterExitLobby, userLogout, updateProfile, updateFbUserId, updateReferral, searchUserData, userWallet, searchUserImage,
    updateWalletData, userGameData, updateAuthData, updateGameData, createPayment, userPayment, userCreateLobby, userSearchLobbyData, userSearchUserLobbyData, userUpdateLobbyData, userDeleteLobby,
    userCreateBank, userCreateUpi, userCreateLobbyHistory, userSearchLobbyHistoryData, userSearchUserLobbyHistoryData,userCreateBuddies,userBuddiesData,searchBuddiesData,requestBuddiesData,updateBuddiesData,
    usersBankData, usersUpiData, deleteBuddies, searchUserDetail, userSearchUserLobbyStructureData, userSearchUserInventoryData, updateByEmoji, updateByDice, AllAchievementss, AllNormalLobbyStructure, AllVipLobbyStructure }
