import {Request, Response, NextFunction, } from "express";
import * as jwt from "jsonwebtoken";
import config from "../config/config";
import { Admin } from "../entity/Admin";
import { Auth } from "../entity/Auth";
import { Unauthorized } from "../errors/InstancesCE";
import { DbUtils } from "../utils/DbUtils";

export const checkJwtToken = async (req:Request,res:Response, next:NextFunction) =>{
    let token = req.headers.authorization.split(' ')[1];
    if(token){
        let decoded = await jwt.verify(token, config.jwtSecret);
        console.log("decoded",decoded);
        const connection = await DbUtils.getConnection();
        let nm = decoded.username;
        let fetched = await connection.manager.findOne(Admin, { where: { Admin_Name : nm, Token: token } });
        if(fetched || fetched != undefined){
            next();
        }
        else{
            throw new Unauthorized("You are unauthorized")
        }
    }
    else{
        throw new Unauthorized("Token Needed")
    }
}

export const clientToken = async (req: Request, res:Response, next:NextFunction) => {
        const connection = await DbUtils.getConnection();
        let auth_header = req.headers.authorization || '0 0';
        console.log(auth_header.split(' '));
        let userid = auth_header.split(' ')[0];
        let token = auth_header.split(' ')[1];
        const tokendata = await connection.manager.findOne(Auth, { where: { User: userid, Token: token } });
        
        console.log(tokendata);
        if(tokendata){
           return next();
        }else{
            console.log("TokenData Does Not Exists");
            throw new Unauthorized();
        }

}

