import { Request, Response, NextFunction } from 'express';
import { CustomError } from '../errors/CustomError';
import {logger} from '../../../src/logger';
import chalk from 'chalk';

export const errorHandler = (err: Error, req:Request, res:Response, next:NextFunction) =>{
console.log("SOMETHING WENT WRONG")
const error = chalk.bold.red;
console.error(error(err)); 
logger.error(err.message, ()=>{
  console.log("log created")
})
if(err instanceof CustomError){
  console.log(true);
  return res.status(err.statusCode).send({errors:err.serializeErrors()});
  
}else{
  res.status(400).send({
   errors:[{ message:'Something Went Wrong' }]
  })
}
}