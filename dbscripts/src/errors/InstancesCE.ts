import { CustomError } from './CustomError';


export class InternalServerError extends CustomError{
    statusCode = 500
    message: 'Internal Server Error:Something went wrong'
    constructor(messages?:string){
        super('Internal Server Error')
        
        Object.setPrototypeOf(this, InternalServerError.prototype)
    }
    serializeErrors(){
        return [{message:this.message}]
    }
}

export class Unauthorized extends CustomError{
    statusCode = 401
    message = 'Unauthorized: Access Denied'
    constructor(message?:string){
        super('Unauthorized attempt')
        this.message = message;
        Object.setPrototypeOf(this, Unauthorized.prototype)
    }
    serializeErrors(){
        return [{message:this.message}]
    }
}

export class NotFoundError extends CustomError{
    statusCode = 404
    msg = 'Resource not found:Check Your Input'
    constructor(message:string){
        super('Not Found')
        this.msg = message
        Object.setPrototypeOf(this, NotFoundError.prototype)
    }
    serializeErrors(){
        return [{message:this.msg}]
    }
}

export class DuplicateDataError extends CustomError{
    statusCode = 409
    msg = 'Data has been already taken, please try another'
    constructor(message?:string){
        super('Duplicate Data')
        this.msg = message
        Object.setPrototypeOf(this, DuplicateDataError.prototype)
    }
    serializeErrors(){
        return [{message:this.msg}]
    }
}

export class BannedError extends CustomError{
    statusCode = 403
    msg = 'Your account is Banned'
    constructor(message?:string){
        super('Account Banned')
        this.msg = message
        Object.setPrototypeOf(this, BannedError.prototype)
    }
    serializeErrors(){
        return [{message:this.msg}]
    }
}

export class WrongDataError extends CustomError{
    statusCode = 400
    msg = 'Incorrect Data'
    constructor(message?:any){
        super('Incorrect Data')
        this.msg = message
        Object.setPrototypeOf(this, WrongDataError.prototype)
    }
    serializeErrors(){
        return [{message:this.msg}]
    }
}

export class NotModifiedError extends CustomError{
    statusCode = 304
    msg = 'Data not modified'
    constructor(message?:string){
        super('Not Modified')
        this.msg = message
        Object.setPrototypeOf(this, NotModifiedError.prototype)
    }
    serializeErrors(){
        return [{message:this.msg}]
    }
}
