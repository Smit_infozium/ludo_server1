import express from 'express';
import { body } from 'express-validator';
import { adminController } from '../controllers/adminControllers';
import {checkJwtToken} from '../middlewares/checkJWT';

const router = express.Router();

router.get('/check',async (req, res, next) => {

    console.log(req.hostname);
    return res.send(`The Server is up on ${req.hostname}!!`);
});

router.post('/createadmin', checkJwtToken,
    [ body('admin_name').isAlphanumeric(), body('admin_role').isIn(['Admin', 'SubAdmin']),
        body('admin_password').isAlphanumeric() ], adminController.createAdmin);

router.put('/adminlogin', [ body('admin_name').isAlphanumeric(),
    body('admin_password').isAlphanumeric() ], adminController.adminLogin);

router.get('/admindata',checkJwtToken, adminController.adminData);

router.get('/bannedadmindata',checkJwtToken, adminController.BannedadminData);

router.post('/searchadmindata',checkJwtToken, [ body('admin_id').isNumeric() ],
    adminController.searchAdminData);

router.put('/updateadmindata',checkJwtToken, [ body('admin_id').isNumeric(),
        body('admin_name').isAlphanumeric(), body('admin_role').isIn(['Admin', 'SubAdmin']),
        body('admin_password').isAlphanumeric(), body('is_active').isBoolean() ],
    adminController.updateAdminData);

router.put('/updatebannedadmindata',checkJwtToken, [ body('admin_id').isNumeric(),
    body('is_active').isBoolean() ], adminController.updateBannedAdminData);

router.delete('/deleteadmin',checkJwtToken, [ body('admin_id').isNumeric() ],
    adminController.deleteAdmin);

router.get('/usersalldetails',checkJwtToken, adminController.userAllDetails);

router.post('/usersdetails',checkJwtToken, [ body('user_id').isNumeric() ],
    adminController.userDetails);

router.get('/bannedusers', checkJwtToken, adminController.bannedUsers);

router.put('/updateactiveuser', checkJwtToken, [ body('user_id').isNumeric(),
    body('is_active').isBoolean() ],  adminController.updateActiveUser);

router.get('/userswallet', checkJwtToken, adminController.usersWallet);

router.post('/userpayments', checkJwtToken, [ body('user_id').isNumeric() ],
    adminController.userPayments);

router.post('/createvariation', checkJwtToken, [ body('variation_name').isAlphanumeric(),
        body('variation_initial').isAlphanumeric(), body('player_count').isIn([ 2, 3, 4]) ],
    adminController.CreateVariatoin);

router.get('/variations', checkJwtToken, adminController.Variations);

router.post('/searchvariations', checkJwtToken, [ body('variation_id').isNumeric() ],
    adminController.SearchVariation);

router.put('/updatevariationsData',checkJwtToken, [ body('variation_id').isNumeric(),
    body('variation_name').isAlphanumeric(), body('variation_initial').isAlphanumeric(),
    body('player_count').isIn([ 2, 3, 4]) ],  adminController.updateVariationsData);

router.delete('/deletevariation',checkJwtToken, [ body('variation_id').isNumeric() ],
    adminController.deleteVariation);

router.post('/searchlobbyhistorydata', checkJwtToken, [ body('lobby_id').isNumeric() ],
    adminController.userSearchLobbyHistoryData);

router.post('/searchuserlobbyhistorydata', checkJwtToken, [ body('user_id').isNumeric() ],
    adminController.userSearchUserLobbyHistoryData);

router.get('/leisurealldata', checkJwtToken, adminController.LeisureAllData);

router.post('/leisuredata', checkJwtToken, [ body('lobby_id').isNumeric() ],
    adminController.LeisureData);

router.post('/userleisuredata', checkJwtToken, [ body('user_id').isNumeric() ],
    adminController.userLeisureData);

router.post('/createachievements', checkJwtToken, [ body('title').isString(),
    body('statement').isString(), body('total_count').isNumeric(),
    body('reward').isNumeric() ], adminController.createAchievements);

router.get('/allachievements', checkJwtToken, adminController.AllAchievements);

router.put('/updateachievementsData',checkJwtToken, [ body('achievements_id').isNumeric(),
    body('title').isString(), body('statement').isString(), body('total_count').isNumeric(),
    body('reward').isNumeric() ],  adminController.updateAchievementsData);

router.delete('/deleteAchievements',checkJwtToken, [ body('achievements_id').isNumeric() ],
    adminController.deleteAchievements);

router.get('/allpayments', checkJwtToken, adminController.AllPayments);

router.get('/alllobbies', checkJwtToken, adminController.AllLobbies);

router.get('/alllobbiescommision', checkJwtToken, adminController.AllLobbiesCommision);

router.post('/createlobbystructure', checkJwtToken, [ body('ls_name').isAlphanumeric(),
        body('ls_entry_fees').isNumeric(), body('ls_per_cut').isNumeric(),
        body('ls_players').isIn([2, 3, 4]), body('admin_id').isNumeric() ],
    adminController.createLobbyStructure);

router.post('/alllobbystructure', checkJwtToken, [ body('admin_id').isNumeric() ],
    adminController.AllLobbyStructure);

router.post('/bannedalllobbystructure', checkJwtToken, [ body('admin_id').isNumeric() ],
    adminController.BannedAllLobbyStructure);

router.put('/updatelobbystructure',checkJwtToken, [ body('ls_id').isNumeric(),
        body('ls_name').isAlphanumeric(), body('ls_entry_fees').isNumeric(),
        body('ls_per_cut').isNumeric(), body('ls_players').isIn([2, 3, 4]) ],
    adminController.updateLobbyStructure);

router.put('/updateactivelobbystructure',checkJwtToken, [ body('ls_id').isNumeric(),
    body('is_active').isBoolean() ], adminController.updateActiveLobbyStructure);

router.delete('/deletelobbystructure',checkJwtToken, [ body('ls_id').isNumeric()],
    adminController.deleteLobbyStructure);

router.get('/dashboard', checkJwtToken, adminController.adminDashboard);

router.put('/regeneratetoken', checkJwtToken, [ body('Admin_Id').isNumeric(),
        body('Admin_Name').isAlphanumeric(), body('Admin_Roles').isIn(['Admin', 'SubAdmin']) ],
    adminController.adminRegenrateToken);

module.exports = router;
