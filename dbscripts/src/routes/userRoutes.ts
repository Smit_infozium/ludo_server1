import express from 'express';
import {userController} from '../controllers/userControllers';
import { clientToken } from '../middlewares/checkJWT';

const router = express.Router();

router.get('/check',async (req, res, next) => {
 
    console.log(req.hostname);
    res.send(`The Server is up on ${req.hostname}!!`);
});
router.post('/register', userController.userRegister);
router.post('/verifymobile', userController.verifyMobile); 
router.post('/checkreferral', userController.checkReferall);
router.post('/login', userController.userLogin);
router.put('/enterexitLobby', clientToken, userController.enterExitLobby);//s
router.put('/logout', clientToken, userController.userLogout);//s
router.put('/updateprofile', clientToken, userController.updateProfile);//s
router.put('/changepassword', clientToken, userController.changePassword);
router.put('/activeuser', clientToken, userController.activeUser);
router.post('/updatereferral', clientToken, userController.updateReferral);//s
router.post('/searchuserdata', clientToken, userController.searchUserData);//s
router.post('/searchuserimage', clientToken, userController.searchUserImage);//s
router.post('/searchuserdetail', clientToken, userController.searchUserDetail);//s

router.post('/userwallet', clientToken, userController.userWallet);//s
router.put('/updatewalletdata', clientToken, userController.updateWalletData);//s
router.post('/userauth', clientToken, userController.userAuth);
router.put('/updateauthdata', clientToken, userController.updateAuthData);
router.post('/usergamedata', clientToken, userController.userGameData);//s
router.put('/updategamedata', clientToken, userController.updateGameData);//s
router.post('/createpayment', clientToken, userController.createPayment);//s
router.post('/userpayments', clientToken, userController.userPayment);//s

router.post('/createlobby', clientToken, userController.userCreateLobby);//s
router.post('/searchlobbydata', clientToken, userController.userSearchLobbyData);//s
router.post('/searchuserlobbydata', clientToken, userController.userSearchUserLobbyData);//s
router.put('/updatelobbydata', clientToken, userController.userUpdateLobbyData);//s
router.delete('/deletelobby', clientToken, userController.userDeleteLobby);//s
router.post('/createlobbyhistory', clientToken, userController.userCreateLobbyHistory);//s
router.post('/searchlobbyhistorydata', clientToken, userController.userSearchLobbyHistoryData);//s
router.post('/searchuserlobbyhistorydata', clientToken, userController.userSearchUserLobbyHistoryData);//s
router.post('/createbuddies', clientToken, userController.userCreateBuddies);//s
router.post('/buddiesdata', clientToken, userController.userBuddiesData);//s
router.post('/searchbuddiesdata', clientToken, userController.searchBuddiesData);//s
router.post('/reqbuddiesdata', clientToken, userController.requestBuddiesData);//s
router.put('/updatebuddiesdata', clientToken, userController.updateBuddiesData);//s
router.delete('/deletebuddies', clientToken, userController.deleteBuddies);//s
router.post('/regeneratetoken', clientToken, userController.reGenerateToken);

router.post('/createbank', clientToken, userController.userCreateBank);//s
router.post('/createupi', clientToken, userController.userCreateUpi);//s
router.post('/usersbankdata', clientToken, userController.usersBankData);//s
router.post('/usersupidata', clientToken, userController.usersUpiData);//s
router.post('/searchuserlobbystructuredata', clientToken, userController.userSearchUserLobbyStructureData);//s
router.post('/searchuserinventorydata', clientToken, userController.userSearchUserInventoryData);//s
router.put('/updatebuyemoji', clientToken, userController.updateByEmoji);//s
router.put('/updatebuydice', clientToken, userController.updateByDice);//s

router.get('/allachievementss', clientToken, userController.AllAchievementss);//s

router.get('/allnormallobbystructure', clientToken, userController.AllNormalLobbyStructure);//s
router.get('/allviplobbystructure', clientToken, userController.AllVipLobbyStructure);//s

module.exports = router;

