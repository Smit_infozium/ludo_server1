import {MigrationInterface, QueryRunner} from "typeorm";

export class test1631857296615 implements MigrationInterface {
    name = 'test1631857296615'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query("CREATE TABLE `Achievements` (`Achievements_Id` int NOT NULL AUTO_INCREMENT, `Title` varchar(255) NOT NULL, `Statement` varchar(255) NOT NULL, `Total_Count` int NOT NULL, `Reward` float NOT NULL, PRIMARY KEY (`Achievements_Id`)) ENGINE=InnoDB");
        await queryRunner.query("CREATE TABLE `Admin` (`Admin_Id` int NOT NULL AUTO_INCREMENT, `Admin_Name` varchar(255) NOT NULL, `Admin_Roles` enum ('Admin', 'SubAdmin') NOT NULL DEFAULT 'SubAdmin', `Admin_Password` varchar(255) NOT NULL, `IsActive` tinyint NOT NULL DEFAULT 1, `Token` varchar(255) NULL, UNIQUE INDEX `IDX_1795a3b7fd18daa1e14f3ed2e7` (`Admin_Name`), PRIMARY KEY (`Admin_Id`)) ENGINE=InnoDB");
        await queryRunner.query("CREATE TABLE `GameData` (`GameData_Id` int NOT NULL AUTO_INCREMENT, `Total_Wins` int NOT NULL, `Total_Loss` int NOT NULL, `Token_Captured` int NOT NULL, `Opp_Token_Captured` int NOT NULL, `Achievements` json NULL, `userUserId` bigint NULL, UNIQUE INDEX `REL_dc361d6804a28412448c66e15f` (`userUserId`), PRIMARY KEY (`GameData_Id`)) ENGINE=InnoDB");
        await queryRunner.query("CREATE TABLE `Wallet` (`Wallet_Id` int NOT NULL AUTO_INCREMENT, `User_Amount` float NOT NULL, `User_Bonus_Cash` float NOT NULL, `User_Credit_Amount` float NOT NULL, `User_Debit_Amount` float NOT NULL, `User_Win_Amount` float NOT NULL, `User_Loss_Amount` float NOT NULL, `userUserId` bigint NULL, UNIQUE INDEX `REL_2f06cd23c62a2b81914b8b94fe` (`userUserId`), PRIMARY KEY (`Wallet_Id`)) ENGINE=InnoDB");
        await queryRunner.query("CREATE TABLE `User` (`User_Id` bigint NOT NULL, `User_Name` varchar(255) NOT NULL, `User_DisplayName` varchar(255) NOT NULL, `User_Password` varchar(255) NOT NULL, `User_Country` varchar(255) NOT NULL, `User_Mobile_Number` bigint NOT NULL, `User_Image` longblob NOT NULL, `IsActive` tinyint NOT NULL, `Priviledge` tinyint NOT NULL, `Fb_User_Id` varchar(255) NULL, `Reference_Id` varchar(255) NOT NULL, `referenceByUserId` bigint NULL, UNIQUE INDEX `IDX_7e96301ce0501574fb3403c896` (`User_Name`), UNIQUE INDEX `IDX_6abae0bce2995fd94433cd05e9` (`User_Mobile_Number`), UNIQUE INDEX `IDX_e71c431aff80b0b4554d732b37` (`Reference_Id`), PRIMARY KEY (`User_Id`)) ENGINE=InnoDB");
        await queryRunner.query("CREATE TABLE `Auth` (`Auth_Id` int NOT NULL AUTO_INCREMENT, `Token` varchar(255) NOT NULL, `Currently_Playing` tinyint NOT NULL, `Device_Id` varchar(255) NULL, `userUserId` bigint NULL, UNIQUE INDEX `REL_cbde71a994e4ba2187bd1578b0` (`userUserId`), PRIMARY KEY (`Auth_Id`)) ENGINE=InnoDB");
        await queryRunner.query("CREATE TABLE `Bank` (`Bank_Id` int NOT NULL AUTO_INCREMENT, `Account_Holder_Name` varchar(255) NOT NULL, `IFSC_Code` varchar(255) NOT NULL, `Account_Number` varchar(255) NOT NULL, `userUserId` bigint NULL, PRIMARY KEY (`Bank_Id`)) ENGINE=InnoDB");
        await queryRunner.query("CREATE TABLE `Buddies` (`Buddies_Id` int NOT NULL AUTO_INCREMENT, `Req_status` enum ('Pending', 'Approved', 'Rejected', 'Block') NOT NULL DEFAULT 'Pending', `requestByUserId` bigint NULL, `requestToUserId` bigint NULL, PRIMARY KEY (`Buddies_Id`)) ENGINE=InnoDB");
        await queryRunner.query("CREATE TABLE `Inventory` (`Inventory_Id` int NOT NULL AUTO_INCREMENT, `Emoji` json NULL, `Dice` json NULL, `userUserId` bigint NULL, UNIQUE INDEX `REL_1ba426efb90c42e464d87d97bb` (`userUserId`), PRIMARY KEY (`Inventory_Id`)) ENGINE=InnoDB");
        await queryRunner.query("CREATE TABLE `Variation` (`Variation_Id` int NOT NULL AUTO_INCREMENT, `Variation_Name` varchar(255) NOT NULL, `Variation_Initial` varchar(3) NOT NULL, `Player_Count` int NOT NULL, PRIMARY KEY (`Variation_Id`)) ENGINE=InnoDB");
        await queryRunner.query("CREATE TABLE `Lobby` (`Lobby_Id` int NOT NULL AUTO_INCREMENT, `Lobby_Name` varchar(255) NOT NULL, `Lobby_Total_Amount` float NOT NULL, `Lobby_Buy_In` float NOT NULL, `Lobby_Commision_Amount` float NOT NULL, `Room_Id` varchar(255) NOT NULL, `Lobby_Status` enum ('Commit', 'Rollback', 'Null') NOT NULL DEFAULT 'Null', `Recorded_Move` json NULL, `Lobby_Start_Time_Date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP, `Lobby_End_Time_Date` timestamp NULL, `variationVariationId` int NULL, `lobbyOwnerUserId` bigint NULL, PRIMARY KEY (`Lobby_Id`)) ENGINE=InnoDB");
        await queryRunner.query("CREATE TABLE `Lobby_History` (`Lobby_History_Id` int NOT NULL AUTO_INCREMENT, `Winning_Status` enum ('W1', 'W2', 'W3', 'W4', 'W5', 'Loss') NOT NULL DEFAULT 'Loss', `Winning_Price` float NOT NULL, `Powerups_used` int NOT NULL, `Total_Amount_Spend` float NOT NULL, `lobbyLobbyId` int NULL, `userUserId` bigint NULL, PRIMARY KEY (`Lobby_History_Id`)) ENGINE=InnoDB");
        await queryRunner.query("CREATE TABLE `Lobby_Structure` (`Lobby_Structure_Id` int NOT NULL AUTO_INCREMENT, `Lobby_Structure_Name` varchar(255) NOT NULL, `Lobby_Entry_fees` float NOT NULL, `Lobby_Per_cut` float NOT NULL DEFAULT '3', `No_of_Players` int NOT NULL, `IsActive` tinyint NOT NULL, `adminAdminId` int NULL, PRIMARY KEY (`Lobby_Structure_Id`)) ENGINE=InnoDB");
        await queryRunner.query("CREATE TABLE `Payment` (`Payment_Id` int NOT NULL AUTO_INCREMENT, `Is_Credit` tinyint NOT NULL, `Payment_Amount` float NOT NULL, `Payment_Time_Date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP, `Transaction_Id` bigint NOT NULL, `Payment_status` enum ('Success', 'failed') NOT NULL, `userUserId` bigint NULL, PRIMARY KEY (`Payment_Id`)) ENGINE=InnoDB");
        await queryRunner.query("CREATE TABLE `UPI` (`U_Id` int NOT NULL AUTO_INCREMENT, `UPI_Id` varchar(255) NOT NULL, `userUserId` bigint NULL, PRIMARY KEY (`U_Id`)) ENGINE=InnoDB");
        await queryRunner.query("ALTER TABLE `GameData` ADD CONSTRAINT `FK_dc361d6804a28412448c66e15f6` FOREIGN KEY (`userUserId`) REFERENCES `User`(`User_Id`) ON DELETE CASCADE ON UPDATE CASCADE");
        await queryRunner.query("ALTER TABLE `Wallet` ADD CONSTRAINT `FK_2f06cd23c62a2b81914b8b94fe5` FOREIGN KEY (`userUserId`) REFERENCES `User`(`User_Id`) ON DELETE CASCADE ON UPDATE CASCADE");
        await queryRunner.query("ALTER TABLE `User` ADD CONSTRAINT `FK_a49c81a9c488bbf6d6c143ba7e6` FOREIGN KEY (`referenceByUserId`) REFERENCES `User`(`User_Id`) ON DELETE CASCADE ON UPDATE CASCADE");
        await queryRunner.query("ALTER TABLE `Auth` ADD CONSTRAINT `FK_cbde71a994e4ba2187bd1578b0c` FOREIGN KEY (`userUserId`) REFERENCES `User`(`User_Id`) ON DELETE CASCADE ON UPDATE CASCADE");
        await queryRunner.query("ALTER TABLE `Bank` ADD CONSTRAINT `FK_b1ab8cd5fb36a60524a8ae446f1` FOREIGN KEY (`userUserId`) REFERENCES `User`(`User_Id`) ON DELETE CASCADE ON UPDATE CASCADE");
        await queryRunner.query("ALTER TABLE `Buddies` ADD CONSTRAINT `FK_8febd80818598a02b406ab3930e` FOREIGN KEY (`requestByUserId`) REFERENCES `User`(`User_Id`) ON DELETE CASCADE ON UPDATE CASCADE");
        await queryRunner.query("ALTER TABLE `Buddies` ADD CONSTRAINT `FK_d0526cd04f4f9c4eb4f8e712757` FOREIGN KEY (`requestToUserId`) REFERENCES `User`(`User_Id`) ON DELETE CASCADE ON UPDATE CASCADE");
        await queryRunner.query("ALTER TABLE `Inventory` ADD CONSTRAINT `FK_1ba426efb90c42e464d87d97bb5` FOREIGN KEY (`userUserId`) REFERENCES `User`(`User_Id`) ON DELETE CASCADE ON UPDATE CASCADE");
        await queryRunner.query("ALTER TABLE `Lobby` ADD CONSTRAINT `FK_f45071005cb81e04f720d30c686` FOREIGN KEY (`variationVariationId`) REFERENCES `Variation`(`Variation_Id`) ON DELETE CASCADE ON UPDATE CASCADE");
        await queryRunner.query("ALTER TABLE `Lobby` ADD CONSTRAINT `FK_f5ee698b294f5d7d3324405f7e5` FOREIGN KEY (`lobbyOwnerUserId`) REFERENCES `User`(`User_Id`) ON DELETE CASCADE ON UPDATE CASCADE");
        await queryRunner.query("ALTER TABLE `Lobby_History` ADD CONSTRAINT `FK_d82b5f92b3512dae4ed006a46e9` FOREIGN KEY (`lobbyLobbyId`) REFERENCES `Lobby`(`Lobby_Id`) ON DELETE CASCADE ON UPDATE CASCADE");
        await queryRunner.query("ALTER TABLE `Lobby_History` ADD CONSTRAINT `FK_1a87c29ad22db9c94dc89d07177` FOREIGN KEY (`userUserId`) REFERENCES `User`(`User_Id`) ON DELETE CASCADE ON UPDATE CASCADE");
        await queryRunner.query("ALTER TABLE `Lobby_Structure` ADD CONSTRAINT `FK_664a34b7a497ef74ce1665e9f9e` FOREIGN KEY (`adminAdminId`) REFERENCES `Admin`(`Admin_Id`) ON DELETE CASCADE ON UPDATE CASCADE");
        await queryRunner.query("ALTER TABLE `Payment` ADD CONSTRAINT `FK_ee5ec9bfb1e3049348a523991d8` FOREIGN KEY (`userUserId`) REFERENCES `User`(`User_Id`) ON DELETE CASCADE ON UPDATE CASCADE");
        await queryRunner.query("ALTER TABLE `UPI` ADD CONSTRAINT `FK_ea1f1e50dcc7a692ef9872300ae` FOREIGN KEY (`userUserId`) REFERENCES `User`(`User_Id`) ON DELETE CASCADE ON UPDATE CASCADE");
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query("ALTER TABLE `UPI` DROP FOREIGN KEY `FK_ea1f1e50dcc7a692ef9872300ae`");
        await queryRunner.query("ALTER TABLE `Payment` DROP FOREIGN KEY `FK_ee5ec9bfb1e3049348a523991d8`");
        await queryRunner.query("ALTER TABLE `Lobby_Structure` DROP FOREIGN KEY `FK_664a34b7a497ef74ce1665e9f9e`");
        await queryRunner.query("ALTER TABLE `Lobby_History` DROP FOREIGN KEY `FK_1a87c29ad22db9c94dc89d07177`");
        await queryRunner.query("ALTER TABLE `Lobby_History` DROP FOREIGN KEY `FK_d82b5f92b3512dae4ed006a46e9`");
        await queryRunner.query("ALTER TABLE `Lobby` DROP FOREIGN KEY `FK_f5ee698b294f5d7d3324405f7e5`");
        await queryRunner.query("ALTER TABLE `Lobby` DROP FOREIGN KEY `FK_f45071005cb81e04f720d30c686`");
        await queryRunner.query("ALTER TABLE `Inventory` DROP FOREIGN KEY `FK_1ba426efb90c42e464d87d97bb5`");
        await queryRunner.query("ALTER TABLE `Buddies` DROP FOREIGN KEY `FK_d0526cd04f4f9c4eb4f8e712757`");
        await queryRunner.query("ALTER TABLE `Buddies` DROP FOREIGN KEY `FK_8febd80818598a02b406ab3930e`");
        await queryRunner.query("ALTER TABLE `Bank` DROP FOREIGN KEY `FK_b1ab8cd5fb36a60524a8ae446f1`");
        await queryRunner.query("ALTER TABLE `Auth` DROP FOREIGN KEY `FK_cbde71a994e4ba2187bd1578b0c`");
        await queryRunner.query("ALTER TABLE `User` DROP FOREIGN KEY `FK_a49c81a9c488bbf6d6c143ba7e6`");
        await queryRunner.query("ALTER TABLE `Wallet` DROP FOREIGN KEY `FK_2f06cd23c62a2b81914b8b94fe5`");
        await queryRunner.query("ALTER TABLE `GameData` DROP FOREIGN KEY `FK_dc361d6804a28412448c66e15f6`");
        await queryRunner.query("DROP TABLE `UPI`");
        await queryRunner.query("DROP TABLE `Payment`");
        await queryRunner.query("DROP TABLE `Lobby_Structure`");
        await queryRunner.query("DROP TABLE `Lobby_History`");
        await queryRunner.query("DROP TABLE `Lobby`");
        await queryRunner.query("DROP TABLE `Variation`");
        await queryRunner.query("DROP INDEX `REL_1ba426efb90c42e464d87d97bb` ON `Inventory`");
        await queryRunner.query("DROP TABLE `Inventory`");
        await queryRunner.query("DROP TABLE `Buddies`");
        await queryRunner.query("DROP TABLE `Bank`");
        await queryRunner.query("DROP INDEX `REL_cbde71a994e4ba2187bd1578b0` ON `Auth`");
        await queryRunner.query("DROP TABLE `Auth`");
        await queryRunner.query("DROP INDEX `IDX_e71c431aff80b0b4554d732b37` ON `User`");
        await queryRunner.query("DROP INDEX `IDX_6abae0bce2995fd94433cd05e9` ON `User`");
        await queryRunner.query("DROP INDEX `IDX_7e96301ce0501574fb3403c896` ON `User`");
        await queryRunner.query("DROP TABLE `User`");
        await queryRunner.query("DROP INDEX `REL_2f06cd23c62a2b81914b8b94fe` ON `Wallet`");
        await queryRunner.query("DROP TABLE `Wallet`");
        await queryRunner.query("DROP INDEX `REL_dc361d6804a28412448c66e15f` ON `GameData`");
        await queryRunner.query("DROP TABLE `GameData`");
        await queryRunner.query("DROP INDEX `IDX_1795a3b7fd18daa1e14f3ed2e7` ON `Admin`");
        await queryRunner.query("DROP TABLE `Admin`");
        await queryRunner.query("DROP TABLE `Achievements`");
    }

}
