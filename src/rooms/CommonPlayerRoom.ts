import {Room ,Client ,ServerError} from "colyseus";
import { ArraySchema, MapSchema } from "@colyseus/schema";
import {CommonPlayerRoomState} from "../schema/CommonPlayerRoomState";
const logger = require("../helpers/logger");
import { userControllerSocket } from "../../dbscripts/src/controllers/userControllersSocket";
import { cli } from "winston/lib/winston/config";
import { SocketError } from "../helpers/socketerrorhandler";
import { matchMaker } from "colyseus";
import { User_common_data} from "../Untils/User_common_data";

export class CommonPlayerRoom extends Room<CommonPlayerRoomState>
{
    PlayerCount: number = 0;
    Player_Data : any = [];

    async onCreate(options: any)
    {
        console.log("\n*************** Common Player Room ***********");
        this.setState(new CommonPlayerRoomState());
        //
        // The patch-rate is the frequency which state mutations are sent to all clients. (in milliseconds)
        // 1000 / 20 means 20 times per second (50 milliseconds)
        //
        this.setPatchRate(1000 / 20);
    }

    async onJoin(client: Client, options?: any, auth?: any)
    {
        logger.info('#### Client joined!'+JSON.stringify(options));
        this.state.TotalClient +=1;
        this.PlayerCount +=1;
        let data = this.Adding_USer_Data(client,options);
        console.log(data);
        //Adding the SessionID To the List
        console.log(this.state.TotalClient + " "+this.PlayerCount);
        this.initializeMessageHandler();

    }

    onLeave(client: Client, consented?: boolean): void | Promise<any> {
        logger.info("#### Client Leave")
    }

    onDispose(): void | Promise<any> {
        logger.info("#### There us no player Currently ")
    }

    async initializeMessageHandler()
    {
        this.onMessage("GetUserData",(client , message)=>
        {
            userControllerSocket.searchUserData(message.user_id).then(value =>{
                console.log(value);
                client.send("GetUserData",JSON.stringify(value));
            }).catch(value=>{
                SocketError.InternalServerError(client, JSON.stringify(value));
                console.log(value);
            });

        });

        this.onMessage("GetAvailableRoom", (client, message) => {
            console.log("GET ROOM AVAILABLE");
            var rooms = matchMaker.query({ name: "LudoGame" });
            rooms.then((value) => {
                var room = value.find(value1 => value1.roomId == message)
                var isAvailable = false;
                var playerCount = null;

                if (room != null)
                {
                    isAvailable = true;
                    playerCount = room.maxClients;
                }
                else
                {
                    isAvailable = false;
                    playerCount = null;
                }

                var data = {
                    available: isAvailable,
                    players: playerCount
                };

                client.send("GetAvailableRoom",  JSON.stringify(data));

            }).catch(value => {

                var data = {
                    available: false,
                    players: null
                };

                client.send("GetAvailableRoom",  JSON.stringify(data));
            });
        });

        this.onMessage("GetUserImage",(client , message)=>
        {
            userControllerSocket.searchUserImage(message.filename).then(value =>{
                console.log(value);
                client.send("GetUserImage",JSON.stringify(value));
            }).catch(value=>{
                console.log(value);
                SocketError.InternalServerError(client, JSON.stringify(value));
            });

        });

        this.onMessage("GetUserDetails",(client , message)=>
        {
            userControllerSocket.searchUserDetail(message.user_id).then(value =>{
                console.log(value);
                client.send("GetUserDetails",JSON.stringify(value));
            }).catch(value=>{
                console.log(value);
                SocketError.InternalServerError(client, JSON.stringify(value));
            });

        });
        this.onMessage("UpdateUserData",(client , message)=>
        {
            userControllerSocket.updateProfile(message.user_id, message.user_displayname, message.user_country,
                message.user_image).then(value =>{
                console.log(value);
                client.send("UpdateUserData",JSON.stringify(value));
            }).catch(value=>{
                console.log(value);
                SocketError.InternalServerError(client, JSON.stringify(value));
            });

        });
        this.onMessage("UpdateFbId",(client , message)=>
        {
            userControllerSocket.updateFbUserId(message.user_id, message.fb_user_id).then(value =>{
                console.log(value);
                client.send("UpdateFbId",JSON.stringify(value));
            }).catch(value=>{
                console.log(value);
                SocketError.InternalServerError(client, JSON.stringify(value));
            });

        });
        this.onMessage("GetUserGameData",(client , message)=>
        {
            userControllerSocket.userGameData(message.user_id).then(value =>{
                console.log(value);
                client.send("GetUserGameData",JSON.stringify(value));
            }).catch(value=>{
                console.log(value);
                SocketError.InternalServerError(client, JSON.stringify(value));
            });

        });
        this.onMessage("GetWalletDetails",(client , message)=>
        {
            userControllerSocket.userWallet(message.user_id).then(value =>{
                console.log(value);
                client.send("GetWalletDetails",JSON.stringify(value));
            }).catch(value=>{
                console.log(value);
                SocketError.InternalServerError(client, JSON.stringify(value));
            });

        });
        this.onMessage("UpdateWallet",(client , message)=>
        {
            userControllerSocket.updateWalletData(message.user_id, message.user_amount, message.user_bonus,
                message.user_credit_amount, message.user_debit_amount, message.user_win_amount,
                message.user_loss_amount).then(value =>{
                console.log(value);
                client.send("UpdateWallet",JSON.stringify(value));
            }).catch(value=>{
                console.log(value);
                SocketError.InternalServerError(client, JSON.stringify(value));
            });

        });
        this.onMessage("GetTransactionHistory",(client , message)=>
        {
            userControllerSocket.userPayment(message.user_id).then(value =>{
                console.log(value);
                client.send("GetTransactionHistory",JSON.stringify(value));
            }).catch(value=>{
                console.log(value);
                SocketError.InternalServerError(client, JSON.stringify(value));
            });

        });
        this.onMessage("GetUserbankData",(client , message)=>
        {
            userControllerSocket.usersBankData(message.user_id).then(value =>{
                console.log(value);
                client.send("GetUserbankData",JSON.stringify(value));
            }).catch(value=>{
                console.log(value);
                SocketError.InternalServerError(client, JSON.stringify(value));
            });

        });
        this.onMessage("GetUserUpiData",(client , message)=>
        {
            userControllerSocket.usersUpiData(message.user_id).then(value =>{
                console.log(value);
                client.send("GetUserUpiData",JSON.stringify(value));
            }).catch(value=>{
                console.log(value);
                SocketError.InternalServerError(client, JSON.stringify(value));
            });

        });
        this.onMessage("CreateUserBank",(client , message)=>
        {
            userControllerSocket.userCreateBank(message.account_holder_name, message.ifsc_code, message.account_number,
                message.user_id).then(value =>{
                console.log(value);
                client.send("CreateUserBank",JSON.stringify(value));
            }).catch(value=>{
                console.log("abc " + value);
                SocketError.InternalServerError(client, JSON.stringify(value));
            });

        });
        this.onMessage("CreateUserUpi",(client , message)=>
        {
            userControllerSocket.userCreateUpi(message.upi_id, message.user_id).then(value =>{
                console.log(value);
                client.send("CreateUserUpi",JSON.stringify(value));
            }).catch(value=>{
                console.log(value);
                SocketError.InternalServerError(client, JSON.stringify(value));
            });

        });
        this.onMessage("CreatePrivateRoom",(client , message)=>
        {
            userControllerSocket.userCreateLobby(message.lobby_name, message.variation_name, message.player_count, message.lobby_total_amount,
                message.lobby_buy_in, message.lobby_commision_amount, message.room_id, message.lobby_status, message.lobby_owner).then(value =>{
                console.log(value);
                client.send("CreatePrivateRoom",JSON.stringify(value));
            }).catch(value=>{
                console.log(value);
                SocketError.InternalServerError(client, JSON.stringify(value));
            });

        });
        this.onMessage("GetUserInventory",(client , message)=>
        {
            userControllerSocket.userSearchUserInventoryData(message.user_id).then(value =>{
                console.log(value);
                client.send("GetUserInventory",JSON.stringify(value));
            }).catch(value=>{
                console.log(value);
                SocketError.InternalServerError(client, JSON.stringify(value));
            });

        });
        this.onMessage("UpdateDiceInventory",(client , message)=>
        {
            userControllerSocket.updateByDice(message.user_id, message.dice).then(value =>{
                console.log(value);
                client.send("UpdateDiceInventory",JSON.stringify(value));
            }).catch(value=>{
                console.log(value);
                SocketError.InternalServerError(client, JSON.stringify(value));
            });

        });
        this.onMessage("UpdateEmojiInventory",(client , message)=>
        {
            userControllerSocket.updateByEmoji(message.user_id, message.emoji).then(value =>{
                console.log(value);
                client.send("UpdateEmojiInventory",JSON.stringify(value));
            }).catch(value=>{
                console.log(value);
                SocketError.InternalServerError(client, JSON.stringify(value));
            });

        });
        this.onMessage("GetBuddiesList",(client , message)=>
        {
            userControllerSocket.userBuddiesData(message.user_id).then(value =>{
                console.log(value);
                client.send("GetBuddiesList",JSON.stringify(value));
            }).catch(value=>{
                console.log(value);
                SocketError.InternalServerError(client, JSON.stringify(value));
            });

        });
        this.onMessage("GetBuddiesRequestList",(client , message)=>
        {
            userControllerSocket.requestBuddiesData(message.user_id).then(value =>{
                console.log(value);
                client.send("GetBuddiesRequestList",JSON.stringify(value));
            }).catch(value=>{
                console.log(value);
                SocketError.InternalServerError(client, JSON.stringify(value));
            });

        });
        this.onMessage("SendBuddyRequest",(client , message)=>
        {
            userControllerSocket.userCreateBuddies(message.request_by, message.request_to).then(value =>{
                console.log(value);
                client.send("SendBuddyRequest",JSON.stringify(value));
            }).catch(value=>{
                console.log(value);
                SocketError.InternalServerError(client, JSON.stringify(value));
            });

        });
        this.onMessage("UpdateBuddyRequest",(client , message)=>
        {
            userControllerSocket.updateBuddiesData(message.request_by, message.request_to, message.req_status).then(value =>{
                console.log(value);
                client.send("UpdateBuddyRequest",JSON.stringify(value));
            }).catch(value=>{
                console.log(value);
                SocketError.InternalServerError(client, JSON.stringify(value));
            });

        });
        this.onMessage("GetAchievements",(client , message)=>
        {
            userControllerSocket.AllAchievementss().then(value =>{
                console.log(value);
                client.send("GetAchievements",JSON.stringify(value));
            }).catch(value=>{
                console.log(value);
                SocketError.InternalServerError(client, JSON.stringify(value));
            });

        });
        this.onMessage("GetNormalLobbies",(client , message)=>
        {
            userControllerSocket.AllNormalLobbyStructure().then(value =>{
                console.log(value);
                client.send("GetNormalLobbies",JSON.stringify(value));
            }).catch(value=>{
                console.log(value);
                SocketError.InternalServerError(client, JSON.stringify(value));
            });

        });
        this.onMessage("GetVIPLobbies",(client , message)=>
        {
            userControllerSocket.AllVipLobbyStructure().then(value =>{
                console.log(value);
                client.send("GetVIPLobbies",JSON.stringify(value));
            }).catch(value=>{
                console.log(value);
                SocketError.InternalServerError(client, JSON.stringify(value));
            });

        });
    }

    Adding_USer_Data(client:Client,option): User_common_data
    {
        let user_common_player :  User_common_data = new User_common_data();
        user_common_player.client = client;
        user_common_player.session_id = client.sessionId;
        user_common_player.User_id = option.UserId;
        this.Player_Data[client.sessionId]=user_common_player;
        return user_common_player;
    }

}
