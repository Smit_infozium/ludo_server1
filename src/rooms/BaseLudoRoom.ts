import {Client, Delayed, Room} from "colyseus";
import {BaseLudoState} from "../schema/BaseLudoState";
import {Player} from "../schema/Player";
import {PlayerImage} from "../helpers/PlayerImage";
import {DiceRolls, TypeOfRolls} from "../Untils/DiceUntils";
import {smsUntils} from "../SMS/SmsUntils"
import {Pawn_Untils} from "../Untils/Pawn_Untils";
import {SeatingClass} from "../Untils/SeatingClass";
import {userControllerSocket} from "../../dbscripts/src/controllers/userControllersSocket";
import {json} from "express";

const logger = require("../helpers/logger");


export class BaseLudoRoom extends Room<BaseLudoState> {

    playerCount: number = 0;

    maxPlayer: number = 0;      //Max player in room

    Winner_Number: number = 0;

    playerTurnWait: boolean = false;

    dicerolls : DiceRolls = new DiceRolls();

    delayedInterval!: Delayed;

    delayedSetTimeOut!: Delayed;

    PlayerImageList:Map<string,PlayerImage> = new Map<string, PlayerImage>();

    Timer_For_Bot: any;

    Seating_arrange: Array<SeatingClass> = new Array<SeatingClass>();

    Ready_players:number=0;

    Pawn_Untils : Pawn_Untils ;

    Dice_Rolled_Count : number = 0;

    Required_Token_To_winner : number = 0;

    Current_Seat : SeatingClass = new SeatingClass();

    Winner_Declared : boolean = false;

    Rolled_Dice : boolean = false;

    /**
     * Callback for when the room is created
     */
    async onCreate(options: any) {
        console.log("\n*********************** MyRoom Created ***********************");
        console.log(options);
        this.setState(new BaseLudoState());
        console.log("***********************\n");
        this.clock.start();

        // this.maxClients = 6;

        //Maximum Players in one room
        this.maxClients = options.maxClients;
        this.maxPlayer = options.maxClients;
        this.Required_Token_To_winner = options.Required_Token_To_winner;

        console.log(options.Required_Token_To_winner);

        this.Pawn_Untils = new Pawn_Untils(4, this.Required_Token_To_winner);

        //Custom Options on room creation
        this.state.bootAmount = options.bootAmount;

        //Store all players boot amount to state to give winning player
        this.state.totalWinAmount = options.bootAmount * this.maxClients;

        this.state.ActivePlayerIndex = 0;
        //
        // The patch-rate is the frequency which state mutations are sent to all clients. (in milliseconds)
        // 1000 / 20 means 20 times per second (50 milliseconds)
        //
        this.setPatchRate(1000 / 20);

        // this.delayedInterval = this.clock.setInterval(() => {
        //         console.log("Server room is start");
        // }, 1_000);
        //
        // // After 10 seconds clear the timeout;
        // // this will *stop and destroy* the timeout completely
        // this.delayedSetTimeOut = this.clock.setTimeout(() => {
        //     this.delayedInterval.clear();
        //     this.disconnect();
        // }, 30_000);
    }

    // onAuth(client: Client, options: any, request?: http.IncomingMessage): any {
    //     return super.onAuth(client, options, request);
    // }

    // Callback when a client has joined the room
    async onJoin(client: Client, options: any)
    {
        logger.info(`######### Client joined!- ${client.sessionId} ***`+options.User_Id);

        if(!this.state.players.has(client.sessionId))
        this.addPlayer(client.sessionId,0,true,options.Username,options.User_Id,options.dice,client);

         await this.database_call(options.User_Id,client);

         if(this.playerCount == this.maxClients)
         {
            await this.lock();
         }
             /*.then(value =>
             {
             })
             .catch(value => {
                console.log("eskfgswipufgvfgv"+value);
            });*/
    }

    async onLeave(client: Client, consented: boolean)
    {
        let player: Player = this.state.players.get(client.sessionId);
        if(player) {
            player.status = 2;
        }
        logger.silly(`*** Player Leave - ${client.sessionId} ***`);
        try {
            if(consented) {
                if (!this.state.GameStart) {
                    await this.Change_Auth(client, false);
                    this.state.players.delete(client.sessionId);
                    this.PlayerImageList.delete(client.sessionId);
                    this.playerCount -= 1;
                    await this.unlock();
                    return;
                }

                player.status = 2;


                for (var i = 0; i < 4; i++)
                {
                    player.token.tokenPosition[i]=-1;
                }
                if (this.state.GameStart && !this.Winner_Declared) {
                    console.log("---=-=-=-=-=-=-=-ON LEAVE THE PLAYER=-=-=-=-=-=-=-=-=->")
                    this.Calculate_Winner_After_Disconnect(client);
                }
                //client.leave();
                return ;
                //throw new Error("consented leave!!");
            }

            if(!this.state.GameStart)
            {
                await this.Change_Auth(client,false);
                this.state.players.delete(client.sessionId);
                this.PlayerImageList.delete(client.sessionId);
                this.playerCount -= 1;
                return ;
            }

            //Set Player connection to False in player state
            this.state.players.get(client.sessionId).status = 4;

            const reconnection = this.allowReconnection(client);

            if(this.state.players[client.sessionId].playerAttempts == 0)
            {
                this.state.players.get(client.sessionId).status = 2;
                reconnection.reject();
                console.log("AFTER REJECT");
                for (var i = 0; i < 4; i++)
                {
                    this.state.players[client.sessionId].tokenPosition[i]=-1;
                }
                await this.Change_Auth(client,false);
                if(this.state.GameStart  && !this.Winner_Declared)
                    this.Calculate_Winner_After_Disconnect(client);
            }

            await reconnection.then((value => {
                console.log("Reconnected-=-=-=->");
                console.log(value);
                //this.Reconnect_SendImage(client);
            })).catch((e) => {
                console.log("error"+e);
            });

            logger.info("let's wait for reconnection for client: " + client.sessionId);
            // manually reject the client reconnection
            //Set Player connection to True in player state
            this.state.players.get(client.sessionId).status = 1;
            logger.info("reconnected! client: " + reconnection);
        } catch(e) {
            logger.info("disconnected! client: " + client.sessionId);
            logger.silly(`*** Removing Player ${client.sessionId} ***`);
            this.state.players.get(client.sessionId).status = 2;
            // remove player
            // this.state.players.delete(client.sessionId);
        }
    }

    async onDispose() {
        console.log("*********************** MyRoom disposed ***********************");
    }

    onGameLoop(dt: number) {
       // this.serverTime += dt;
    }

    Process_Of_Join(client:Client)
    {
        // Arrange the image of the PLayer
            let AllData : any[] = [];

            this.PlayerImageList.forEach((value , key)=>
            {
               // console.log("Name"+this.state.players.get(key).user_name);
                if(client.sessionId != key)
                AllData.push({
                "Session_id": key,
                "Player_data":value,
            })
            });
            console.log("=-=-=-=-=-=-=-=-=-=-=-=-=-=-==-=-=-=-=-=-=>");
            // console.log(AllData);
            client.send("PlayerImage", AllData);
            AllData = null;

            //Sending the data to the other Oppenent image
            let Newplayerdata : any[] = []
            console.log(this.PlayerImageList);

            Newplayerdata.push({
                "Session_id": client.sessionId,
                "Player_data":this.PlayerImageList.get(client.sessionId),
            });
                console.log(Newplayerdata);
            this.clients.forEach(tempclient =>{
                if(tempclient.sessionId != client.sessionId )
                {
                    tempclient.send("OtherPlayer",Newplayerdata);
                }
            })


        if(this.playerCount == this.maxClients)
        {
            logger.info(`************ Room Locked - ${this.roomId} *************`);

            this.lock().then(value =>
            {
                console.log("Resolved:"+value);

                this.broadcast("Lobby_is_full",true);

                //This will stop server to disconnect the lobby
                this.Disable_time();
                console.log(this.clock.elapsedTime);

            }).catch((error)=>{
                console.log("Rejected:"+error);
            }).finally(() =>{
                console.log("Completed");
            });
        }

        //
        //Handle all message form client to server
        //
        this.initializeMessageHandler();
    }

    async initializeMessageHandler()
    {

        //Message to start game

        this.onMessage("Play", (client, message) => {

            console.log("PLAy");
            if(this.playerCount == this.maxClients && this.locked) {
                 //this.startGame();
            }
        });

        this.onMessage("Roll_Dice", (client,message) =>
        {
            /*console.log("Roll_DICE");
            let player: Player = this.state.players.get(client.sessionId);
            var diceNumberCount: number = 0;
            this.Rolled_Dice = true;

            this.state.dice = this.dicerolls.DiceRoll(<TypeOfRolls>message);

            let Tokenmovement=this.Pawn_Untils.Calculate_available_move(player,this.state.dice);
            console.log("Enable"+Tokenmovement);

            let data =
            {
                "CurrentIndex":this.state.ActivePlayerIndex,
                "DiceNumber": this.state.dice,
                "EnablePawns": Tokenmovement
            }
            console.log(data);
            this.broadcast("Dice_Rolled", data);
            if(!Tokenmovement.Enable)
            {
                this.Disable_time();
                this.clock.setTimeout(()=>
                {
                    this.Send_Current_Index();
                },1_000);
            }*/

            this.Roll_Dice_Message(client,message);
        });

        this.onMessage("Senddata",(client)=>
        {
            //this.state.dice = (Math.random() * (5-1) + 1);
            this.state.dice = this.dicerolls.DiceRoll();
            this.state.ActivePlayerIndex = 1;
            console.log("this",this.state.dice);
        });

        this.onMessage("nextPlayerTurn", (client, message) => {
            if(this.playerTurnWait) {
                this.playerTurnWait = false;
                this.nextPlayerTurn();
            }
        });

        this.onMessage("Chat",(client,message)=>{
            let data ={
                "Session_id":client.sessionId,
                "Message":message,
                "CurrentIndex":this.state.players[client.sessionId].seat,
                "Username":this.state.players[client.sessionId].user_name
            }

            this.broadcast("Chat",data);
        })

        this.onMessage("Quick_message",(client,message)=>{
            let data ={
                "Session_id":client.sessionId,
                "Index":message,
                "CurrentIndex":this.state.players[client.sessionId].seat
            }

            this.broadcast("Quick_message",data);

        });

        this.onMessage("Emoji_message",(client,message)=>{
            let data ={
                "Session_id":client.sessionId,
                "Index":message,
                "CurrentIndex":this.state.players[client.sessionId].seat
            }

            this.broadcast("Emoji_message",data);
        });

        this.onMessage("Ready_for_Game",(client,message) =>{
            this.Ready_players +=1;
            console.log("Ready_for_Game"+this.Ready_players)
            if(this.Ready_players == this.maxClients)
            {
                this.clock.setTimeout(()=>
                {
                    this.StartGame();
                    delete this.Ready_players;
                },10);

            }
        });

        // Set the callback for the "ping" message for tracking server-client latency
        this.onMessage("SendOTP", (client: Client) => {
            //client.send(0, { serverTime: this.serverTime });
            let sms : smsUntils = new smsUntils();
            let data = sms.SendOTP();
            data.then(value => {
                console.log(value);
            })
        });

        this.onMessage("VerifyOTP",(client => {
            let sms : smsUntils = new smsUntils();
            sms.Verfication();
        }));

        this.onMessage("Token_Moved",((client, message) =>
        {
            this.Calculate_Winner(client,message);
        }));

        this.onMessage("Reconnect_PlayerImage",(client,message)=>{
            this.Reconnect_SendImage(client);
        })
        this.setSimulationInterval((dt) => this.onGameLoop(dt));
    }

    startGame() {
        this.state.ActivePlayerIndex = 0;
        logger.info("************************ Game Started ***********************");
        this.broadcast("playGame", this.getPlayerFromSeat(this.state.ActivePlayerIndex));
    }

    //Add new player into room or add bot to room
     addPlayer(sessiontId: string, guest_profile_index: number = 0, isGuest: boolean = true , User_name = "Sonu",User_id :number,dice:number,client:Client)
    {

        let newPlayer: Player = new Player();
        newPlayer.status = 1;
        newPlayer.seat = this.playerCount;
        newPlayer.sessionId = sessiontId;
        newPlayer.Winnerplace = 4;
        newPlayer.guest = isGuest;
        newPlayer.user_name = User_name;
        newPlayer.IsBot = false;
        newPlayer.playerAttempts = 3;
        newPlayer.dice = dice;
        newPlayer.user_id = User_id;

        this.playerCount += 1;
        let seat=0;

            seat = -1+(2*this.playerCount);
            console.log("Seat"+seat);
            if(seat>4)
            {
                seat = 1+(seat- 4);
                console.log("Seat1234"+seat);
            }
            console.log(seat)
            newPlayer.seat = seat;
            newPlayer.token.index = seat;
            newPlayer.token.Setting_Point();

            let temp : SeatingClass = new SeatingClass();
            temp.SeatNumber = seat;
            temp.session_id = sessiontId;
            temp.client = client;

            this.Seating_arrange.push(temp);

            this.Seating_arrange.sort((a, b) => {
              return   a.SeatNumber - b.SeatNumber
            });

            console.log(this.Seating_arrange);
        //Set Player to state
        this.state.players.set(sessiontId, newPlayer);


        logger.info("************************ New PLayer ***********************");
        logger.info(`******** New Player ${newPlayer.sessionId} added Successfully!! ********`);
        logger.info("***********************************************************");
    }

    removePlayer() {

    }

    //Remove player from room
    removePlayerFromRoom(sessionId: string) {
        
        this.state.players.delete(sessionId);
        logger.silly(`******** Player Removed - ${sessionId} ********`);
    }

    //Make Bot trun if player left the Game
    makeBotTurn (client: Client ) {
        console.log("This.Rolled_dice" + this.Rolled_Dice);
        let Tokenmovement :any;

        if (this.Rolled_Dice) {
            let player: Player = this.state.players.get(client.sessionId);
            Tokenmovement = this.Pawn_Untils.Calculate_available_move(player, this.state.dice);

        }
        else {
            let player: Player = this.state.players.get(client.sessionId);

            this.Rolled_Dice = true;

            this.state.dice = this.dicerolls.DiceRoll(TypeOfRolls.None);
            this.state.dice = 6;
            Tokenmovement = this.Pawn_Untils.Calculate_available_move(player, this.state.dice);
            let data =
            {
                "CurrentIndex": this.state.ActivePlayerIndex,
                "DiceNumber": this.state.dice,
                "EnablePawns": Tokenmovement,
                "IsBot": true
            }
            console.log(data);
            this.broadcast("Dice_Rolled", data);
        }
        if (!Tokenmovement.Enable) {
            this.Disable_time();
            this.clock.setTimeout(() => {
                this.Send_Current_Index();
            }, 1_000);
        } else
            {
            console.log("Enable" + Tokenmovement);
            var selectedToken: number = Tokenmovement.Token_Index[0];
            this.Disable_time();
            this.clock.setTimeout(() => {
                this.Calculate_Winner(client,selectedToken)
            }, 1_000);
        }


    }

    //Next Player turn
    nextPlayerTurn( Another_Change?:boolean) : Promise<any> {
        // this.state.allPlayersTurnCompleted += 1;
        let promise = new Promise<any>((resolve, reject) =>
        {
        try
        {
            /*if((this.state.dice == 6 || Another_Change) && this.Dice_Rolled_Count<3)*/
            if(Another_Change && this.Dice_Rolled_Count<3)
            {
                this.Dice_Rolled_Count += 1;
            }
            else {
                let endloop = false;

                this.Dice_Rolled_Count = 1;

                while (!endloop) {
                    let index = this.Seating_arrange.indexOf(this.Current_Seat);
                    index = (index + 1) % this.playerCount;
                    this.Current_Seat = this.Seating_arrange[index];

                    if(this.state.players.get(this.Current_Seat.session_id).status == 1 || this.state.players.get(this.Current_Seat.session_id).status == 4)
                    {
                        this.state.ActivePlayerIndex = this.Current_Seat.SeatNumber;
                        console.log("index" + index + "Seating" + this.Seating_arrange);
                        endloop = true;
                    }
                }
            }
            resolve(1);
        }
          catch (e)
          {
              reject(e);
          }
        });
        return promise;
    }

    getPlayerFromSeat(seat: number): Player
    {
        let player: Player = new Player();
        this.state.players.forEach(key => {
            let currentPlayer: Player = this.state.players.get(key.sessionId);
            if(key.seat == seat) {
                player = currentPlayer;
            }
        });
        console.log("======> ", JSON.stringify(player));
        return player;
    }

    StartGame()
    {
        this.broadcast("Player_Ready","ALL PLAYER ARE READY");
            this.Current_Seat = this.Seating_arrange[0];
            this.state.ActivePlayerIndex = this.Current_Seat.SeatNumber;
            this.state.GameStart =true;

            let user_ids : Array<number> = new Array<number>();
            this.state.players.forEach(value => {
                user_ids.push(value.user_id);
            });

            userControllerSocket.Cut_update_money(user_ids,this.state.bootAmount * -1).then((value)=>{
                this.broadcast("UpdateMoney",value);
                // start the clock ticking
                // Set an interval and store a reference to it
                // so that we may clear it later
                this.delayedInterval = this.clock.setInterval(() => {
                    console.log("Time now " + this.clock.currentTime);
                }, 1000);

                // After 10 seconds clear the timeout;
                // this will *stop and destroy* the timeout completely
                this.delayedSetTimeOut=this.clock.setTimeout(() => {
                    this.delayedInterval.clear();

                    this.broadcast("StartPlay");
                    this.Send_Current_Index(true);
                }, 5_000);
            });

    }

    Send_Current_Index(Attack?:boolean)
    {
        this.Disable_time();
        this.nextPlayerTurn(Attack).then(value => {
            if(value)
            {
                this.broadcast("CurrentPlayer", this.state.ActivePlayerIndex);

                this.Rolled_Dice = false;
                let timer: number = 0;

                this.delayedInterval = this.clock.setInterval(() => {
                    console.log("Time now set "+this.state.ActivePlayerIndex+" "+ this.clock.currentTime);

                    this.broadcast("Current_Player_Turn", {
                        CurrentIndex: this.state.ActivePlayerIndex,
                        timer: timer++,
                        maxTime: 20
                    });

                    }, 1000);

                this.delayedSetTimeOut = this.clock.setTimeout(()=>
                {
                    this.delayedInterval.clear();
                    if(this.state.players[this.Current_Seat.session_id].playerAttempts ==0)
                    {
                        console.log("attempt"+this.state.players[this.Current_Seat.session_id].playerAttempts)
                        this.state.players[this.Current_Seat.session_id].status = 2;
                        this.Current_Seat.client.leave(4200 , "You Removed from server");
                        this.Calculate_Winner_After_Disconnect(this.Current_Seat.client);
                    }
                    else
                    {
                        this.state.players.get(this.Current_Seat.session_id).playerAttempts -=1;
                        console.log("attempt1233"+this.state.players.get(this.Current_Seat.session_id).playerAttempts);
                        var data = {
                            "Current_Index":this.Current_Seat.SeatNumber,
                            "Attempt": this.state.players[this.Current_Seat.session_id].playerAttempts
                        }
                        console.log("Data-=-=-=-=-=-=->");
                        console.log(data);
                        this.broadcast("Attempt",data);
                        //this.Send_Current_Index();
                        this.makeBotTurn(this.Current_Seat.client);
                    }
                    //console.log("Send from delayed SetTime out "+this.state.players.get(this.Current_Seat.session_id).playerAttempts);

                },21_000);
            }
        }).catch((value)=>{
            console.log("error"+value);
        })
    }

    async database_call(User_Id:number ,client:Client)
    {
            userControllerSocket.searchUserDetail(User_Id).then(value =>
            {
                /*let promise = new Promise((resolve, reject) =>
                {*/
                    try{
                        console.log("Data is Added");
                        //Adding the Image to the To the list
                        //Get the Image data from the Database and then add

                        let PI : PlayerImage = new PlayerImage();

                        //Get the Image data from the Database and then add
                        //PI.Initialize("User_id",User_Image)

                        PI.Initialize(value,client);
                        this.PlayerImageList.set(client.sessionId,PI);

                        console.log(this.PlayerImageList);

                        this.Process_Of_Join(client);
                    }
                    catch (e) {
                        console.log("Database connection is fail"+e);
                       /* reject (e);*/
                    }
                    /*console.log("Data Added to the list"+User_Id)
                    return promise;
                });*/
                // console.log(value);
            }).catch(value => {
                console.log(value);
            })
    }

    Disable_time()
    {
        this.delayedInterval.clear();
        this.delayedSetTimeOut.clear()
    }

    Calculate_Winner_After_Disconnect(client:Client)
    {

        let playercount = this.state.players.size;
        let playerleft : number=0;
        this.state.players.forEach((value, key) => {
            if(value.status == 2)
            {
                playerleft++;
            }
        });

        if(playercount - (playerleft+this.Winner_Number) ==1)
        {
            this.Disable_time();
            this.state.players.forEach((value, key) => {
                if(value.status == 1 && this.Winner_Number<this.maxClients/2)
                {
                    this.Winner_Number++;
                    value.status = 3;
                    value.Winnerplace = this.Winner_Number;
                }
            });

            console.log("Get in the Winner Set");
            this.Send_Winner();
        }
        else
        {
            console.log("---=-=-=-=-=-=-=-ON LEAVE THE PLAYER INSIDE SENDMESSAGE=-=-=-=-=-=-=-=-=->")
            if(this.state.players[client.sessionId].seat == this.state.ActivePlayerIndex)
            {
                console.log("Current_index");
                this.Send_Current_Index();
            }
        }

    }

    Send_Winner()
    {
        console.log("Winner_Number"+this.Winner_Number+"this.maxClients"+(this.maxClients/2))
        // if(this.Winner_Number == this.maxClients/2)
        // {
            let amount = this.state.bootAmount * this.playerCount*0.9;

            let Winner_array : Array<any> = new Array<any>();
            this.state.players.forEach((value, key) => {
                if(value.status == 3)
                {
                    let player_amount;
                    if(this.maxClients == 4)
                    {
                        if(value.Winnerplace == 1)
                        player_amount = amount * 0.6;
                        else
                        player_amount = amount * 0.4;
                    }
                    else
                    {
                        player_amount = amount;
                    }


                    let data = {
                        "Session_id":value.sessionId,
                        "winner_place":value.Winnerplace,
                        "winner_stats":value.status,
                        "winner_amount":player_amount,
                        "winner_user_id":value.user_id
                    }
                    Winner_array.push(data);
                }
                else
                {
                    let data ={
                        "Session_id":value.sessionId,
                        "winner_place":value.Winnerplace,
                        "winner_stats":value.status,
                        "winner_amount":0,
                        "winner_user_id":value.user_id
                    }
                    Winner_array.push(data);
                }
            });

            let data ={
                winners : Winner_array
            }
            this.broadcast("Winner",data)
            this.Winner_Declared = true;
            console.log(data);

            this.Update_Winner_Amount(Winner_array).then((value)=>{
                console.log(value);
                this.disconnect();
            }).catch((e)=>{
                console.log(e);
            });




            return;
       // }
    }

    Reconnect_SendImage(client:Client)
    {
        let AllData : any[] = [];

        this.PlayerImageList.forEach((value , key)=>
        {
            // console.log("Name"+this.state.players.get(key).user_name);
            if(client.sessionId != key)
                AllData.push({
                    "Session_id": key,
                    "Player_data":value,
                })
        });
        console.log("=-=-=-=-=-=-=-=-=-=-=-=-=-=-==-=-=-=-=-=-=>");
        // console.log(AllData);
        client.send("Reconnect_PlayerImage", AllData);
    }

    Roll_Dice_Message(client:Client,message:any)
    {
        console.log("Roll_DICE");
        let player: Player = this.state.players.get(client.sessionId);

        this.Rolled_Dice = true;
        this.state.dice = this.dicerolls.DiceRoll(<TypeOfRolls>message);
        let Tokenmovement=this.Pawn_Untils.Calculate_available_move(player,this.state.dice);
        console.log("Enable"+Tokenmovement);

        let data =
            {
                "CurrentIndex":this.state.ActivePlayerIndex,
                "DiceNumber": this.state.dice,
                "EnablePawns": Tokenmovement,
                "IsBot": false
            }
        console.log(data);
        this.broadcast("Dice_Rolled", data);

        if(message != -1) {
            var user_data: Array<number> = new Array<number>();
            user_data.push(player.user_id);

            var amount: number =Math.floor(this.state.bootAmount * -0.2);
            userControllerSocket.Cut_update_money(user_data, amount).then(value => {
                client.send("UpdateMoney", value);
            });
        }

        if(!Tokenmovement.Enable)
        {
            this.Disable_time();
            this.clock.setTimeout(()=>
            {
                this.Send_Current_Index();
            },1_000);
        }
    }

    Calculate_Winner(client:Client,Tokenindex:any)
    {
        var Place_of_token = this.Pawn_Untils.Calculate_move(this.state.players, this.state.players.get(client.sessionId), this.state.dice, Tokenindex);
        console.log(Place_of_token);

        this.broadcast("Token_Moved", Place_of_token);

        console.log("Place_of_token.Winner" + Place_of_token.Winner);

        if (Place_of_token.Winner) {
            this.Winner_Number++;
            this.state.players.get(client.sessionId).status = 3;
            this.state.players.get(client.sessionId).Winnerplace = this.Winner_Number;

            let playercount = this.state.players.size;
            let playerleft : number=0;
            this.state.players.forEach((value, key) => {
                if(value.status == 2)
                {
                    playerleft++;
                }
            });

            console.log("No_of_winner" + this.maxClients / 2);
            if(playercount - (playerleft+this.Winner_Number) == 1 || this.Winner_Number ==this.maxClients/2 )
            {
                this.Disable_time();
                this.state.players.forEach((value, key) => {
                    if(value.status == 1 && this.Winner_Number < this.maxClients/2)
                    {
                        this.Winner_Number++;
                        value.status = 3;
                        value.Winnerplace = this.Winner_Number;
                    }
                });

                console.log("Get in the Winner Set");
                this.Send_Winner();
                return;
            }
        }

        let time: number;

        if (Place_of_token.Attack != null) {
            time = 1000 + Place_of_token.Dice_Value * 100;
        } else {
            time = Place_of_token.Dice_Value * 100;
        }

        this.clock.setTimeout(() => {
            console.log(Place_of_token.Another_Change);
            this.Send_Current_Index(Place_of_token.Another_Change);
        }, time);

    }

    async Change_Auth(client:Client , Active:boolean)
    {
        let user_id: Array<number> = new Array<number>();
        let data : number = this.state.players[client.sessionId].client.clone();
        user_id.push(data);
        userControllerSocket.updateAuthData(user_id,Active);
    }

    async Update_Winner_Amount(data:Array<any>):Promise<any>
    {
       let promise  = new Promise(async (resolve,reject)=>
       {
        try {
            console.log("lolololol");
            for (let i = 0; i < data.length; i++) {
                if(data[i].winner_stats==4)
                {
                    continue;
                }
                let ids: Array<number> = new Array<number>();
                ids.push(data[i].winner_user_id);
                await userControllerSocket.Cut_update_money(ids,data[i].winner_amount);

            }
            resolve("1");
        }
        catch (e) {
            console.log(e);
            reject(e);
        }

       });
        return promise;
    }

}