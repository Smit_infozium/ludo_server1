import { Schema, type, MapSchema, ArraySchema } from "@colyseus/schema";

export class CommonPlayerRoomState extends Schema
{
    @type("number") TotalClient : number = 0;
}