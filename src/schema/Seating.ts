import { Schema, MapSchema, ArraySchema, type } from "@colyseus/schema";
import enumerate = Reflect.enumerate;

export class Seating extends Schema
{
    @type("number") SeatNumber: number;

    @type(["number"]) tokenPosition = new ArraySchema<number>(-1,-1,-1,-1);
}