import { Schema, type, MapSchema } from "@colyseus/schema";
import { Token } from "./Token";

export class Player extends Schema {

    @type("number") seat: number;

    @type("string") sessionId: string;

    //This will store number 1=Playing,2=left,3=Winner,4=Connnection lost.
    @type("number") status: number;

    @type("number") Winnerplace: number = 0;

    @type("number") user_id: number;

    @type("string") user_name: string;

    @type("boolean") guest: boolean;

    @type("number") dice: number;

    @type(Token) token = new Token();

    @type("number") playerAttempts: number;

    @type("boolean") IsBot: boolean;
}