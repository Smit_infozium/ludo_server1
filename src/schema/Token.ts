import { Schema, MapSchema, ArraySchema, type } from "@colyseus/schema";
import enumerate = Reflect.enumerate;
export class Token extends Schema
{
    @type("number") index: number =0;

    @type(["number"]) tokenPosition = new ArraySchema<number>(-1,-1,-1,-1);

    @type("int32") StartIndex : number = 0;

    @type("int32") EndIndex : number = 0;

    public Setting_Point()
    {
        this.StartIndex = 13 * (this.index - 1) + 1;
        if (this.StartIndex == 1)
        {
            this.EndIndex = 51;
        }
        else
        {
            this.EndIndex = this.StartIndex - 2;
        }
    }
}