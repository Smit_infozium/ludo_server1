import { Schema, type, MapSchema, ArraySchema } from "@colyseus/schema";
import { Player } from "./Player";

export class BaseLudoState extends Schema {
    
    @type({ map: Player }) players = new MapSchema<Player>();

    @type("number") bootAmount: number = 0;

    @type("number") totalWinAmount: number = 0;
    
    @type("number") allPlayersTurnCompleted: number = 0;

    @type(["string"]) Seating = new ArraySchema<string>();

    @type("number") dice: number = 0 ;

    @type("number") ActivePlayerIndex: number = 0;

    @type("boolean") GameStart : boolean = false;
}