import { Client } from "colyseus"


export class SocketError{
    public static InternalServerError(client : Client, msg: string) : void
    {
        client.send("Internal_Server_Error", msg);
    }
}