import {Client} from "colyseus"
export class PlayerImage
{
    User_Id: number = 0;
    Username: string = "LOL";
    User_Image: string = "";
    Total_Wins: number = 0;
    User_Country: string = "";
    Token_Captured:number = 0;
    Opp_Token_Captured:number = 0;
    Total_Loss: number = 0;
    //client : Client = null;

    public Initialize(data:any,playerclient:Client)
    {
        this.User_Id = data.User_Id;
        this.Username = data.User_DisplayName;
        this.User_Image = data.User_Image;
        this.User_Country = data.Total_Loss;
        this.Token_Captured = data.Token_Captured;
        this.Opp_Token_Captured = data.Opp_Token_Captured;
        this.Total_Loss = data.Total_Loss;
        //this.client = playerclient;
    }
}