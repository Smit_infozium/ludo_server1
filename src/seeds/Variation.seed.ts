import { Factory, Seeder } from 'typeorm-seeding';
import { Connection } from 'typeorm';
import { Variation } from '../../dbscripts/src/entity/Variation';

let data = require('./DefaultData.json')

export default class CreateAchievements implements Seeder {
  public async run(factory: Factory, connection: Connection): Promise<any> {
      let entries = data.filter(obj => {
          if (obj.Entity == 'Variation') return obj.Data
      })
    await connection
      .createQueryBuilder()
      .insert()
      .into(Variation)
      .values(entries[0].Data)
      .execute()
  }
}