import { Factory, Seeder } from 'typeorm-seeding';
import { Connection } from 'typeorm';
import { Lobby_Structure } from '../../dbscripts/src/entity/Lobby_Structure';

let data = require('./DefaultData.json')

export default class CreateAchievements implements Seeder {
  public async run(factory: Factory, connection: Connection): Promise<any> {
      let entries = data.filter(obj => {
          if (obj.Entity == 'Lobby_Structure') return obj.Data
      })
    await connection
      .createQueryBuilder()
      .insert()
      .into(Lobby_Structure)
      .values(entries[0].Data)
      .execute()
  }
}
