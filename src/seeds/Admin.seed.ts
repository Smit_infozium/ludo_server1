import { Factory, Seeder } from 'typeorm-seeding';
import { Connection } from 'typeorm';
import { Admin } from '../../dbscripts/src/entity/Admin';

let data = require('./DefaultData.json')

export default class CreateAdmin implements Seeder {
  public async run(factory: Factory, connection: Connection): Promise<any> {
      let entries = data.filter(obj => {
          if (obj.Entity == 'Admin') return obj.Data
      })
    await connection
      .createQueryBuilder()
      .insert()
      .into(Admin)
      .values(entries[0].Data)
      .execute()
  }
}