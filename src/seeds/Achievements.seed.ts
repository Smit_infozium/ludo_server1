import { Factory, Seeder } from 'typeorm-seeding';
import { Connection } from 'typeorm';
import { Achievements } from '../../dbscripts/src/entity/Achievements';

let data = require('./DefaultData.json')

export default class CreateAchievements implements Seeder {
  public async run(factory: Factory, connection: Connection): Promise<any> {
      let entries = data.filter(obj => {
          if (obj.Entity == 'Achievements') return obj.Data
      })
    await connection
      .createQueryBuilder()
      .insert()
      .into(Achievements)
      .values(entries[0].Data)
      .execute()
  }
}