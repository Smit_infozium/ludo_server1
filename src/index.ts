// @ts-ignore
const newrelic = require('newrelic')
import http from "http";
import express from "express";
import cors from "cors";
import { Server } from "colyseus";
import { monitor } from "@colyseus/monitor";
import { WebSocketTransport } from "@colyseus/ws-transport"
import { BaseLudoRoom } from "./rooms/BaseLudoRoom";
import { CommonPlayerRoom} from "./rooms/CommonPlayerRoom";
import { NotFoundError} from '../dbscripts/src/errors/InstancesCE';
import { errorHandler } from '../dbscripts/src/middlewares/errorhandler';

require('express-async-errors');

const userLinks = require("../dbscripts/src/routes/userRoutes");
const adminLinks = require('../dbscripts/src/routes/adminRoutes');

const port = Number(process.env.PORT || 2567);
const app = express();

app.use(cors());
app.use(express.json({limit:1024*1024*50}))

let lol : WebSocketTransport;

const server = http.createServer(app);

lol = new WebSocketTransport({server});
const gameServer = new Server
(
    {
    "transport": lol,
});

gameServer.define("LudoGame", BaseLudoRoom).filterBy(['maxClients','bootAmount']);
gameServer.define("CustomRoom",BaseLudoRoom);
gameServer.define("CommonLobby", CommonPlayerRoom);

// register colyseus monitor AFTER registering your room handlers
app.use("/colyseus", monitor());
app.use('/api/admin', adminLinks);
app.use('/api/user',userLinks);

app.all('*', (req, res)=>{
    throw new NotFoundError('Path Not Found')
})

app.use(errorHandler);

process.on('unhandledRejection', (reason)=>{
    console.log(reason)
})

process.on('uncaughtException', (error)=>{
    console.log(error);
    process.exit(1)

})

gameServer.listen(port);
console.log(`Listening on ws://localhost:${ port }`)
