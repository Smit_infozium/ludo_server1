export enum TypeOfRolls {
    None = -1,
    Even,
    Odd,
    High,
    Low,
}

export class DiceRolls
{
    private Even = [2,4,6]
    private Odd = [1,3,5]
    private High = [4,5,6]
    private Low = [1,2,3]

    public DiceRoll(Rolls:TypeOfRolls = TypeOfRolls.None)
    {
        let diceNumberList;
        let diceNumber: number;


        switch (Rolls) {
            case TypeOfRolls.None:
                console.log("====================================> DICE ROLL: NONE");
                diceNumberList = [1, 2, 3, 4, 5, 6];
                break;

            case TypeOfRolls.Even:
                console.log("====================================> DICE ROLL: EVEN");
                diceNumberList = this.Even;
                break;

            case TypeOfRolls.Odd:
                console.log("====================================> DICE ROLL: ODD");
                diceNumberList = this.Odd;
                break;

            case TypeOfRolls.High:
                console.log("====================================> DICE ROLL: HIGH");
                diceNumberList = this.High;
                break;

            case TypeOfRolls.Low:
                console.log("====================================> DICE ROLL: LOW");
                diceNumberList = this.Low;
                break;

        }

        diceNumber = diceNumberList[Math.floor(Math.random() * diceNumberList.length)];
        return diceNumber;
    }
}