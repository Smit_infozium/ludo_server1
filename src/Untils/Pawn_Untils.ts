import {Token} from "../schema/Token";
import {Player} from "../schema/Player";
import { ArraySchema, MapSchema } from "@colyseus/schema";
import enumerate = Reflect.enumerate;

export class Pawn_Untils {

    SafeArray : any = new Array<number>();
    Winner_Token_require : number = 0;

    constructor(Board:number , Winner_Token : number)
    {
        for(let i = 0 ;i< Board;i++)
        {
            let SafeArea1: number = 0;
            let SafeArea2: number = 0;

            SafeArea1 = 1+(i * 13);
            SafeArea2 = 9+(i * 13);

            this.SafeArray.push(SafeArea1);
            this.SafeArray.push(SafeArea2);
        }
         this.SafeArray.sort((a:number,b:number)=>{
               return a-b;
         });

        this.Winner_Token_require = Winner_Token;
        console.log("SafeArray"+JSON.stringify(this.SafeArray));
    }

    public Calculate_move (Players:MapSchema<Player>,player:Player , Dice_Value: number ,Token_Index:number ): any
    {
        let tokentemp : Token = player.token;
        let winner : boolean = false;
        let another_change : boolean = false;

        if(Dice_Value == 6)
        {
            another_change = true;
        }
        if(tokentemp.tokenPosition[Token_Index]==-1)
        {
            tokentemp.tokenPosition[Token_Index] = tokentemp.StartIndex;
            winner = false;
        }
        else
        {
            let temp :number = tokentemp.tokenPosition[Token_Index];
            let home_Start: number = 51 +(6 * player.token.index - 5)

            for (let i = 1; i <= Dice_Value; i++)
            {
                // console.log(tokentemp.EndIndex);
                if(temp == tokentemp.EndIndex )
                {
                    // console.log("inner if else"+temp);
                    temp = home_Start+(Dice_Value-i);
                    break;
                }
                else
                {
                    if(temp == 51)
                    {
                        temp = 0;
                    }
                    else
                    {
                        temp +=1;
                    }
                }
            }
            tokentemp.tokenPosition[Token_Index] = temp;

            if(temp == (home_Start+5))
            {
                console.log("-=-=-=-=-=-=-=-=->"+this.Winner_Token_require);
                let winner_TokenCount : number = 0;
                another_change = true;
                for (let i = 0;i<4;i++)
                {
                    if(tokentemp.tokenPosition[i] == (home_Start+5) )
                    {
                        winner_TokenCount++;
                    }
                }
                console.log("-=-=-=-=-=winner_TokenCount-=-=-=->"+winner_TokenCount);
                if(winner_TokenCount == this.Winner_Token_require)
                {
                    winner = true;
                }
            }
            else
            {
                winner = false;
            }

        }
        let Cal = this.Calculate_Attack(Players,player,Token_Index);
        if(Cal != null)
        {
            another_change = true;
        }

        let data ={
            "Current_Index" : player.seat,
            "Token" : Token_Index,
            "Place_of_token" : tokentemp.tokenPosition[Token_Index],
            "Dice_Value" : Dice_Value,
            "Attack" : Cal,
            "Another_Change": another_change,
            "Winner" : winner
        }
         console.log(data);
        return data;
    }

    public Calculate_available_move(player:Player,Dice_Value:number):any
    {
        //For testing
        let Token_Will_move : Array<number> = new Array<number>();

        let Enable : boolean = false;

        //Testing
       /* for (let i = 0 ; i<4 ; i++)
        {
           if(player.seat == 1 && i == 0)
            player.token.tokenPosition[i] = 56;
            if(player.seat == 1 && i == 1)
                player.token.tokenPosition[i] = 56;
           if(player.seat == 3)
            player.token.tokenPosition[i] = 14;
        }*/

        let Home : number = 51 + (6 * player.token.index);
        for(let i=0; i<player.token.tokenPosition.length; i++)
        {
                if(player.token.tokenPosition[i] == -1)
                {
                    if(Dice_Value == 6) {
                        Enable = true;
                        Token_Will_move.push(i);
                    }
                }
                else
                {
                    if(player.token.tokenPosition[i]+Dice_Value<=Home && player.token.tokenPosition[i] != Home)
                    {
                        Enable = true;
                        Token_Will_move.push(i);
                    }
                }
        }

        let data ={
            "Enable":Enable,
            "Token_Index":Token_Will_move
        }
        return data;
    }

    public Calculate_Attack(Players:MapSchema<Player>,player:Player,Token:number):any
    {
        let indexof = this.SafeArray.indexOf(player.token.tokenPosition[Token]);
        console.log("indexof"+indexof);

        if(indexof !=-1)
        {
            return null;
        }

        let AttackToken = new Array();
        console.log("didnt go in safeplace");

        console.log(JSON.stringify(Players));

        let data = null;

        let Flag: boolean = false;

        Players.forEach((value, key):any =>
        {

            if(value != player && !Flag)
            {
                console.log("-=-=-=-=-=-=-==-=-=-=-=-=-=-=-=-==-=-=  1");
                for (let i =0; i<value.token.tokenPosition.length; i++)
                {
                    console.log("-=-=-=-=-=-=-==-=-=-=-=-=-=-=-=-==-=-=  2");
                    if(value.token.tokenPosition[i]==player.token.tokenPosition[Token])
                    {
                        console.log("Token"+i);
                        Flag = true;
                        value.token.tokenPosition[i] = -1;
                        AttackToken.push(i);
                    }
                }

                if(Flag)
                {
                    data ={
                        "PlayerIndex":value.seat,
                        "Array_Attack":AttackToken
                    }
                    console.log("-=-=-=-=-=-=-=->");
                    console.log(data);
                }
                else
                {
                    console.log("-=-=-=-=-=-=-==-=-=-=-=-=-=-=-=-==-=-=  3" + Flag);
                    data = null;
                }
            }
        });
        return data;
    }
}